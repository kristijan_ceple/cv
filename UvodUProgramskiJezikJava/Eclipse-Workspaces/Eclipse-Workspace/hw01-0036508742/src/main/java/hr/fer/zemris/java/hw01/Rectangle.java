package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Helps model a simple function which calculates the circumference and area of a <code>Rectangle</code>
 * when given its <code>width</code> and <code>height</code>.
 * 
 * @author kikyy99
 *
 */
public class Rectangle {

	/**
	 * The main function - detects if an illegal number of command line arguments was passed. Legal quantity
	 * of arguments passed is either 0 or 2.
	 * 
	 * <p>If 0 arguments are passed then further flow of the program is redirected to the <code>userInput()</code> function.</p>
	 * <p>Else if 2 arguments are passed  then further flow of the program is redirected to the <code>cmdArgs(String[] args)</code> function.</p>
	 * <p>Any other number of arguments will immediatelly exit the application with <code>status</code> code set to -1, signalling an error.</p>
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		/*
		 * Check whether there are either 2 or 0 arguments - will
		 * use switch just for the sake of practice and some change from the usual if
		 */
		
		switch(args.length) {
		case 0:
			userInput();
			break;
		case 2:
			cmdArgs(args);
			break;
		default:
			//DISALLOWED STATE!!!
			System.err.println("Ili 0 ili 2 argumenta mogu biti predana funkciji. Izlazim...");
			System.exit(-1);
		}
		
	}
	
	/**
	 * Parses <code>width</code> and <code>height</code> from command line arguments.
	 * <p>Uses the function <code>dataParsePrint(double width, double height)</code> to output the data to the user.</p>
	 * 
	 * @param args command line arguments
	 */
	private static void cmdArgs(String[] args) {
		String widthString = args[0];
		String heightString = args[1];
		
		try {
			double width = Double.parseDouble(widthString);
			double height = Double.parseDouble(heightString);
			dataParsePrint(width, height);
		} catch(NumberFormatException ex) {
			System.err.println("Pogresno unesen format command line argumenata.");
		} catch(IllegalArgumentException ex) {
			System.err.println(ex.getMessage());
		}
	}
	
	/**
	 * Retrieves <code>width</code> and <code>height</code> by using the function <code>parseUserInput(String whatToParse, Scanner sc)</code>,
	 * which uses a Scanner to get and then parse user input.
	 * <p>Uses the function <code>dataParsePrint(double width, double height)</code> to output the data to the user.</p>
	 */
	private static void userInput() {
		try(Scanner sc = new Scanner(System.in)){
			double width = parseUserInput("sirinu", sc);
			double height = parseUserInput("visinu", sc);
			dataParsePrint(width, height);
		}
	}
	/**
	 * <p>This function is called in the event no command line arguments were submitted; Specifically, when no cmd args are submitted,
	 * first <code>main(String[] args)</code> calls <code>userInput()</code>, and then <code>userInput()</code> calls this function.</p>
	 * 
	 * <p>This function retrieves and parses user input from its parameter '<code>Scanner</code>', and returns the parsed input - the <code>result</code></p>
	 * 
	 * @param whatToParse the element whose parameter we expect the user to input to this <code>Scanner</code>. To be concrete, in this 
	 * <code>Rectangle</code> program these would specifically be <code>width</code> and <code>height</code>.
	 * @param sc the <code>Scanner</code> used for getting user input
	 * @return result - the parameter user submitted to the <code>Scanner</code>
	 */
	private static double parseUserInput(String whatToParse, Scanner sc) {
		while(true) {
			System.out.printf("Unesite %s > ", whatToParse);
			String input = sc.nextLine();
			Double result;
			try{
				result = Double.parseDouble(input);
				if(result < 0) {
					throw new IllegalArgumentException();
				}
			} catch(NumberFormatException ex) {
				System.out.println(String.format("'%s' se ne moze protumaciti kao broj.", input));
				continue;
			}catch(IllegalArgumentException ex) {
				System.out.println("Unijeli ste negativnu vrijednost.");
				continue;
			}
			
			return result;
		}
	}
	
	/**
	 * Calculates <code>circumference</code> and <code>area</code>, and then prints following data in a human readable form :
	 * <code>width</code>, <code>height</code>, <code>circumference</code>, <code>area</code>.
	 * 
	 * @param width the <code>width</code> of this <code>Rectangle</code>
	 * @param height the <code>height</code> of this <code>Rectangle</code>
	 */
	private static void dataParsePrint(double width, double height) {
		double circumference = circumference(width, height);
		double area = area(width, height);
		
		System.out.printf("Pravokutnik sirine %.1f i visine %.1f ima "
				+ "povrsinu %.1f te opseg %.1f.%n", width, height, area, circumference);
	}
	
	/**
	 * Calculates the circumference.
	 * 
	 * @param width the <code>width</code> of this <code>Rectangle</code>
	 * @param height the <code>height</code> of this <code>Rectangle</code>
	 * @return the circumference of this <code>Rectangle</code>
	 */
	private static double circumference(double width, double height) {
		if(width<0 || height<0) {
			throw new IllegalArgumentException("Nijedan argument ne smije biti negativan...");
		}
		
		return 2*(width + height);
	}
	
	/**
	 * Calculates the area.
	 * 
	 * @param width the <code>width</code> of this <code>Rectangle</code>
	 * @param height the <code>height</code> of this <code>Rectangle</code>
	 * @return the area of this <code>Rectangle</code>
	 */
	private static double area(double width, double height) {
		if(width<0 || height<0) {
			throw new IllegalArgumentException("Nijedan argument ne smije biti negativan...");
		}
		
		return width * height;
	}

}
