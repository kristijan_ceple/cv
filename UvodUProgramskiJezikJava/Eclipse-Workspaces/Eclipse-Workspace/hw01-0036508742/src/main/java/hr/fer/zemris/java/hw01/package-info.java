/**
 * First homework of the Introduction to Java course @ FER.
 * 
 * Student: Kristijan Ceple
 * Professor: Marko Cupic
 * 
 * @since 1.0
 */
package hr.fer.zemris.java.hw01;