package hr.fer.zemris.java.pred03;

import hr.fer.zemris.java.tecaj_3.prikaz.Slika;

public abstract class GeometrijskiLik {
	
	public abstract boolean sadrziTocku(int x, int y);
	
	public void popuniLik(Slika slika) {
		int width = slika.getSirina(), height = slika.getVisina();
		
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				if(this.sadrziTocku(i, j)) {
					slika.upaliTocku(i, j);
				}
			}
		}
	}
}
