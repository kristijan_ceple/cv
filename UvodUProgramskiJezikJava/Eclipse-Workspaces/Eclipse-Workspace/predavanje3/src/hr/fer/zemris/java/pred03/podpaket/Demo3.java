package hr.fer.zemris.java.pred03.podpaket;

public class Demo3 {

	interface BiProcessor {
		void process(double value, double transformedValue);
	}
	
	public static void main(String[] args) {
		BiProcessor p2 = new BiProcessor() {
			@Override
			public void process(double value, double transformedValue) {
				System.out.printf("%f <- %f%n", transformedValue, value);
			}
		};
		
		BiProcessor p3 = (double stefica, double transformedValue) -> {
				System.out.printf("%f <- %f%n", transformedValue, stefica);
			}
		;
	}

}
