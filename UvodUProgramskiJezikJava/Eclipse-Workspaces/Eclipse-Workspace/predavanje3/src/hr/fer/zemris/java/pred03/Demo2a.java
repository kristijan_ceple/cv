package hr.fer.zemris.java.pred03;

public class Demo2a {
	
	interface Operator {
		double whatever(double d1, double d2);
	}
	
	interface Transformer {
		double transform(double value);
	}
	
	interface BiProcessor {
		void process(double value, double transformedValue);
	}
	
	private static class dodaj3 implements Transformer {
		@Override
		public double transform(double value) {
			return value + 3;
		}
	}
	
	private static class kvadriraj implements Transformer {
		@Override
		public double transform(double value) {
			return value * value;
		}
	}
	
	private static class ispisObaSaStrelicom implements BiProcessor {
		@Override
		public void process(double value, double transformedValue) {
			System.out.printf("%f -> %f%n", value, transformedValue);
		}
	}
	
	public static void main(String[] args) {
		double[] ulaz = {1, 2, 3, 4, 5, 6, 7, 8, 9 ,10};
		String operacija = "kvadriraj";		//kvadriraj
		
		Transformer t = null;
		if(operacija.equals("dodaj3")) {
			t = new dodaj3();
		} else if(operacija.equals("kvadriraj")) {
			t = new kvadriraj();
		} else {
			System.err.println("Dragi korisnice...");
			return;
		}
		BiProcessor p1 = new ispisObaSaStrelicom();
		BiProcessor p2 = new BiProcessor() {
			@Override
			public void process(double value, double transformedValue) {
				System.out.printf("%f <- %f%n", transformedValue, value);
			}
		};
		
		BiProcessor p3 = (double stefica, double transformedValue) -> {
				System.out.printf("%f <- %f%n", transformedValue, stefica);
			}
		;
		
		BiProcessor p4 = (value, transformedValue) -> System.out.printf("%f <- %f%n", transformedValue, value);
		
		obradi(ulaz, t, p2);
		
		// hmmm
		
		Operator o1 = (a,b) -> a+b;
		Operator o2 = (a,b) -> Double.sum(a, b);
		Operator o3 = 
		
		// hmmm
	}
	
	public static void obradi(double[] ulaz, Transformer t, BiProcessor bp) {
		double[] rezultat = new double[ulaz.length];
		for(int i = 0; i < ulaz.length; i++) {
			rezultat[i] = t.transform(ulaz[i]);
		}
		
		int i = 0;
		for(double broj: rezultat) {
			bp.process(ulaz[i], rezultat[i]);
			i++;
		}

	}
}
