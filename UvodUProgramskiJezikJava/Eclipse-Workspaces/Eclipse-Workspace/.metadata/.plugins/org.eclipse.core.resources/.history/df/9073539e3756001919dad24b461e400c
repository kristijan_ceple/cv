package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * A class modelling a parameterized dictionary/map/associative array. Internally is an adaptor
 * to an instance of {@link ArrayIndexedCollection}.
 * 
 * @author kikyy99
 *
 * @param <K> represents key
 * @param <V> represents value
 */
public class Dictionary<K, V> {

	/**
	 * Inner-mechanism {@link ArrayIndexedCollection} used for storage
	 */
	private ArrayIndexedCollection<Entry<K, V>> col;
	
	/**
	 * Class representing a key-value pair - used inside {@link Dictionary}
	 * @author kikyy99
	 *
	 * @param <K> represents key
	 * @param <V> represents value
	 */
	private static class Entry<K, V> {
		
		/**
		 * key
		 */
		private K key;
		/**
		 * value
		 */
		private V value;
		
		/**
		 * Constructs an entry.
		 * 
		 * @throws NullPointerException if key is null
		 * @param key key
		 * @param value value
		 */
		public Entry(K key, V value) {
			Objects.requireNonNull(key);
			
			this.key = key;
			this.value = value;
		}
	}
	
	/**
	 * Checks whether this {@link Dictionary} is empty.
	 * @return whether this {@link Dictionary} is empty.
	 */
	public boolean isEmpty() {
		return col.isEmpty();
	}
	
	/**
	 * Returns the size of this {@link Dictionary}
	 * @return the size of this {@link Dictionary}
	 */
	public int size() {
		return col.size();
	}
	
	/**
	 * Clears the {@link Dictionary}.
	 */
	public void clear() {
		col.clear();
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value) {
		int position = col.indexOf(value);
		if(position != -1) {
			//means it has been found
			col.add(new Entry(key, value));
		}
	}
	
}
