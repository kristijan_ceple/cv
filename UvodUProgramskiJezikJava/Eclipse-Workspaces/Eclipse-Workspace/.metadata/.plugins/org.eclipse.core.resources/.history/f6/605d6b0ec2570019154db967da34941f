package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw03.prob1.Lexer;
import hr.fer.zemris.java.hw03.prob1.LexerException;
import hr.fer.zemris.java.hw03.prob1.Token;
import hr.fer.zemris.java.hw03.prob1.TokenType;

class SmartScriptLexerTest {

	@Test
	public void testNotNull() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		assertNotNull(lexer.nextToken(), "Token was expected but null was returned.");
	}
	
	@Test
	public void testNullInput() {
		// must throw!
		assertThrows(NullPointerException.class, () -> new SmartScriptLexer(null));
	}

	@Test
	public void testEmpty() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType(), "Empty input must generate only EOF token.");
	}
	
	@Test
	public void testGetReturnsLastNext() {
		// Calling getToken once or several times after calling nextToken must return each time what nextToken returned...
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		SmartScriptToken token = lexer.nextToken();
		assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
		assertEquals(token, lexer.getToken(), "getToken returned different token than nextToken.");
	}
	
	@Test
	public void testRadAfterEOF() {
		SmartScriptLexer lexer = new SmartScriptLexer("");

		// will obtain EOF
		lexer.nextToken();
		// will throw!
		assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	public void testOneStringText() {
		// When input is only of spaces, tabs, newlines, etc...
		SmartScriptLexer lexer = new SmartScriptLexer("   \r\n\t    ");
		
		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType(), "String_text token should have been generated.");
		assertEquals("   \r\n\t    ", lexer.getToken().getValue());
	}
	
	@Test
	public void testEscapeTextOnly() {
		SmartScriptLexer lexer = new SmartScriptLexer("   \r\n\t     aaaa"
				+ "eeenhnjnh \\\\yoo \\{$ fake\n\t\n\n\t\r tag $} \n\najaja");
		
		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType(), "String_text token should have been generated.");
		assertEquals("   \r\n\t     aaaa"
				+ "eeenhnjnh \\yoo {$ fake\n\t\n\n\t\r tag $} \n\najaja", lexer.getToken().getValue());
	}
	
	@Test
	public void testTextAndTag() {
		SmartScriptLexer lexer = new SmartScriptLexer("Some \\\\ test X");
		
		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType(), "String_text token should have been generated.");
		assertEquals("Some \\ test X", lexer.getToken().getValue());
	}
	
	@Test
	public void testTagText() {
		SmartScriptLexer lexer = new SmartScriptLexer("\"Joe \\\"Long\\\" Smith\"");
		
		lexer.setState(SmartScriptLexerState.TAG_TEXT);
		assertEquals(SmartScriptTokenType.SYMBOL, lexer.nextToken().getType());
		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType());
		assertEquals("Joe \"Long\" Smith", lexer.getToken().getValue());
		assertEquals(SmartScriptTokenType.SYMBOL, lexer.nextToken().getType());
	}
	
	@Test
	public void testMultipleTextAndTags() {
		String testString = "A tag follows {$= \"Joe \\\"Long\\\" Smith\"$}.";
		SmartScriptLexer lexer = new SmartScriptLexer(testString);
		
		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType());
		assertEquals("A tag follows ", lexer.getToken().getValue());
		assertEquals(SmartScriptLexer.TAG_OPENING, lexer.nextToken().getValue());
		lexer.setState(SmartScriptLexerState.TAG_TEXT);
		checkTokenStream(
				lexer,
				new SmartScriptToken[]{
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, "$"),
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, "="),
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, "\""),
						new SmartScriptToken(SmartScriptTokenType.STRING_TEXT, "Joe \\\"Long\\\" Smith"),
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, "\""),
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, "$"),
						new SmartScriptToken(SmartScriptTokenType.SYMBOL, SmartScriptLexer.TAG_CLOSING),
					}
				);
		lexer.setState(SmartScriptLexerState.BASIC_TEXT);

		assertEquals(SmartScriptTokenType.STRING_TEXT, lexer.nextToken().getType());
		assertEquals(".", lexer.getToken().getValue());
		assertEquals(SmartScriptTokenType.EOF, lexer.nextToken().getType());
	}
	//##########################	HELPER METHODS		################################
	// Helper method for checking if lexer generates the same stream of tokens
	// as the given stream.
	private void checkTokenStream(SmartScriptLexer lexer, SmartScriptToken[] correctData) {
		int counter = 0;
		for(SmartScriptToken expected : correctData) {
			SmartScriptToken actual = lexer.nextToken();
			String msg = "Checking token "+counter + ":";
			assertEquals(expected.getType(), actual.getType(), msg);
			assertEquals(expected.getValue(), actual.getValue(), msg);
			counter++;
			System.out.println(msg);
		}
	}

	private void checkToken(Token actual, Token expected) {
		String msg = "Token are not equal.";
		assertEquals(expected.getType(), actual.getType(), msg);
		assertEquals(expected.getValue(), actual.getValue(), msg);
	}
}
