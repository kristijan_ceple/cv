package hr.fer.zemris.java.custom.scripting.parser;

import com.sun.org.apache.xerces.internal.dom.ElementDefinitionImpl;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerState;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.proto.lexer.Lexer;

/**
 * Parser for the <code>SmartScript</code> language.
 * @author kikyy99
 *
 */
public class SmartScriptParser {
	
		/**
		 * The input to be parsed
		 */
		private String input;
		/**
		 * The lexer for tokenisation
		 */
		private SmartScriptLexer lexer;
		/**
		 * The helping stack
		 */
		private ObjectStack stack;
		/**
		 * Specifies whether we're already located inside the tag
		 */
		private boolean insideTag;
	
		public SmartScriptParser(String input) {
			if(input == null) {
				throw new SmartScriptParserException("Error: NULL input passed to the parser!");
			}
			
			this.input = input;
			this.lexer = new SmartScriptLexer(input);
			stack = new ObjectStack();
			
			parse();
		}
		
		private void parse() throws SmartScriptParserException{
			try {
				//at the beginning put a document node -- we're in BASIC mode
				DocumentNode startNode = new DocumentNode();
				stack.push(startNode);
				
				//let's add other nodes right now
				while(lexer.getToken().getType()!=SmartScriptTokenType.EOF) {
					SmartScriptToken token = lexer.nextToken();
					Node parent = (Node)stack.peek();
					
//					if(lexer.getState()==SmartScriptLexerState.BASIC_TEXT) {
//						//only text or tag beginning encountered
//					} else if(lexer.getState()==SmartScriptLexerState.TAG_TEXT) {
//						
//					} else {
//						throw new SmartScriptParserException("Unallowed lexer state!");
//					}
					
					//let's differentiate them now
					if(token.getType()==SmartScriptTokenType.STRING_TEXT) {
						Node pushToStack = new TextNode((String)token.getValue());
						parent.addChildNode(pushToStack);
						//no need to push this node to the stack
					} else if(token.getType()==SmartScriptTokenType.TAG_OPENING) {
						if(!insideTag) {
							parseTag(parent);
						} else {
							throw new SmartScriptParserException("Tag inside tag NOT ALLOWED!");
						}
					} else if(token.getType()==SmartScriptTokenType.SYMBOL) {
						checkSymbol(token, parent);
					} 
				}
				
			} catch(Exception e) {
				throw new SmartScriptParserException("Exception thrown during parsing: " + e.getMessage());
			}
		}
		
		/**
		 * Helper functions used for parsing tags
		 * @param parent the parent {@link Node}from the stack
		 */
		private void parseTag(Node parent){
			lexer.setState(SmartScriptLexerState.TAG_TEXT);
			SmartScriptToken nextToken = lexer.nextToken();
			
			if(!(nextToken.getType()!=SmartScriptTokenType.SYMBOL && 
					((Character)nextToken.getValue()).equals('$'))) {
				throw new SmartScriptParserException("Tag opener must be followed "
						+ "by an appropriate sign!");
			}
			//so far so good - continue parsing rest of the tag
			parseRestOfTag(parent);
		}
		
		/**
		 * Checks symbols
		 * @param token the symbol token to be checked
		 * @param parent from the stack
		 */
		private void checkSymbol(SmartScriptToken token, Node parent) {
			//check what symbol this is:
			//if the previous element was a string, and this symbol is
			//the tag opening character - then a tag follows
			Character character = (Character)token.getValue();
			
		}
		
		/**
		 * Helps parse the rest of the tag after its begining has been encountered
		 * @param parent the parent node from the stack which this tag is a child of
		 */
		private void parseRestOfTag(Node parent) {
			//check type of tag
			SmartScriptToken nextToken = lexer.nextToken();
			
			//TAG_TYPE must follow
			if(nextToken.getType() != SmartScriptTokenType.TAG_TYPE) {
				throw new SmartScriptParserException("Tag type must follow after "
						+ "tag opener and dollar sign!");
			}
			
			//read the tag and check if it's a FOR tag to push it onto stack
			String tagName = (String)nextToken.getValue();
			if(tagName.equals("FOR")) {
				ForLoopNode forNode = new ForLoopNode();
				parent.addChildNode(forNode);
				stack.push(forNode);
				parseForNode(forNode);
				lexer.setState(SmartScriptLexerState.BASIC_TEXT);
				return;
			} else if(tagName.equals("=")) {
				//just add as child - EchoNode
				EchoNode echoChild = new EchoNode();
				parent.addChildNode(echoChild);
				return;
			} else {
				
			}
		}
		
		/**
		 * Helper method for parsing for loop tags.
		 * We expect 1 element variable, and after that 2 or 3
		 * {@link hr.fer.zemris.java.custom.scripting.elems#Element}s of types
		 * variable, number or string.
		 * 
		 * @param forNode the node to be parsed
		 */
		private void parseForNode(ForLoopNode forNode) {
			SmartScriptToken nextToken = lexer.nextToken();
			if(nextToken.getType()!=SmartScriptTokenType.VARIABLE) {
				throw new SmartScriptParserException("Variable expected after FOR tag!");
			}
			
			//2 or 3 more
			SmartScriptToken second = lexer.nextToken();
			Element secondElem = checkForLoopOtherTokens(second);
			SmartScriptToken third = lexer.nextToken();
			Element thirdElem = checkForLoopOtherTokens(third);
			
			SmartScriptToken fourth = lexer.nextToken();
			SmartScriptTokenType fourthType = fourth.getType();
			if(fourthType == SmartScriptTokenType.SYMBOL && ((Character)fourth.getValue()).equals('$')) {
				//end of a for tag, a tag_closing symbol must follow
				SmartScriptToken tagCloser = lexer.nextToken();
				if(tagCloser.getType()!=SmartScriptTokenType.TAG_CLOSING) {
					throw new SmartScriptParserException("A tag closer must follow the $ sign!");
				}
				forNode.setVariable(new ElementVariable((String)nextToken.getValue()));
				forNode.setStartExpression(secondElem);
				forNode.setEndExpression(thirdElem);
				return;
			}
			//else we have the fourth element as well, and then comes the end
			Element fourthElem = checkForLoopOtherTokens(fourth);
			forNode.setVariable(new ElementVariable((String)nextToken.getValue()));
			forNode.setStartExpression(secondElem);
			forNode.setStepExpression(thirdElem);
			forNode.setEndExpression(fourthElem);
			return;
		}
		
		private Element checkForLoopOtherTokens(SmartScriptToken token) {
			SmartScriptTokenType type = token.getType();
			if(!(type == SmartScriptTokenType.VARIABLE
					|| type == SmartScriptTokenType.CONSTANT_DOUBLE
					|| type == SmartScriptTokenType.CONSTANT_INTEGER
					|| type == SmartScriptTokenType.STRING_TEXT)) {
				throw new SmartScriptParserException("Only variable, number(integer and double) "
						+ "and string are permitted after the first variable following the FOR tag!");
			}
			
			if(type == SmartScriptTokenType.VARIABLE) {
				return new ElementVariable((String)token.getValue());
			} else if(type == SmartScriptTokenType.CONSTANT_DOUBLE) {
				return new ElementConstantDouble((Double)token.getValue());
			} else if(type == SmartScriptTokenType.CONSTANT_INTEGER) {
				return new ElementConstantInteger((Integer)token.getValue());
			} else if(type == SmartScriptTokenType.STRING_TEXT) {
				return new ElementString((String)token.getValue());
			}
			
		}
}
