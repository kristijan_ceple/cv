package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

/**
 * A class representing a single {@code Token}.
 * 
 * @author kikyy99
 *
 */
public class SmartScriptToken {
	
	/**
	 * The type of this token
	 */
	private SmartScriptTokenType tokenType;
	/**
	 * The actual token itself
	 */
	private Object value;


	/**
	 * 2-argument constructor. 
	 * @param tokenType the type of the token
	 * @param value the actual token itself
	 */
	public SmartScriptToken(SmartScriptTokenType tokenType, Object value) {
		if(tokenType == null) {
			throw new IllegalArgumentException("Token type can not be null!");
		}
		
		switch(tokenType) {
		case EOF:
			if(value != null) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case VARIABLE:
			if(!(value instanceof String)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case CONSTANT_INTEGER:
			if(!(value instanceof Integer)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case CONSTANT_DOUBLE:
			if(!(value instanceof Double)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case STRING_TEXT:
			if(!(value instanceof String)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case FUNCTION:
			if(!(value instanceof String)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			break;
		case OPERATOR:
			//need to check if it's one of valid operators
			if(!(value instanceof SmartScriptOperator)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			
			break;
		case TAG_OPENING:
			if(!(value instanceof String) || !value.equals(Character.toString(SmartScriptLexer.TAG_OPENING))) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			
			break;
		case TAG_CLOSING:
			if(!(value instanceof String) || !value.equals(Character.toString(SmartScriptLexer.TAG_CLOSING))) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			
			break;
		case SYMBOL:
			if(!(value instanceof String || value instanceof Character)) {
				throw new IllegalArgumentException("Unallowed passed arguments "
						+ "token type - value pair!");
			}
			
			if(value instanceof String) {
				String tempString = (String) value;
				this.value = tempString.charAt(0);		//want them to be characters for speed's sake
			} else {
				this.value = value;
			}
			this.tokenType = tokenType;
			return;
		default:
			throw new IllegalArgumentException("Unallowed passed arguments "
					+ "token type - value pair!");
		}
		
		this.tokenType = tokenType;
		this.value = value;
	}
	
	/**
	 * Returns the actual value itself of the token
	 * @return the actual value itself of the token
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Returns the type of the token
	 * @return the type of the token
	 */
	public SmartScriptTokenType getType() {
		return tokenType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(tokenType, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SmartScriptToken))
			return false;
		SmartScriptToken other = (SmartScriptToken) obj;
		return tokenType == other.tokenType && Objects.equals(value, other.value);
	}
	
	
}
