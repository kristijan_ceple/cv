package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerException;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerState;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptOperator;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * Parser for the <code>SmartScript</code> language.
 * @author kikyy99
 *
 */
public class SmartScriptParser {
	
		/**
		 * The input to be parsed
		 */
		private String input;
		/**
		 * The lexer for tokenisation
		 */
		private SmartScriptLexer lexer;
		/**
		 * The helping stack
		 */
		private ObjectStack stack;
		/**
		 * Specifies whether we're already located inside the tag
		 */
		public static final int ECHO_DEFAULT_CAPACITY = 128;
		/**
		 * The root of the tree
		 */
		public DocumentNode root;
	
		public SmartScriptParser(String input) {
			if(input == null) {
				throw new SmartScriptParserException("Error: NULL input passed to the parser!");
			}
			
			this.input = input;
			this.lexer = new SmartScriptLexer(input);
			stack = new ObjectStack();
			
			parse();
		}
		
		private void parse() throws SmartScriptParserException{
			try {
				//at the beginning put a document node -- we're in BASIC mode
				root = new DocumentNode();
				stack.push(root);
				
				//let's add other nodes right now
				SmartScriptToken token = lexer.nextToken();
				Node parent = (Node)stack.peek();
				while(lexer.getToken().getType()!=SmartScriptTokenType.EOF) {
//					if(lexer.getState()==SmartScriptLexerState.BASIC_TEXT) {
//						//only text or tag beginning encountered
//					} else if(lexer.getState()==SmartScriptLexerState.TAG_TEXT) {
//						
//					} else {
//						throw new SmartScriptParserException("Unallowed lexer state!");
//					}
					
					//let's differentiate them now
					if(token.getType()==SmartScriptTokenType.STRING_TEXT) {
						Node pushToStack = new TextNode((String)token.getValue());
						parent.addChildNode(pushToStack);
						//no need to push this node to the stack
					} else if(token.getType()==SmartScriptTokenType.TAG_OPENING) {
						parseTag(parent);
					} else {
						throw new SmartScriptParserException("Unallowed token returned!");
					}

					token = lexer.nextToken();
					parent = (Node)stack.peek();
				}
				
				//EOF encountered - check the stack for unclosed FOR tags. Only documentNode should be present
				if(stack.size() > 1) {
					throw new SmartScriptParserException("Unclosed FOR tags!");
				}
				
			} catch(Exception e) {
				throw new SmartScriptParserException("Exception thrown during parsing: " + e.getMessage());
			}
		}
		
		/**
		 * Helper functions used for parsing tags
		 * @param parent the parent {@link Node}from the stack
		 */
		private void parseTag(Node parent){
			lexer.setState(SmartScriptLexerState.TAG_TEXT);
			SmartScriptToken nextToken = lexer.nextToken();
			
			if(!(nextToken.getType()!=SmartScriptTokenType.SYMBOL && 
					((Character)nextToken.getValue()).equals('$'))) {
				throw new SmartScriptParserException("Tag opener must be followed "
						+ "by an appropriate sign!");
			}
			//so far so good - continue parsing rest of the tag
			parseRestOfTag(parent);
		}
		
		/**
		 * Helps parse the rest of the tag after its begining has been encountered
		 * @param parent the parent node from the stack which this tag is a child of
		 */
		private void parseRestOfTag(Node parent) {
			//check type of tag
			SmartScriptToken nextToken = lexer.nextToken();
			
			//TAG_TYPE must follow
			if(nextToken.getType() != SmartScriptTokenType.TAG_TYPE) {
				throw new SmartScriptParserException("Tag type must follow after "
						+ "tag opener and dollar sign!");
			}
			
			//read the tag and check if it's a FOR tag to push it onto stack
			String tagName = (String)nextToken.getValue();
			if(tagName.equalsIgnoreCase("FOR")) {
				ForLoopNode forNode = new ForLoopNode();
				parseForNode(forNode);
				
				parent.addChildNode(forNode);
				stack.push(forNode);
			} else if(tagName.equalsIgnoreCase("=")) {
				//just add as child - EchoNode
				Element[] elements = echoGatherElements();
				EchoNode echoChild = new EchoNode(elements);
				
				//check for tag closer
				SmartScriptToken token = lexer.nextToken();
				if(token.getType() != SmartScriptTokenType.TAG_CLOSING) {
					throw new SmartScriptLexerException("A tag closer must follow after the "
							+ "$ sign!");
				}
				parent.addChildNode(echoChild);
			} else if(tagName.equalsIgnoreCase("END")) {
				//next two tokens have to be $ and tag closer
				
				//pop one from the stack - IT HAS TO BE A FOR NODE
				Object popped = stack.pop();
				if(!(popped instanceof ForLoopNode)) {
					throw new SmartScriptParserException("No corresponding forLoopNode for End tag?!?");
				}
				//check for $ and tag closer
				
				SmartScriptToken token = lexer.nextToken();
				if(token.getType()!=SmartScriptTokenType.SYMBOL && ((Character)token.getValue()).equals('$')) {
					throw new SmartScriptParserException("$ sign expected after the \"END\" string!");
				}
				
				token = lexer.nextToken();
				if(token.getType()!=SmartScriptTokenType.TAG_CLOSING) {
					throw new SmartScriptParserException("A tag closer "
							+ "expected after the $ sign!");
				}
			} else {
				throw new SmartScriptParserException("No additional tags supported yet!");
			}
			
			lexer.setState(SmartScriptLexerState.BASIC_TEXT);
		}
		
		private Element[] echoGatherElements() {
			//gather elements until the end of tag, check for end of tag
			Element[] elements = new Element[ECHO_DEFAULT_CAPACITY];
			SmartScriptToken token;
			token = lexer.nextToken();
			for(int i = 0; token.getType() != SmartScriptTokenType.SYMBOL
					&& ((Character)token.getValue()).equals('$'); i++) {
				Element element = differentiateElement(token);
				elements[i] = element;
				token = lexer.nextToken();
			}
			
			return elements;
		}
		
		private Element differentiateElement(SmartScriptToken token) {
			Element toReturn;
			
			switch(token.getType()) {
			case CONSTANT_DOUBLE:
				toReturn = new ElementConstantDouble((Double)token.getValue());
				break;
			case CONSTANT_INTEGER:
				toReturn = new ElementConstantInteger((Integer)token.getValue());
				break;
			case FUNCTION:
				toReturn = new ElementFunction((String)token.getValue());
				break;
			case OPERATOR:
				toReturn = new ElementOperator(((Character)((SmartScriptOperator)token.getValue()).getValue()).toString());
				break;
			case STRING_TEXT:
				toReturn = new ElementString((String)token.getValue());
				break;
			case VARIABLE:
				toReturn = new ElementVariable((String)token.getValue());
				break;
			default:
				throw new SmartScriptParserException("Can't differentiate element inside a tag!");
			}
			
			return toReturn;
		}
		
		/**
		 * Helper method for parsing for loop tags.
		 * We expect 1 element variable, and after that 2 or 3
		 * {@link hr.fer.zemris.java.custom.scripting.elems#Element}s of types
		 * variable, number or string.
		 * 
		 * @param forNode the node to be parsed
		 */
		private void parseForNode(ForLoopNode forNode) {
			SmartScriptToken nextToken = lexer.nextToken();
			if(nextToken.getType()!=SmartScriptTokenType.VARIABLE) {
				throw new SmartScriptParserException("Variable expected after FOR tag!");
			}
			
			//2 or 3 more
			SmartScriptToken second = lexer.nextToken();
			Element secondElem = checkForLoopOtherTokens(second);
			SmartScriptToken third = lexer.nextToken();
			Element thirdElem = checkForLoopOtherTokens(third);
			
			SmartScriptToken fourth = lexer.nextToken();
			SmartScriptTokenType fourthType = fourth.getType();
			if(fourthType == SmartScriptTokenType.SYMBOL && ((Character)fourth.getValue()).equals('$')) {
				//end of a for tag, a tag_closing symbol must follow
				SmartScriptToken tagCloser = lexer.nextToken();
				if(tagCloser.getType()!=SmartScriptTokenType.TAG_CLOSING) {
					throw new SmartScriptParserException("A tag closer must follow the $ sign!");
				}
				forNode.setVariable(new ElementVariable((String)nextToken.getValue()));
				forNode.setStartExpression(secondElem);
				forNode.setEndExpression(thirdElem);
				return;
			}
			//else we have the fourth element as well, and then comes the end
			Element fourthElem = checkForLoopOtherTokens(fourth);
			forNode.setVariable(new ElementVariable((String)nextToken.getValue()));
			forNode.setStartExpression(secondElem);
			forNode.setStepExpression(thirdElem);
			forNode.setEndExpression(fourthElem);
			return;
		}
		
		/**
		 * 
		 * @param token
		 * @return
		 */
		private Element checkForLoopOtherTokens(SmartScriptToken token) {
			SmartScriptTokenType type = token.getType();
			if(!(type == SmartScriptTokenType.VARIABLE
					|| type == SmartScriptTokenType.CONSTANT_DOUBLE
					|| type == SmartScriptTokenType.CONSTANT_INTEGER
					|| type == SmartScriptTokenType.STRING_TEXT)) {
				throw new SmartScriptParserException("Only variable, number(integer and double) "
						+ "and string are permitted after the first variable following the FOR tag!");
			}
			
			if(type == SmartScriptTokenType.VARIABLE) {
				return new ElementVariable((String)token.getValue());
			} else if(type == SmartScriptTokenType.CONSTANT_DOUBLE) {
				return new ElementConstantDouble((Double)token.getValue());
			} else if(type == SmartScriptTokenType.CONSTANT_INTEGER) {
				return new ElementConstantInteger((Integer)token.getValue());
			} else if(type == SmartScriptTokenType.STRING_TEXT) {
				return new ElementString((String)token.getValue());
			}
			
			return null;
		}

		/**
		 * Stack getter
		 * @return stack
		 */
		public ObjectStack getStack() {
			return stack;
		}

		/**
		 * Root getter
		 * @return root
		 */
		public DocumentNode getRoot() {
			return root;
		}
		
		
		
}
