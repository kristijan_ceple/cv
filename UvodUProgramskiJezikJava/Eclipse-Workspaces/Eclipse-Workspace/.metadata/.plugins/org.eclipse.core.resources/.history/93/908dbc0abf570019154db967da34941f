package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Arrays;
import java.util.Objects;

import hr.fer.zemris.java.hw03.prob1.LexerException;
import hr.fer.zemris.java.hw03.prob1.Token;
import hr.fer.zemris.java.hw03.prob1.TokenType;

/**
 * A lexer - splits the input into {@link SmartScriptToken} instances and returns them one by
 * one upon calling its method {@link SmartScriptLexer#nextToken()}.
 * @author kikyy99
 *
 */
public class SmartScriptLexer {
	/**
	 * Marks the beginning of a string inside tags
	 */
	public static final char STRING_ENCLOSE_SIGN = '"';
	/**
	 * Signals tag beginning
	 */
	public static final char TAG_OPENING = '{';
	/**
	 * Signals end of tag
	 */
	public static final char TAG_CLOSING = '}';
	/**
	 * The escape character
	 */
	public static final char ESCAPE_CHAR = '\\';
	/**
	 * Annotates functions
	 */
	public static final char FUNCTION_ANNOTATION = '@';
	/**
	 * Specifies in which state is the {@link SmartScriptLexer} located. 
	 */
	private SmartScriptLexerState state = SmartScriptLexerState.BASIC_TEXT;
	/**
	 * Entry text/characters
	 */
	private char[] data;
	/**
	 * The current token
	 */
	private SmartScriptToken token;
	/**
	 * Index of the first non-processed character
	 */
	private int currentIndex;

	public SmartScriptLexer(String text) {
		if(text == null) {
			throw new NullPointerException("Null passed as input to the Lexer.");
		}
		
		this.data = text.toCharArray();
	}
	
	/**
	 * Returns the next {@link SmartScriptToken}
	 * @return the next {@link SmartScriptToken}
	 */
	public SmartScriptToken nextToken() {
		for(int i = currentIndex; i < data.length; i++) {
			if(state == SmartScriptLexerState.BASIC_TEXT) {
				//we're in basic state - we encounter only strings and tags
				if(data[i]==ESCAPE_CHAR) {
					//need to check for i+1
					if(i+1 < data.length) {
						//check what character it is escaping
						if(data[i+1]==ESCAPE_CHAR || data[i+1]==TAG_OPENING) {
							//valid characters - start parsing from that spot
							currentIndex = i+2;
							token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, data[i+1]);
							return token;
						} else{
							throw new SmartScriptLexerException("Escape attempt on an unescapable character: " + data[i+1] + "!");
						}
					} else {
						throw new SmartScriptLexerException("Lone \\ encountered!");
					}
				} else if(data[i] == TAG_OPENING){
					/*
					 * We've encountered the tag
					 */
					
					currentIndex = i+1;
					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, data[i]);
					return token;
				} else {
					//everything else is a string yeeay
					currentIndex = readString(i);
					token = tokeniseString(i, currentIndex);
					return token;
				}
			} else if(state == SmartScriptLexerState.TAG_TEXT) {
				//we're in the TAG state - we encounter vars, func,...
				if(Character.isWhitespace(data[i])) {
					i = skipWhitespaces(i) - 1;		//skip all whitespaces
					continue;
//				} else if(data[i]=='$') {
//					currentIndex = i+1;
//					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, "$");
//					return token;
//				} else if(data[i]=='=') {
//					currentIndex = i+1;
//					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, "=");
//					return token;
//			    } else if(data[i]=='"') {
//					//beginning of a string
//					currentIndex = i+1;
//					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, "\"");
//					return token;
				} else if(i-1 >= 0 && data[i-1]=='"') {
					//the string itself. ' " ' are unescapable in tags so this will hopefully not explode
					currentIndex = readString(i);
					token = tokeniseString(i, currentIndex);
					return token;
				} else if(Character.isDigit(data[i])) {
					//spotted a number - still not sure if its integer or double
					currentIndex = readAndTokeniseNumber(i);
					return token;
				} else if(data[i]=='+' || data[i]=='-') {
					//need to check if a digit follows!
					if(i+1 < data.length && Character.isDigit(data[i+1])) {
						currentIndex = readAndTokeniseNumber(i);
						return token;
					} else {
						//else we're dealing with symbols
						currentIndex = i+1;
						token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, data[i]);
						return token;
					}
				} else if(data[i]==FUNCTION_ANNOTATION) {
					currentIndex = i+1;
					return new SmartScriptToken(SmartScriptTokenType.FUNCTION, FUNCTION_ANNOTATION);
				} else if(i-1 >= 0 &&  data[i-1]==FUNCTION_ANNOTATION) {
					//we've encountered a function!
					currentIndex = readVariableAndFunctionName(i);
					token = tokeniseFunction(i, currentIndex);
					return token;
				} else if(Character.isLetter(data[i])) {
					//beginning of a variable name - check it
					currentIndex = readVariableAndFunctionName(i);
					token = tokeniseVariable(i, currentIndex);
					return token;
//				} else if(data[i] == TAG_CLOSING) {
//					currentIndex = i+1;
//					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, TAG_CLOSING);
				} else {
					//let's treat is as a symbol since we've covered all the other cases???
					currentIndex = i+1;
					token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, data[i]);
					return token;
				}
			} else {
				throw new LexerException("Unallowed lexer state!");
			}
		}
		
		//generate an EOF token
		if(token == null || token.getType() != SmartScriptTokenType.EOF) {
			//Input has been successfully parsed or is empty --> GENERATE EOF!!!
			currentIndex = data.length;
			return tokenizeEOF();
		} else {
			//WRONG! end of input already reached
			throw new SmartScriptLexerException("Input already processed - its end has already been reached, and/or an EOF generated!");
		}
	}
	
	/**
	 * Reads a {@link SmartScriptTokenType#VARIABLE} and {@link SmartScriptTokenType#FUNCTION} name
	 * according to the following rules - the first character MUST be a letter, and then follows
	 * any number of letter, digits and/or underscores.
	 * @param startIndex the starting index, inclusive
	 * @return the end index, exclusive
	 */
	private int readVariableAndFunctionName(int startIndex) {
		int i = startIndex;
		
		if(i+1>=data.length) {
			throw new LexerException("Function/variable name not declared at the end of input!");
		}
		if(!Character.isLetter(data[i+1])) {		//MUST be a letter
			throw new LexerException("First letter of a function/variable name MUST be a letter!");
		}
		
		//now letters, numbers and underscores are permitted
		while(i < data.length) {
			if(!(Character.isAlphabetic(data[i]) || data[i]=='_')) {
				break;		//if the current char is not either a letter, number or underscore - end of name reached
			}
			i++;
		}
		
		return i;
	}
		
	/**
	 * Helper method used to check if a number comes immediately after the sign.
	 * 
	 * @param i the index of the sign
	 * @throws LexerException if false;
	 * @return true; otherwise throws
	 */
	private boolean checkNumberAfterSign(int i) {
		if(i + 1 < data.length) {
			if(!Character.isDigit(data[i+1])) {
				throw new LexerException("A number must follow after the sign!");
			}
		} else {
			throw new LexerException("Number not finished at end of input!");
		}
		
		return true;
	}
	
	/**
	 * Reads and tokenises {@link SmartScriptTokenType#CONSTANT_DOUBLE} and {@link SmartScriptTokenType#CONSTANT_INTEGER}.
	 * 
	 * @param startIndex the starting index, inclusive
	 * @return the end index, exclusive
	 */
	private int readAndTokeniseNumber(int startIndex) {
		int i = startIndex+1;	//skip the first digit or sign

//		boolean negative = false;
//		//can be a digit, or a sign
//		//need to check if a digit follows the sign in both cases
//		if(data[i] == '+') {
//			checkNumberAfterSign(i);
//			i+=2;			//i -> sign, i+1 -> digit, i+2 -> check it!
//		} else if(data[i] == '-') {
//			checkNumberAfterSign(i);
//			negative = true;
//			i+=2;			//i -> sign, i+1 -> digit, i+2 -> check it!
//		} else {
//			i++;	//move on
//		}
		
		//we already know about the 1st place - sign or a number. In case
		//the user submits +.2 or -...2 or something like that, Double and Int
		//parse will catch such errors, and the exc shall be rethrown
		//now need to check the rest.
		boolean valid = true;
		boolean dotEncountered = false;
		while(i < data.length) {
			if(Character.isDigit(data[i])) {
				valid = true;
				i++;
			} else if(data[i] == '.' && dotEncountered == false) {
				valid = false;				//used to check if a digit follow after a dot, e.g.: 2. -> NO; 2.1 -> YES, good input
				dotEncountered = true;		//used to check if we have EXACTLY 1 dot and to differentiate between integers and doubles
				i++;
			} else {
				//reached the end of this number
				break;
			}
		}
		
		if(!valid) {
			throw new SmartScriptLexerException("A number must follow afer the dot!");
		}
		
		//###########################	2nd approach	#########################################
//		while(i < data.length && (Character.isDigit(data[i]) || data[i]=='.')) {
//			if(data[i]=='.') {
//				dotEncountered = true;
//			}
//			i++;
//		}
		//###########################	2nd approach	#########################################
		
		char[] numberCharArray = Arrays.copyOfRange(data, startIndex, i);
		String numberString = String.valueOf(numberCharArray);
		
		//make either an integer, or a double token
		try{
			token = dotEncountered ? new SmartScriptToken(SmartScriptTokenType.CONSTANT_DOUBLE, Double.parseDouble(numberString)) 
				: new SmartScriptToken(SmartScriptTokenType.CONSTANT_INTEGER, Integer.parseInt(numberString));
		} catch(NumberFormatException ex) {
			throw new SmartScriptLexerException(ex.getMessage());
		}
		
		return i;
	}
			
	/**
	 * Tokenises the substring of the input in interval [startIndex, endIndex>.
	 * 
	 * @param startIndex inclusive - the starting index
	 * @param endIndex exclusive - the ending index
	 * @return a new {@link SmartScriptTokenToken} of type {@link SmartScriptTokenType#STRING_TEXT}
	 */
	private SmartScriptToken tokeniseString(int startIndex, int endIndex) {
		char[] toTokenise = Arrays.copyOfRange(data, startIndex, endIndex);
		StringBuilder sb = new StringBuilder();
		boolean firstBackslash = true;
		//iterate over this string, and check for escaped characters
		//readString method made sure that all escaped characters are in a proper form
		
		//escaped characters are \, { and ". Basically just need to make sure about the starting \
		int i = 0;
		while(i < toTokenise.length) {
			if(data[i] == ESCAPE_CHAR && firstBackslash) {
				//skip it
				i++;
				firstBackslash = false;
				continue;
			} else {
				firstBackslash = true;		//other bslashes after this one shall be first in the row
				sb.append(data[i++]);
				continue;
			}
		}
		
		String toTokenizeString = sb.toString();
		return new SmartScriptToken(SmartScriptTokenType.STRING_TEXT, toTokenizeString);
	}
	
	/**
	 * Tokenises the substring of the input in interval [startIndex, endIndex>.
	 * 
	 * @param startIndex inclusive - the starting index
	 * @param endIndex exclusive - the ending index
	 * @return a new {@link SmartScriptTokenToken} of type {@link SmartScriptTokenType#FUNCTION}
	 */
	private SmartScriptToken tokeniseFunction(int startIndex, int endIndex) {
		String toTokenizeString = extractFromArrayIntoString(startIndex, endIndex);
		return new SmartScriptToken(SmartScriptTokenType.FUNCTION, toTokenizeString);
	}
	
	/**
	 * Tokenises the substring of the input in interval [startIndex, endIndex>.
	 * 
	 * @param startIndex inclusive - the starting index
	 * @param endIndex exclusive - the ending index
	 * @return a new {@link SmartScriptTokenToken} of type {@link SmartScriptTokenType#VARIABLE}
	 */
	private SmartScriptToken tokeniseVariable(int startIndex, int endIndex) {
		String toTokenizeString = extractFromArrayIntoString(startIndex, endIndex);
		return new SmartScriptToken(SmartScriptTokenType.VARIABLE, toTokenizeString);
	}
	
	/**
	 * Extracts a substring of the input, and returns it as an 
	 * instance of a {@link String}
	 * 
	 * @param startIndex inclusive - the starting index
	 * @param endIndex exclusive - the ending index
	 * @return the specified interval of the input as a {@link String}
	 */
	private String extractFromArrayIntoString(int startIndex, int endIndex) {
		char[] toTokenise = Arrays.copyOfRange(data, startIndex, endIndex);
		String toTokenizeString = String.valueOf(toTokenise);
		return toTokenizeString;
	}
	
	/**
	 * Reads the string starting ON the index {@code startIndex} - inclusive.
	 * 
	 * @param startIndex inclusive - the starting index from which a {@link String} shall be read
	 * @return the index of the next token after this {@link String}
	 */
	private int readString(int startIndex) {
		//depending on the state we're in - parse Strings differently
		int i = startIndex;
		while(i < data.length) {
			if(state == SmartScriptLexerState.BASIC_TEXT) {
				//read everything as is, just take care of escapes
				if(data[i]==ESCAPE_CHAR) {
					//check if next char exists
					if(i+1 < data.length) {
						if(data[i+1]==ESCAPE_CHAR || data[i+1]==TAG_OPENING) {
							//allowed signs
							i+=2;
							continue;
						} else {
							throw new SmartScriptLexerException("Escape attempt on an unescapable character: " + data[i+1] + "!");
						}
					} else {
						throw new SmartScriptLexerException("Lone \\ encountered at the end of input!");
					}
				} else if(data[i]==TAG_OPENING) {
					//end of string
					return i;
				} else {
					//all other characters permitted
					i++;
					continue;
				}
			} else if(state == SmartScriptLexerState.TAG_TEXT) {
				/*
				 * We're in TAG state - different rules.
				 * Go from " to the next ". The first " has already been encountered - so we read on until another " is encountered.
				 * Take care of escaping " and \
				 */
				if(data[i]==STRING_ENCLOSE_SIGN) {
					//we've come to an end of the string
					return i;
				} else if(data[i]==ESCAPE_CHAR) {
					//escape sign - check if " or another \ follow
					if(i+1 < data.length) {
						if(data[i+1]==ESCAPE_CHAR || data[i+1]=='"') {
							i+=2;
							continue;
						} else {
							throw new SmartScriptLexerException("Escape attempt on an unescapable character: " + data[i+1] + "!");
						}
					} else {
						throw new SmartScriptLexerException("Lone \\ encountered at the end of input!");
					}
				} else {
					//everything else allowed - game on
					i++;
					continue;
				}
			} else {
				throw new LexerException("Unallowed lexer state!");
			}
		}
		
		return i;
	}
	
	/**
	 * Creates and returns an {@link TokenType#EOF} {@link Token}
	 * 
	 * @return an {@link TokenType#EOF} {@link Token}
	 */
	private SmartScriptToken tokenizeEOF() {
		token = new SmartScriptToken(SmartScriptTokenType.EOF, null);
		return token;
	}
	
	/**
	 * Sets the state of this {@link SmartScriptLexer} to the passed argument 
	 * {@code state}
	 * 
	 * @param state the {@code state} which this {@link SmartScriptLexer} shall be set to
	 */
	public void setState(SmartScriptLexerState state) {
		Objects.requireNonNull(state);
		
		this.state = state;
	}
	
	/**
	 * State getter
	 * @return state of the lecer
	 */
	public SmartScriptLexerState getState() {
		return state;
	}

	/**
	 * Last token generated getter
	 * @return last token generated
	 */
	public SmartScriptToken getToken() {
		return token;
	}

	/**
	 * Returns index of the next non-whitespace character.
	 * 
	 * @param startingIndex the starting index - inclusive
	 * @return index of the next non-whitespace character
	 */
	private int skipWhitespaces(int startingIndex) {
		int i = startingIndex;
		while(i < data.length && Character.isWhitespace(data[i])) {
			i++;
		}
		return i;
	}
}
