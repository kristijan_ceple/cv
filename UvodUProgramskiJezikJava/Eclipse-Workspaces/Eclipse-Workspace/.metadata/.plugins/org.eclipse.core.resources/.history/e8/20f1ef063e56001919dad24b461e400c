package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * A class modelling a parameterized dictionary/map/associative array. Internally is an adaptor
 * to an instance of {@link ArrayIndexedCollection}.
 * 
 * @author kikyy99
 *
 * @param <K> represents key
 * @param <V> represents value
 */
public class Dictionary<K, V> {

	/**
	 * Inner-mechanism {@link ArrayIndexedCollection} used for storage
	 */
	private ArrayIndexedCollection<Entry<K, V>> col;
	
	/**
	 * Class representing a key-value pair - used inside {@link Dictionary}
	 * @author kikyy99
	 *
	 * @param <K> represents key
	 * @param <V> represents value
	 */
	private static class Entry<K, V> {
		
		/**
		 * key
		 */
		private K key;
		/**
		 * value
		 */
		private V value;
		
		/**
		 * Constructs an entry.
		 * 
		 * @throws NullPointerException if key is null
		 * @param key key
		 * @param value value
		 */
		public Entry(K key, V value) {
			Objects.requireNonNull(key);
			
			this.key = key;
			this.value = value;
		}
	}
	
	/**
	 * Helper constructor used for fast insertion - not tested yet. 
	 * 
	 * @param pairs the key-value pairs to be added
	 */
	@SuppressWarnings({"unchecked", "unused"})
	private Dictionary(Object[]...pairs){
		for(Object[] entry : pairs) {
			if(entry.length != 2) {
				throw new IllegalArgumentException("Constructor error - wrong arguments passed!");
			}
			K key = (K) entry[0];
			V value = (V) entry[1];
			this.put(key, value);
		}
	}
	
	/**
	 * Default empty constructor.
	 */
	public Dictionary() {
		col = new ArrayIndexedCollection<Dictionary.Entry<K,V>>();
	}
	
	/**
	 * Checks whether this {@link Dictionary} is empty.
	 * @return whether this {@link Dictionary} is empty.
	 */
	public boolean isEmpty() {
		return col.isEmpty();
	}
	
	/**
	 * Returns the size of this {@link Dictionary}
	 * @return the size of this {@link Dictionary}
	 */
	public int size() {
		return col.size();
	}
	
	/**
	 * Clears the {@link Dictionary}.
	 */
	public void clear() {
		col.clear();
	}
	
	/**
	 * Puts the value into the map under its key. Overwrites old values.
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public void put(K key, V value) {
		int position = col.indexOf(key);
		Entry<K, V> toInsert = new Entry<K, V>(key, value);
		if(position != -1) {
			//means it has been found
			col.insert(toInsert, position);
		} else {
			col.add(toInsert);
		}
	}
	
	/**
	 * Returns the value located in the {@link Dictionary}under the key 
	 * passed as argument to this method.
	 * 
	 * @param key the key
	 * @return the value stored under this key in the {@link Dictionary}
	 */
	public V get(Object key) {
		Objects.requireNonNull(key);
		
		Entry<K, V>[] entrySet = (Entry<K, V>)col.toArray();
		for(Entry<K, V> entry : entrySet) {
			if(entry.key.equals(key)) {
				return entry.value;
			}
		}
		
		return null;
	}
	
}
