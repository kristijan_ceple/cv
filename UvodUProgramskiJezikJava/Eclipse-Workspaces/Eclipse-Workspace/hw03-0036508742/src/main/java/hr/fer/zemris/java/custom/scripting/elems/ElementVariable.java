package hr.fer.zemris.java.custom.scripting.elems;

import java.util.Objects;

public class ElementVariable extends Element {
	
	private String name;
	
	
	public ElementVariable(String name) {
		Objects.requireNonNull(name);
		
		this.name = name;
	}


	@Override
	public String asText() {
		return name;
	}
	
}
