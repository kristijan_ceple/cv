package hr.fer.zemris.java.custom.collections;

/**
 * Exception thrown upon a zero division attempt.
 * 
 * @author kikyy99
 *
 */
public class DivisionByZeroException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

}
