package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.elems.Element;

public class EchoNode extends Node {
	
	private Element[] elements;
	
	public EchoNode(Element[] elements) {
		Objects.requireNonNull(elements);
		this.elements = elements;
	}

	public int numberOfElements() {
		int counter = 0;
		
		for(Element element : elements) {
			if(element != null) {
				counter++;
			} else {
				break;
			}
		}
		
		return counter;
	}

	public Element[] getElements() {
		return elements;
	}
	
}
