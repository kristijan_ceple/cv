package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration of token types
 * @author kikyy99
 *
 */
public enum TokenType {
	/**
	 * End of file
	 */
	EOF, 
	
	/**
	 * Word
	 */
	WORD, 
	
	/**
	 * Number
	 */
	NUMBER, 
	
	/**
	 * Symbol
	 */
	SYMBOL;
}
