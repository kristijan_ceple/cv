package hr.fer.zemris.java.custom.scripting.elems;

public class ElementConstantDouble extends Element {
	
	private double value;
	
	public ElementConstantDouble(double value) {
		super();
		this.value = value;
	}

	@Override
	public String asText() {
		return Double.toString(value);
	}
}
