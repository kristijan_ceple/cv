package hr.fer.zemris.java.custom.collections;

/**
 * Simple interface that tests the given {@link Object}
 * for a certain criteria
 * 
 * @author kikyy99
 *
 */
public interface Tester {
	/**
	 * Tests the given {@link Object} for a certain criteria
	 * 
	 * @param obj {@link Object} to be tested
	 * @return true or false, depending on the expression evaluation
	 */
	boolean test(Object obj);
}
