package hr.fer.zemris.java.custom.collections;

/**
 * Interface {@link List} extends the interface {@link Collection} and
 * represents a contract for a list.
 * 
 * @author kikyy99
 */
public interface List extends Collection{
	/**
	 * Gets an element from this collection located at position index.
	 * 
	 * @param index the position which an element shall be retrieved from
	 * @return the element located at the position {@code index}
	 */
	Object get(int index);
	/**
	 * <p>Inserts the value at the requested position, and if necessary shifts the array contents.</p>
	 * 
	 * @param value the value to be inserted
	 * @param position the index at which the value shall be inserted
	 */
	void insert(Object value, int position);
	/**
	 * Attempts to find the value inside this collection. If found, returns its index.
	 * 
	 * @param value The value to be found
	 * @return value's index if value is found in this, else returns -1
	 */
	int indexOf(Object value);
	/**
	 * <p>Removes an element at the requested index, and shifts the elements if necessary.</p>
	 * 
	 * @param index the index from which the element shall be removed
	 */
	void remove(int index);
}
