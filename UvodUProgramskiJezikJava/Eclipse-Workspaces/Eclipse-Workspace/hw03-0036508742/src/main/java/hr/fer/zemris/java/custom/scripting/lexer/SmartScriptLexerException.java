package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Exception thrown in case of a lexer error
 * @author kikyy99
 *
 */
public class SmartScriptLexerException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default empty constructor
	 */
	public SmartScriptLexerException() {}
	
	/**
	 * Constructor with a message relayed to the user
	 * 
	 * @param message the message that shall be output
	 */
	public SmartScriptLexerException(String message) {
		super(message);
	}

}
