package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

public class ForLoopNode extends Node {

	private ElementVariable variable;
	private Element startExpression;
	private Element endExpression;
	/**
	 * Can be null
	 */
	private Element stepExpression;
	
	public ElementVariable getVariable() {
		return variable;
	}
	public Element getStartExpression() {
		return startExpression;
	}
	public Element getEndExpression() {
		return endExpression;
	}
	public Element getStepExpression() {
		return stepExpression;
	}
	public void setVariable(ElementVariable variable) {
		this.variable = variable;
	}
	public void setStartExpression(Element startExpression) {
		this.startExpression = startExpression;
	}
	public void setEndExpression(Element endExpression) {
		this.endExpression = endExpression;
	}
	public void setStepExpression(Element stepExpression) {
		this.stepExpression = stepExpression;
	}
	
}
