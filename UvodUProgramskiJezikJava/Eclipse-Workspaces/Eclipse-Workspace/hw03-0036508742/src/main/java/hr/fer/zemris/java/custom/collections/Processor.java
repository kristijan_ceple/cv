package hr.fer.zemris.java.custom.collections;

/**
 * The class implementing a simple processor
 * 
 * @author kikyy99
 *
 */
public interface Processor {
		/**
		 * The method that shall be performed over the argument {@code value}
		 * 
		 * @param value the parameter that shall be processed
		 */
		void process(Object value);
	
}
