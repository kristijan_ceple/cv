package hr.fer.zemris.java.custom.scripting.elems;

import java.util.Objects;

public class ElementString extends Element{

	private String value;
	
	public ElementString(String value) {
		Objects.requireNonNull(value);
		this.value = value;
	}

	@Override
	public String asText() {
		return value;
	}
	
}
