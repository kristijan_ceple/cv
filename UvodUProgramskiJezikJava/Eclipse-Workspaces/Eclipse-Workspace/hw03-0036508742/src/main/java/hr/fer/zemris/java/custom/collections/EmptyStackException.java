package hr.fer.zemris.java.custom.collections;

/**
 * Exception thrown upon pop and/or peek attempt on an empty stack.
 * @author kikyy99
 *
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

}
