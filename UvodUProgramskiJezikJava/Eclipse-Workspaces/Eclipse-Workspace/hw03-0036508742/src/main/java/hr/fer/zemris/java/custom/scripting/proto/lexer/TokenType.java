package hr.fer.zemris.java.custom.scripting.proto.lexer;

/**
 * Enumeration of token types
 * @author kikyy99
 *
 */
public enum TokenType {
	/**
	 * End of file
	 */
	EOF, 
	
	/**
	 * Word
	 */
	WORD, 
	
	/**
	 * Number
	 */
	NUMBER, 
	
	/**
	 * Symbol
	 */
	SYMBOL;
}
