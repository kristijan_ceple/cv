package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

public class TextNode extends Node {
	
	private String text;
	
	public TextNode(String text) {
		Objects.requireNonNull(text);
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
}
