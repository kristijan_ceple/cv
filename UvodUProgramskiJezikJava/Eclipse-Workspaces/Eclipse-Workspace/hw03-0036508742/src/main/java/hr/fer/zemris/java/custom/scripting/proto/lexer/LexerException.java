package hr.fer.zemris.java.custom.scripting.proto.lexer;

public class LexerException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default empty constructor
	 */
	public LexerException() {}
	
	/**
	 * Constructor with a message relayed to the user
	 * 
	 * @param message the message that shall be output
	 */
	public LexerException(String message) {
		super(message);
	}

}
