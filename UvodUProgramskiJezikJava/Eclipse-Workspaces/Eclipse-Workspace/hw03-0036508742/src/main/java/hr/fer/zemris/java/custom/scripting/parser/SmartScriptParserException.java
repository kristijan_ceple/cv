package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Exception thrown if a parsing error occurs
 * @author kikyy99
 *
 */
public class SmartScriptParserException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Default empty non-message constructor
	 */
	public SmartScriptParserException() {}
	
	/**
	 * Constructor with an error message
	 * @param message the error message
	 */
	SmartScriptParserException(String message) {
		super(message);
	}
}
