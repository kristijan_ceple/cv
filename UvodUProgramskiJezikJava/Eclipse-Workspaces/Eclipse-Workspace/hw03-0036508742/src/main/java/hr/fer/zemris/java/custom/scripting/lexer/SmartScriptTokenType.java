package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of token types
 * @author kikyy99
 *
 */
public enum SmartScriptTokenType {
	/**
	 * End of file
	 */
	EOF,
	/**
	 * Variable
	 */
	VARIABLE,
	/**
	 * Integer constant
	 */
	CONSTANT_INTEGER,
	/**
	 * Double constant
	 */
	CONSTANT_DOUBLE,
	/**
	 * Text in string form
	 */
	STRING_TEXT,
	/**
	 * Function
	 */
	FUNCTION,
	/**
	 * Operator
	 */
	OPERATOR,
	/**
	 * Opening of a tag
	 * @see SmartScriptLexer#TAG_OPENING
	 */
	TAG_OPENING,
	/**
	 * Closing of a tag.
	 * @see SmartScriptLexer#TAG_CLOSING
	 */
	TAG_CLOSING,
	/**
	 * A symbol.
	 */
	SYMBOL,
	/**
	 * Tag type
	 */
	TAG_TYPE;
}
