package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Helps us model a simple collection by providing a contract that shall be fulfilled by the implementing classes.
 * Other classes shall implement this interface and provide the functions and storage necessary for the proper
 * functioning of a full-fledged collection.
 *  
 * @author kikyy99
 *
 */
public interface Collection {
	/**
	 * Checks whether this collection has got any elements
	 * spoiler: uses the size() function to check emptiness
	 * 
	 * @return true if empty - false otherwise
	 */
	default public boolean isEmpty() {
		return size()==0;
	}
	/**
	 * Tests all the elements of the argument {@link Collection} col, as according to
	 * the argument {@link Tester} tester. Each element that passes the test shall be
	 * added into this {@link Collection}.
	 * 
	 * @param col {@link Collection} from which elements shall be retrieved
	 * and tested. If the test according to this argument {@link Tester} tester 
	 * returns true, the tested element shall be added into THIS {@link Collection}
	 * @param tester implementation of the {@link Tester} interface, that shall be used
	 * for testing the elements of the argument {@link Collection} col.
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		 ElementsGetter getter = col.createElementsGetter();
		 while(getter.hasNextElement()) {
			 Object element = getter.getNextElement();
			 if(tester.test(element)) {
				 //if true, add element to this collection
				 this.add(element);
			 }
		 }
	}
	
	/**
	 * Retrieves the size of this collection and returns it.
	 * 
	 * @return the size of this collection
	 */
	int size();
	
	/**
	 * Adds an object into this collection.
	 * 
	 * @param value
	 */
	void add(Object value);

	/**
	 * Checks whether this {@link Collection} contains the passed argument {@code Object value}
	 * 
	 * 
	 * @param value the {@code Object value} that is searched in the collection for
	 * @return whether the specified {@code Object value} was found in the collection or not
	 */
	boolean contains(Object value);
	
	/**
	 * Removes the object from the collection.
	 * 
	 * @param value the value to find and remove from this collection
	 * @return whether an object was found and removed
	 */
	boolean remove(Object value);
	
	/**
	 * <p>Allocates a NEW array, and copies all the elements from this collection
	 * into this new array, and then returns it.</p>
	 * 
	 * <p>In other words, the array isn't read-through.</p>
	 * 
	 * @return the array featuring all the elements of this collection
	 */
	Object[] toArray();
	
	/**
	 * Processor.process(Object) method is called over each element of this
	 * collection.
	 * 
	 * @param processor the process whose process() method shall be executed over every element
	 */
	default void forEach(Processor processor) {
		ElementsGetter getter = this.createElementsGetter();
		getter.processRemaining(processor);
	}
	
	/**
	 * Copies all elements from another collection(the other collection itself is UNCHANGED) and
	 * adds them into this collection
	 * 
	 * spoiler: uses a processor in order to perform the addition
	 * 
	 * @param other the other {@link Collection} that SHALL NOT BE MODIFIED
	 */
	default public void addAll(final Collection other) {
		class addAllProcessor implements Processor {
			
			@Override
			public void process(Object value) {
				Objects.requireNonNull(value, "Argument passed is null!");
				//Validation done, now act on the object
				Collection.this.add(value);
			}
		}
		
		Processor addAllProcessor = new addAllProcessor();
		for(Object element : other.toArray()){
			addAllProcessor.process(element);
		}
	}
	
	/**
	 * Removes all the elements from this collection.
	 */
	void clear();
	
	/**
	 * Instantiates and returns an {@link ElementGetter}.
	 * @return an {@link ElementGetter} instance
	 */
	ElementsGetter createElementsGetter();
}
