package hr.fer.zemris.java.hw03.prob1;

import java.util.Arrays;
import java.util.Objects;

/**
 * Class representing a simple {@code Lexer}.
 * 
 * @see LexerStates for a more detailed description of possible
 * {@link Lexer} states
 * @author kikyy99
 *
 */
public class Lexer {
	/**
	 * Special character used to enter the {@link LexerState#EXTENDED} mode
	 */
	public static final char SPECIAL_SIGN = '#';
	/**
	 * Specifies in which state is the {@link Lexer} located. 
	 */
	private LexerState state = LexerState.BASIC;
	/**
	 * Entry text/characters
	 */
	private char[] data;      // ulazni tekst
	/**
	 * The current token
	 */
	private Token token;      // trenutni token
	/**
	 * Index of the first non-processed character
	 */
	private int currentIndex; // indeks prvog neobrađenog znaka
	
	/**
	 * 1-argument constructor. Processes the input {@code text}
	 * into tokens.
	 * 
	 * @param text the text to be processed
	 */
	// konstruktor prima ulazni tekst koji se tokenizira
	public Lexer(String text) {
		if(text == null) {
			throw new NullPointerException("Null passed as input to the Lexer.");
		}
		
		this.data = text.toCharArray();
	}
	
	/**
	 * Counts the total length of the word currently being parsed
	 * 
	 * @param i the beginning index of the word to be parsed
	 * @return the length of the parsed word
	 */
	private int parseWord(int i) {
		while(i < data.length) {
			if(Character.isLetter(data[i])) {
				i++;
				continue;
			} else if(Character.isWhitespace(data[i])){
				//whitespace encountered - the end of word
				return i;
			} else if(data[i] == '\\' && state == LexerState.BASIC) {
				//now check if digit follows
				if(i+1 < data.length) {
					// Check the letter that follows, if it's a digit or another backslash parse it. 
					if(Character.isDigit(data[i+1])) {
						i+=2;
						continue;
					} else if(data[i+1]=='\\') {
						i+=2;
						continue;
					} else {
						//not digit, not c, t or r - ERROR WRONG SYMBOL
						throw new LexerException("Invalid symbol after backslash encountered! Digit or another "
								+ "backslash expected!");
					}
				} else {
					throw new LexerException("Single backslash encountered!");
				}
			} else if(state == LexerState.EXTENDED){
				// We should now tokenize as per the EXTENDED state - just check for SPECIAL_SIGN
				if(data[i] == SPECIAL_SIGN) {
					//we end the input at position i
					return i;
				} else {
					i++;
					continue;
				}
				
			} else {
				/*
				 * Now it's hit the fan - neither basic nor extended cases have been hit.
				 * 
				 * This means that either a symbol or a number has come, and we need to
				 * end this word token since at position i came something not to be put
				 * in a word.
				 */
				return i;
			}
		}
		
		return i;
	}
	
	/**
	 * Counts the total length of the number currently being parsed
	 * 
	 * @param i the beginning index of the number to be parsed
	 * @return the length of the parsed number
	 */
	private int parseNumber(int i) {
		while(i < data.length && Character.isDigit(data[i])) {
			i++;
		}
		
		return i;
	}
	
	/**
	 * Skips all the whitespaces before the next {@link Token}
	 * 
	 * @param i the index from which whitespaces shall be skipped
	 * @return the index of the next {@link Token}
	 */
	private int skipWhitespace(int i) {
		while(i < data.length && Character.isWhitespace(data[i])) {
			i++;
		}
		
		return i;
	}
	
	/**
	 * Creates a new {@link Token} from the given word.
	 * 
	 * @param start the start of the word from which a {@link Token} shall be
	 * created
	 * @param nextToken the index at which the next {@link Token} starts
	 * @return the {@link Token} holding this word
	 */
	private Token tokenizeWord(int start, int nextToken) {
		char[] word = Arrays.copyOfRange(data, start, nextToken);
		char[] filteredWord = new char[word.length];
		
		boolean firstBackslash = true;
		int i, j;
		for(i = 0, j = 0; i < word.length; i++) {
			
			if(word[i] == '\\' && firstBackslash && state == LexerState.BASIC) {
				firstBackslash = false;
				continue;
			}
			
			firstBackslash = true;			//only matters if we're in BASIC mode
			filteredWord[j++] = word[i];
		}
		
		filteredWord = Arrays.copyOf(filteredWord, j);
		String finalWord = String.valueOf(filteredWord);
		token = new Token(TokenType.WORD, finalWord);
		return token;
	}
	
	/**
	 * Creates a new {@link Token} from the given number.
	 * 
	 * @param start the start of the number from which a {@link Token} shall be
	 * created
	 * @param nextToken the index at which the next {@link Token} starts
	 * @return the {@link Token} holding this number
	 */
	private Token tokenizeNumber(int start, int nextToken) {
		String word = String.valueOf(data, start, nextToken-start);
		try{
			token = new Token(TokenType.NUMBER, Long.valueOf(word));
		} catch(NumberFormatException ex) {
			throw new LexerException("Number too large!");
		}
		return token;
	}
	
	/**
	 * Creates and returns an {@link TokenType#EOF} {@link Token}
	 * 
	 * @return an {@link TokenType#EOF} {@link Token}
	 */
	private Token tokenizeEOF() {
		token = new Token(TokenType.EOF, null);
		return token;
	}
	/**
	 * Creates and returns an {@link TokenType#Symbol} {@link Token}
	 * 
	 * @return an {@link TokenType#Symbol} {@link Token}
	 */
	private Token tokenizeSymbol(int i) {
		token = new Token(TokenType.SYMBOL, Character.valueOf(data[i]));
		if(data[i]=='#') {
			setState(state == LexerState.EXTENDED ? LexerState.BASIC : LexerState.EXTENDED);
		}
		return token;
	}
	
	/**
	 * Returns the next token
	 * 
	 * @return the next token
	 */
	// generira i vraća sljedeći token
	// baca LexerException ako dođe do pogreške
	public Token nextToken() {
		for(int i = currentIndex; i < data.length; i++) {
			if(state.equals(LexerState.EXTENDED)) {
				/*
				 * Here we take whitespaces and use them for splitting the tokens.
				 * Everything else are words.
				 */
				if(data[i] == SPECIAL_SIGN) {
					currentIndex = i+1;
					return tokenizeSymbol(i);
				} else if(Character.isWhitespace(data[i])) {
					currentIndex = skipWhitespace(i);
					i = currentIndex - 1;
					continue;					//skip whitespace and go on to the next token
				} else {
					//everything else shall be parsed as words bwahhaha
					currentIndex = parseWord(i);
					return tokenizeWord(i, currentIndex);
				}
			}
			
			if(Character.isLetter(data[i])) {
				//we've got a word on our hands
				currentIndex = parseWord(i);
				return tokenizeWord(i, currentIndex);
			} else if(Character.isDigit(data[i])) {
				currentIndex = parseNumber(i);
				return tokenizeNumber(i, currentIndex);
			} else if(Character.isWhitespace(data[i])) {
				currentIndex = skipWhitespace(i);
				i = currentIndex - 1;
				continue;					//skip whitespace and go on to the next token
			} else if(data[i] == '\\'){
				/*
				 * Check if digit or another backslash
				 */
				//check if backslash escapes a digit or another backslash and return the new Token
				//else throw an error - the function checkBackslashOther throws these errors
				return checkBackslashOther(i);
			} else {
				//else we've encountered a character
				currentIndex = i+1;
				return tokenizeSymbol(i);
			}
		}
		
		//generate an EOF token
		if(token == null || token.getType() != TokenType.EOF) {
			//Input has been successfully parsed or is empty --> GENERATE EOF!!!
			currentIndex = data.length;
			return tokenizeEOF();
		} else {
			//WRONG! end of input already reached
			throw new LexerException("Input already processed - its end has already been reached, and/or an EOF generated!");
		}

	}
	
	/**
	 * A helper method which checks if backslash escapes a digit
	 * or another backslash
	 * 
	 * @param i the index which the (first) backslash(or in other words - 
	 * the one being followed by a digit or another backslash)  is located at
	 * @return a {@link Token} holding the tokenized word
	 */
	private Token checkBackslashOther(int i) {
		if(i+1 < data.length) {
			if(data[i+1] == '\\' || Character.isDigit(data[i+1])){
				//form a word
				currentIndex = parseWord(i);
				return tokenizeWord(i, currentIndex);
			} else {
				//invalid character encountered
				throw new LexerException("Invalid character encountered following the \\.");
			}
		} else {
			//invalid input - backslash in wrong place
			throw new LexerException("\\ encountered: expecting 'r', 'n', 't', a digit or another \\ "
					+ "after the encounter \\, but none of them is found.");
		}
	}
	
	/**
	 * Returns the current token
	 * 
	 * @return the current token
	 */
	// vraća zadnji generirani token; može se pozivati
	// više puta; ne pokreće generiranje sljedećeg tokena
	public Token getToken() {
		return token;
	}

	/**
	 * Sets the state of this {@link Lexer} to the passed argument 
	 * {@code state}
	 * 
	 * @param state the {@code state} which this {@link Lexer} shall be set to
	 */
	public void setState(LexerState state) {
		Objects.requireNonNull(state);
		
		this.state = state;
	}
}
