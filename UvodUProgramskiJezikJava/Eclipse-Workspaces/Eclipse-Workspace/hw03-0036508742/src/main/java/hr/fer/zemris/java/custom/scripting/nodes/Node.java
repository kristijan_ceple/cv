package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

public class Node {
	
	public static final int DEFAULT_INITIAL_COL_CAPACITY = 32;
	private ArrayIndexedCollection col;
	
	public void addChildNode(Node child) {
		if(col == null) {
			col = new ArrayIndexedCollection(DEFAULT_INITIAL_COL_CAPACITY);
		}
		
		col.add(child);
	}
	
	public int numberOfChildren() {
		return col.size();
	}
	
	public Node getChild(int index) {
		return (Node)col.get(index);
	}
}
