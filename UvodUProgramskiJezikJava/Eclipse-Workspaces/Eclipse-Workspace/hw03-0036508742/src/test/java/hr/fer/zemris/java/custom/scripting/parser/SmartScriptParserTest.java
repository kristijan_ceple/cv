package hr.fer.zemris.java.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

class SmartScriptParserTest {


	@Test
	void testNull() {
		assertThrows(SmartScriptParserException.class, ()->new SmartScriptParser(null));
	}
	
	@Test
	void testOnlyTextNoTags() {
		String input = "    \r/r\naiiiiikk\r";
		SmartScriptParser parser = new SmartScriptParser(input);
		DocumentNode root = parser.getRoot();
		
		assertEquals(1, root.numberOfChildren());
		assertTrue(root.getChild(0) instanceof TextNode);
		assertEquals(input, ((TextNode)root.getChild(0)).getText());
	}

	@Test
	void testEchoTag() {
		String input = "{$  =  AJMOOO \"0.001\" 11.1$}";
		SmartScriptParser parser = new SmartScriptParser(input);
		DocumentNode root = parser.getRoot();
		
		assertEquals(1, root.numberOfChildren());
		assertTrue(root.getChild(0) instanceof EchoNode);
		assertEquals(3, ((EchoNode)root.getChild(0)).numberOfElements());
	}
	
	@Test
	void testMixed() {
		String input = "EHH ajak }}} ask \\{{$  =  AJMOOO \"0.001\" 11.1$}\r\n\\\\ stagod";
		SmartScriptParser parser = new SmartScriptParser(input);
		DocumentNode root = parser.getRoot();
	}
	
	@Test
	void testCompleteExample() {
		String input = "This is sample text.\r\n{$ FOR i 1 10 1 $}\r\n  This is {$= i $}-th time this message is generated.\r\n{$END$}\r\n{$FOR i 0 10 2 $}\r\n  sin({$=i$}^2) = {$= i i * @sin  \"0.000\" @decfmt $}\r\n{$END$}";
		SmartScriptParser parser = new SmartScriptParser(input);
		DocumentNode root = parser.getRoot();
	}
}
