package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DictionaryTest {

	Dictionary<String, Integer> avioni;
	
	@BeforeEach
	void setUp() throws Exception {
		Dictionary<String, Integer> avioni = new Dictionary<>();
		avioni.put("Boeing 747", 2000);
		avioni.put("Airbus A380", 2150);
		avioni.put("Airbus A320", 1500);
		avioni.put("Su-35", 3000);
		
		this.avioni = avioni;
	}

	@Test
	void testIsEmpty() {
		Dictionary<String, Integer> newDict = new Dictionary<String, Integer>();
		assertTrue(newDict.isEmpty());
		newDict.put("F\\A-18", 1270);
		assertFalse(newDict.isEmpty());
		
		assertFalse(avioni.isEmpty());
	}

	@Test
	void testSize() {
		assertTrue(avioni.size()==4);
		avioni.put("Piper Cub", 50);
		assertTrue(avioni.size()==5);
	}

	@Test
	void testClear() {
		avioni.clear();
		assertTrue(avioni.isEmpty());
		assertTrue(avioni.size()==0);
	}

	@Test
	void testPut() {
		avioni.put("Piper Cub", 50);
		assertFalse(avioni.isEmpty());
		assertTrue(avioni.size()==5);
		assertEquals(2000, avioni.get("Boeing 747"));
		
		assertThrows(NullPointerException.class, ()-> avioni.put(null, 1000000000));
	}

	@Test
	void testGet() {
		assertEquals(2000, avioni.get("Boeing 747"));
		assertNull(avioni.get("haakasdkask"));
		assertNull(avioni.get("SU-35"));			//should be Su-35, not SU-35
	}
}
