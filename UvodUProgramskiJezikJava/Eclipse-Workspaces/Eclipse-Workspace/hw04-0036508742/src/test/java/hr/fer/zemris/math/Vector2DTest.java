package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Vector2DTest {
	
	public static final double EPSILON = 1E-6;
	
	Vector2D v1;
	Vector2D v2;
	
	@BeforeEach
	void setUp() throws Exception {
		v1 = new Vector2D(1.78, 2.565);
		v2 = new Vector2D(-3.25, -1.8);
	}

	@Test
	void testGetX() {
		assertTrue(doublesEqual(1.78, v1.getX()));
		assertTrue(doublesEqual(-3.25, v2.getX()));
	}

	@Test
	void testGetY() {
		assertTrue(doublesEqual(2.565, v1.getY()));
		assertTrue(doublesEqual(-1.8, v2.getY()));
	}

	@Test
	void testTranslate() {
		v1.translate(new Vector2D(2, 3));
		assertTrue(doublesEqual(v1.getX(), 3.78));
		assertTrue(doublesEqual(v1.getY(), 5.565));
	}

	@Test
	void testTranslated() {
		Vector2D v3 = v2.translated(new Vector2D(1.334, 3.2100));
		assertTrue(doublesEqual(v3.getX(), -1.916));
		assertTrue(doublesEqual(v3.getY(), 1.41));
	}

	@Test
	void testRotate() {
		v2.rotate(3.421199501);
		assertTrue(doublesEqual(v2.getX(), 2.62702303));
		assertTrue(doublesEqual(v2.getY(), 2.62702303));
	}

	@Test
	void testRotated() {
		Vector2D v3 = v1.rotated(0.6066588128);
		
		assertTrue(doublesEqual(v3.getX(), 0));
		assertTrue(doublesEqual(v3.getY(), 3.122118672));
	}

	@Test
	void testScale() {
		v2.scale(0.432);
		assertTrue(doublesEqual(v2.getLength(), 1.60495413));
		v1.scale(3.4445);
		assertTrue(doublesEqual(v1.getLength(), 10.75413777));
		v1.scale(0);
		assertTrue(doublesEqual(v1.getLength(), 0));
		
		assertThrows(IllegalArgumentException.class, ()->v2.scale(-0.001));
	}

	@Test
	void testScaled() {
		assertThrows(IllegalArgumentException.class, ()->v2.scaled(-0.001));
		
		Vector2D v3 = v1.scaled(2.091);
		assertTrue(doublesEqual(v3.getLength(), 6.528350143));
		v3 = v2.scaled(0);
		assertTrue(doublesEqual(v3.getLength(), 0));
	}

	@Test
	void testCopy() {
		Vector2D v3 = v1.copy();
		assertTrue(doublesEqual(v3.getX(), v1.getX()));
		assertTrue(doublesEqual(v3.getY(), v1.getY()));
	}
	
	static boolean doublesEqual(double d1, double d2) {
		return Math.abs(d1-d2) < EPSILON;
	}

}
