package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * A custom-made interface resembling an {@link Iterator}. This
 * interface is parameterized. 
 * 
 * @author kikyy99
 *
 */
public interface ElementsGetter<T> {
	/**
	 * Generic algorithm implementation that iterates over all the elements
	 * of the class implementing this interface - for each one of its objects
	 * processor p is called
	 * 
	 * @param p the {@link Processor} whose method {@link Processor#process(Object)} shall
	 * be called over each elements of the class implementing this interface
	 */
	default void processRemaining(Processor<? super T> p) {
		while(this.hasNextElement()) {
			p.process(this.getNextElement());
		}
	}
	/**
	 * Checks whether this {@link ElementsGetter} features any unreturned elements
	 * 
	 * @throws ConcurrentModificationException if the overlaying {@link Collection} has been modified
	 * during the lifetime of this {@link ElementsGetter}
	 * @return true if this {@link ElementsGetter} has got any elements to return; false otherwise
	 */
	boolean hasNextElement();
	/**
	 * Returns the next element from this {@link ElementsGetter} if it has any elements left. Otherwise throws an {@link NoSuchElementException}
	 * 
	 * @throws ConcurrentModificationException if the overlaying {@link Collection} has been modified
	 * during the lifetime of this {@link ElementsGetter}
	 * @throws NoSuchElementException if there are no more elements left in this
	 * {@link Collection}
	 * @return the next element from this {@link ElementsGetter}
	 */
	T getNextElement();
}
