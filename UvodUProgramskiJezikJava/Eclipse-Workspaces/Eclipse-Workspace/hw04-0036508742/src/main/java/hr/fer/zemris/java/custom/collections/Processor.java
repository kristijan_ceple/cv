package hr.fer.zemris.java.custom.collections;

/**
 * The class implementing a simple processor, and is
 * parameterized.
 * 
 * @author kikyy99
 *
 */
public interface Processor<T> {
		/**
		 * The method that shall be performed over the argument {@code value}
		 * 
		 * @param value the parameter that shall be processed
		 */
		void process(T value);
	
}
