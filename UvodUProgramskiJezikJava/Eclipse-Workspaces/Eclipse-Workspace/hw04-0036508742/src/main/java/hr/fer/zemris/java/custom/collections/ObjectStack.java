package hr.fer.zemris.java.custom.collections;

/**
 * An adaptor/wrapper of class {@code ArrayIndexedCollection} towards a stack.
 * 
 * @author kikyy99
 *
 */
public class ObjectStack<T> {
	/**
	 * The inner-mechanism {@code ArrayIndexedCollection} used for storage
	 */
	private ArrayIndexedCollection<T> storage;
	
	/**
	 * Default constructor. Initializes the inner-mechanism 
	 * {@code ArrayIndexedCollection} for storage
	 */
	public ObjectStack() {
		storage = new ArrayIndexedCollection<T>();
	}
	
	/**
	 * Checks whether the stack is empty
	 * 
	 * @return true if stack is empty, false otherwise
	 */
	public boolean isEmpty() {
		return storage.isEmpty();
	}
	
	/**
	 * Checks the size of the size and returns it
	 * 
	 * @return the size of this stack
	 */
	public int size() {
		return storage.size();
	}
	
	/**
	 * Pushes an object onto the stack.
	 * 
	 * @param value the value to be pushed
	 */
	public void push(T value) {
		storage.add(value);
	}
	
	/**
	 * Retrieves and removes the object from the top of the stack.
	 * 
	 * @throws EmptyStackException if the stack is empty
	 * @return the object located at the top of the stack
	 */
	public T pop() {
		if(this.size() == 0) {
			throw new EmptyStackException();
		}
		
		int index = storage.size()-1;
		T toReturn = storage.get(index);
		storage.remove(index);
		
		return toReturn;
	}
	
	/**
	 * Retrieves, but DOESN'T remove the object located at the top of the stack
	 * 
	 * @throws EmptyStackException if the stack is empty
	 * @return the object located at the top of the stack
	 */
	public T peek() {
		if(this.size() == 0) {
			throw new EmptyStackException();
		}
		
		int index = storage.size()-1;
		T toReturn = storage.get(index);
		//no deletion!!!
		
		return toReturn;
	}
	
	/**
	 * Clears the stack of all elements.
	 */
	public void clear() {
		storage.clear();
	}
}
