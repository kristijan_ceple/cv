package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * A proper, full-fledged functioniong implementation of the class
 * {@code Collection}. It represents a {@code Doubly-Linked List}.
 * 
 * @author kikyy99
 *
 */
public class LinkedListIndexedCollection<T> implements List<T> {

	/**
	 * An iterator-like class that implements the {@link ElementsGetter} interface.
	 * Returns elements from the {@link Collection}.
	 * @author kikyy99
	 *
	 */
	private static class ListElementsGetter<T> implements ElementsGetter<T>{
		
		/**
		 * Cached modificationCount of the {@link ArrayIndexedCollection}. During subsequent calls
		 * to this {@link ElementsGetter} modificationCounts of the getter and the collection shall be compared - 
		 * and if they are not equal a {@link ConcurrentModificationException} will be thrown.
		 */
		private long savedModificationCount;
		/**
		 * The index of the next element to be returned.
		 */
		private int currIndex; 
		/**
		 * The {@link LinkedListIndexedCollection} from which elements are being retrieved.
		 */
		private LinkedListIndexedCollection<T> list;
		
		/**
		 * 1-argument constructor.
		 * 
		 * @param array The {@link LinkedListIndexedCollection} used for storage
		 * and element retrieval
		 */
		private ListElementsGetter(LinkedListIndexedCollection<T> list, long modificationCount) {
			this.list = list;
			this.savedModificationCount = modificationCount;
		}
		
		@Override
		public boolean hasNextElement() {
			if(savedModificationCount != list.modificationCount) {
				throw new ConcurrentModificationException("Collection has been modified during the lifetime of this ElementsGetter");
			}
			
			return currIndex <= list.size-1;
		}
		
		@Override
		public T getNextElement() {
			if(savedModificationCount != list.modificationCount) {
				throw new ConcurrentModificationException("Collection has been modified during the lifetime of this ElementsGetter");
			}
			

			T toReturn = null;
			try {
				toReturn = list.get(currIndex++);
			} catch(IndexOutOfBoundsException ex) {
				throw new NoSuchElementException("No more elements left to iterate over!");
			}
			
			return toReturn;
		}
	}
	
	/**
	 * Counts structural modifications made to this {@link ArrayIndexedCollection}
	 */
	private long modificationCount;
	/**
	 * The size of the list
	 */
	private int size;
	/**
	 * Reference to the first element of the list(the 'Head')
	 */
	private ListNode<T> first;
	/**
	 * Reference to the first element of the list(the 'Tail')
	 */
	private ListNode<T> last;
	
	/**
	 * A static nested class representing a single node of this list
	 * @author kikyy99
	 *
	 */
	private static class ListNode<T> {
		/**
		 * Reference to the next node
		 */
		private ListNode<T> next;
		/**
		 * Reference to the previous node
		 */
		private ListNode<T> previous;
		/**
		 * The value that the node is holding/wrapping
		 */
		private T value;
		
		/**
		 * 1-argument constructor; Initializes this node's value to the given argument.
		 * Sets node.next and node.previous references to null. Meant to be used ONLY
		 * as an inner mechanism by the class {@code LinkedListIndexedCollection}
		 * 
		 * @param value The value that this node shall hold/wrap
		 */
		private ListNode(T value) {
			this.value = value;
		}
	}
	
	/**
	 * Empty constructor - everything is set to null
	 */
	public LinkedListIndexedCollection() {}
	
	/**
	 * Copies all the elements from other collection to this collection. The other collection is unchanged.
	 * 
	 * @throws NullPointerException if {@link Collection} {@code other} is {@code null}
	 * @param other
	 */
	public LinkedListIndexedCollection(final Collection<? extends T> other) {
		Objects.requireNonNull(other);
		addAll(other);
	}
	
	//	##################### COLLECTION METHODS IMPLEMENTATION		##############################
	/**
	 * Returns the size of this {@code LinkedListIndexedCollection}
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * Checks whether this list contains the specified argument.
	 */
	@Override
	public boolean contains(Object value) {
		if(value == null) {
			return false;
		} else if(this.first == null && this.last == null) {
			//empty list thus doesn't contain anything
			return false;
		}
		
		ListNode<T> currentNode = this.first;
		while(currentNode != null) {
			if(currentNode.value.equals(value)) {
				return true;
			}
			
			currentNode = currentNode.next;
		}
		
		//else we've iterated over the whole list and found no match
		return false;
	}
	
	/**
	 * Removes the specified argument from the list - if 
	 * such an arguments exists in the list.
	 */
	@Override
	public boolean remove(Object value) {
		//let's first find that node
		if(value == null) {
			return false;
		} else if(this.first == null && this.last == null) {
			//empty list thus doesn't contain anything
			return false;
		}
		
		//if the element is located at the beginning or the end - easy(ier anyway) deletion
		if(this.first.value.equals(value)) {
			this.first.next.previous = null;
			this.first = this.first.next;
			size--;
			return true;
		} else if(this.last.value.equals(value)) {
			this.last.previous.next = null;
			this.last = this.last.previous;
			size--;
			return true;
		}
			
		ListNode<T> currentNode = this.first.next;
		while(currentNode != this.last) {
			if(currentNode.value.equals(value)) {
				//Need to remove this one
				currentNode.previous.next = currentNode.next;
				currentNode.next.previous = currentNode.previous;
				size--;
				return true;
			}
			
			currentNode = currentNode.next;
		}
		
		return false;
	}
	
	/**
	 * Returns an array of all the elements contained by this list
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T[] toArray() {
		T[] array = (T[])new Object[this.size];
		
		var currentNode = this.first;
		int i = 0;
		while(currentNode != null) {
			array[i++] = currentNode.value;
			currentNode = currentNode.next;
		}
		
		return array;
	}
	//	##################### COLLECTION METHODS IMPLEMENTATION		##############################
	
	/**
	 * Adds an element into this list. Works at the O(1) complexity, since it adds elements at the end
	 * of the list. 
	 * 
	 * @throws NullPointerException if value is null
	 */
	@Override
	public void add(T value) {
		Objects.requireNonNull(value);
		
		ListNode<T> newNode = new ListNode<>(value);
		size++;
		//If list is empty
		if((this.first == null) && (this.last == null)) {
			this.first = this.last = newNode;
			return;
		}
		
		//else list isn't empty, so just update the last
		this.last.next = newNode;
		newNode.previous = this.last;
		this.last = newNode;
	}
	
	/**
	 * <p>Finds and retrieves the element located in this list at the index
	 * specified by the argument {@code index}.</p>
	 * 
	 * <p>Works at the O(n/2) complexity,
	 * since it approaches the element from the side which the
	 * searched index is located nearer to.</p>
	 * 
	 * @throws IndexOutOfBounds if index isn't in range [0, collection.size()>
	 * @param index The index from which the element should be retrieved.
	 * Must be in range [0, size>.
	 * @return object retrieved at position {@code index}
	 */
	public T get(int index) {
		Objects.checkIndex(index, this.size());
		
		//Time to decide from which side will we approach the searched value
		boolean fromLeft = false;
		if(index < this.size()/2) {
			fromLeft = true;
		}
		
		if(fromLeft) {
			ListNode<T> current = this.first;
			int i = 0;
			while(i != index) {
				current = current.next;
				i++;
			}
			
			//grab and return the value
			return current.value;
		} else {
			ListNode<T> current = this.last;
			int i = this.size() - 1;
			while(i != index) {
				current = current.previous;
				i--;
			}
			
			return current.value;
		}
	}
	
	/**
	 * Removes all the elements from this list
	 */
	@Override
	public void clear() {
		this.first = this.last = null;
		this.size = 0;
	}
	
	/**
	 * Inserts an element into this list. If necessary, shifts the other elements.
	 * As such works at average complexity O(n/2).
	 * 
	 * @throws IndexOutOfBounds if index isn't in range [0, collection.size()]
	 * @throws NullPointerException if value is null
	 * @param value the value to be inserted
	 * @param position the position in the list which this value shall be inserted at
	 */
	public void insert(T value, int position) {
		Objects.checkIndex(position, this.size()+1);
		
		/*
		 * If the list is empty or we're adding to the end
		 * then we just add an element the normal way
		 */
		if(this.size() == 0 || position == this.size()) {
			this.add(value);
			return;
		}
		
		/*
		 * Else list isn't empty.
		 * 
		 * We will need to get to position - 1 or position + 1. This will obviously
		 * be a problem if position is 0 - we can't get to the element
		 * at pos -1 cause it doesn't exist.
		 * 
		 * Getting to the element at pos + 1 will NEVER be a problem, because
		 * we already handled the event when element is being added to the end
		 * of the list
		 * 
		 * First we have to decide if we shall go from the left or right
		 * using the same algo as the get method
		 * 
		 * If position is 0 we'll have to handle that a little bit differently
		 */
		Objects.requireNonNull(value);
		
		boolean fromLeft = false;
		if(position < this.size()/2) {
			fromLeft = true;
		}
		
		if(fromLeft) {
			ListNode<T> newNode = new ListNode<>(value);
			if(position == 0) {
				//add at the beginning
				newNode.next = this.first;
				this.first.previous = newNode;
				this.first = newNode;
				size++;
				return;
			}

			//need to get to element at position - 1
			ListNode<T> currentNode = this.first;
			int i = 0;
			while(i < position - 1) {
				currentNode = currentNode.next;
				i++;
			}
			newNode.previous = currentNode;
			newNode.next = currentNode.next;
			//update references of currentNode and currentNode.next
			currentNode.next.previous = newNode;
			currentNode.next = newNode;
		} else {
			ListNode<T> newNode = new ListNode<>(value);
			//need to get to element at position + 1
			ListNode<T> currentNode = this.last;
			int i = this.size();
			while(i > position + 1) {
				currentNode = currentNode.previous;
				i--;
			}
			newNode.next = currentNode;
			newNode.previous = currentNode.previous;
			currentNode.previous.next = newNode;
			currentNode.previous = newNode;
		}
		size++;
	}
	
	@Override
	public int indexOf(Object value) {
		if(value == null) {
			return -1;
		}
		
		ListNode<T> currentNode = this.first;
		int i = 0;
		while(currentNode != null) {
			if(currentNode.value.equals(value)) {
				return i;
			}
			
			currentNode = currentNode.next;
			i++;
		}
		
		return -1;
	}
	
	/**
	 * <p>Removes the element located at index {@code index}. Works at
	 * average complexity O(n/2), as it approaches from the side which the
	 * index is located closer to.</p>
	 * 
	 * Shifts the remaining elements indexes to the left(naturally algorithmically).
	 *
	 * @throws IndexOutOfBounds if index isn't in range [0, collection.size()>
	 * @param index the index at which an element shall be removed
	 */
	public void remove(int index) {
		Objects.checkIndex(index, size);
		
		//check if element is first or last for an easy deletion
		if(index == 0) {
			if(this.first.next != null) {
				//at least 2 elements in the list
				this.first.next.previous = null;
				this.first = this.first.next;
			} else {
				//only 1 element in the list
				this.first = this.last = null;
			}
			this.size--;
			return;
		} else if(index == this.size-1){
			/*
			 * If only 1 element then the upper if will fire off,
			 * so no need for copy paste code
			 */
			this.last.previous.next = null;
			this.last = this.last.previous;
			this.size--;
			return;
		}
		
		/*
		 * Same as in get and insert - first check 
		 * from which side to approach the index
		 */
		boolean fromLeft = false;
		if(index < this.size()/2) {
			fromLeft = true;
		}
		
		ListNode<T> currentNode;
		if(fromLeft) {
			//let's approach the element at pos - 1
			currentNode = this.first.next;		//already checked index 0
			int i = 1;
			while(i < index - 1) {
				currentNode = currentNode.next;
				i++;
			}
			currentNode.next = currentNode.next.next;
			currentNode.next.previous = currentNode;
		} else {
			//approach the element at pos + 1
			if(this.size > 3) {
				currentNode = this.last.previous;		//already checked index size-1
			} else {
				currentNode = this.last;				//hard to explain, just trust me on this one
				/*
				 * Draw it on a paper if you're still skeptical, it just breaks on size 3 because
				 * currentNode goes undr pos + 1, it actually goes to the node you're deleting itself,
				 * and that breaks the algorithmic part underneath
				 */
			}
			int i = this.size()-2;
			while(i > index + 1) {
				currentNode = currentNode.previous;
				i--;
			}
			currentNode.previous = currentNode.previous.previous;
			currentNode.previous.next = currentNode;
		}
		this.size--;
	}

	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ListElementsGetter<>(this, modificationCount);
	}
}
