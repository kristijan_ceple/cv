package hr.fer.zemris.java.custom.collections;

/**
 * Simple interface that tests the given {@link Object}
 * for a certain criteria, and is parameterized.
 * 
 * @author kikyy99
 *
 */
public interface Tester<T> {
	/**
	 * Tests the passed argument for a certain criteria
	 * 
	 * @param obj the object to be tested
	 * @return true or false, depending on the expression evaluation
	 */
	boolean test(T obj);
}
