/**
 * Package modelling a vector drawn from the origin of the Euclidean 2D space.
 * 
 * Professor: Marko Cupic
 * @author Kristijan Ceple
 * @version 1.0
 */
package hr.fer.zemris.math;
