package hr.fer.zemris.java.custom.collections;

/**
 * Exception to be thrown in case of an overflow
 * @author kikyy99
 *
 */
public class OverflowException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default empty constructor
	 */
	public OverflowException() {}
	
	/**
	 * Constructor that takes a message
	 * @param message the message accompanying the excepion
	 */
	public OverflowException(String message) {
		super(message);
	}
}
