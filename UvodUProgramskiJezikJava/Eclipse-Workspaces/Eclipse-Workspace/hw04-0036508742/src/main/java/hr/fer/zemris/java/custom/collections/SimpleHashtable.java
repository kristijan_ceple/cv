package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * A simple class modelling a hashtable. Implement the {@link Iterable} interface
 * @author kikyy99
 *
 * @param <K> represents key
 * @param <V> represents value
 */
public class SimpleHashtable<K, V> implements Iterable<SimpleHashtable.TableEntry<K,V>> {
	/**
	 * Counts modifications
	 */
	private int modificationsCounter;
	/**
	 * The ratio after which the table's capacity shall be doubled
	 */
	private static final double FILL_RATIO_BOUND = 0.75;
	/**
	 * The default capacity
	 */
	private static final int DEFAULT_CAPACITY = 16;

	/**
	 * Table used to hold hash values
	 */
	private TableEntry<K, V>[] table;
	
	/**
	 * Amount of slots take
	 */
	private int tableSlotsFilled;
	
	/**
	 * Amount of all the pairs located in the hashtable
	 */
	private int size;
	
	/**
	 * A helper inner static class representing a single
	 * table entry
	 * @author kikyy99
	 *
	 * @param <K> represents key
	 * @param <V> represents value
	 */
	public static class TableEntry<K, V> {
		
		/**
		 * Key
		 */
		private K key;
		/**
		 * Value
		 */
		private V value;
		/**
		 * Reference to the next table entry(if any) in this slot's list. If there is only a
		 * single table entry in this slot, this reference points to null.
		 */
		private TableEntry<K, V> next;
		
		/**
		 * 2-argument constructor
		 * @param key The key to denote the entry - <strong>CANNOT</strong>  be null, <strong>MUST</strong> be unique
		 * @param value the value to be put into this table entry
		 */
		public TableEntry(K key, V value) {
			Objects.requireNonNull(key);
			
			this.key = key;
			this.value = value;
		}

		/**
		 * Returns the value of this table entry
		 * @return the value
		 */
		public V getValue() {
			return value;
		}

		/**
		 * Sets the value of this table entry
		 * @param value the value which this table entry's field {@link TableEntry#value}
		 * should be set to
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * Key getter
		 * @return key
		 */
		public K getKey() {
			return key;
		}
		
		public String toString() {
			return key + "=" + (value != null ? value : "null");
		}

		@Override
		public int hashCode() {
			return Objects.hash(key);
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableEntry))
				return false;
			TableEntry<K, V> other = (TableEntry<K, V>) obj;
			return Objects.equals(key, other.key);
		}
		
		
	}
	
	/**
	 * Inner static implementation of an {@link Iterator}.
	 * @author kikyy99
	 *
	 */
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>> {
		
		/**
		 * Used for checking if any modifications had been made on the hashtable
		 */
		private int savedModifications;
		/**
		 * The key last processed
		 */
		private TableEntry<K,V> currentEntry;
		/**index
		 * Index of the key NEXT TO BE PROCESSED
		 */
		private int currentIndex;
		
		/**
		 * 1-argument constructor
		 * @param savedModifications modifications made to the hashtable 
		 * before this constructor's inception 
		 * 
		 */
		public IteratorImpl(int savedModifications) {
			this.savedModifications = savedModifications;
		}
		
		@Override
		public boolean hasNext() {
			if(savedModifications != SimpleHashtable.this.modificationsCounter) {
				throw new ConcurrentModificationException("Hashtable has been changed during this "
						+ "Iterator's lifetime!");
			} else if(currentIndex < SimpleHashtable.this.size) {
				return true;
			} else {
				return false;
			}
		}
		
		@Override
		public SimpleHashtable.TableEntry<K, V> next(){
			if(savedModifications != SimpleHashtable.this.modificationsCounter) {
				throw new ConcurrentModificationException("Hashtable has been changed during this "
						+ "Iterator's lifetime!");
			}
			
			if(!hasNext()) {
				throw new NoSuchElementException("No more elements left to iterate over!");
			}
			
			//else get the next element
			TableEntry<K,V> entry = getEntryFromIndex(currentIndex++);
			currentEntry = entry;
			return entry;
		}
		
		@Override
		public void remove() {
			if(savedModifications != SimpleHashtable.this.modificationsCounter) {
				throw new ConcurrentModificationException("Hashtable has been changed during this "
						+ "Iterator's lifetime!");
			}
			
			if(currentEntry == null) {
				throw new IllegalStateException("Method getNext() hasn't yet been called on any elements, "
						+ "or an element has already been removed!");
			}
			
			SimpleHashtable.this.remove(currentEntry.key);
			currentEntry = null;
			currentIndex--;
			this.savedModifications++;
		}
		/**
		 * Helper function - iterates sequentially until it picks up
		 * the key located at the specified index.
		 * 
		 */
		private TableEntry<K,V> getEntryFromIndex(int index) {
			Objects.checkIndex(0, SimpleHashtable.this.size);
			
			int j = 0;	
			for(TableEntry<K,V> entry : table) {
				if(entry == null) {
					continue;
				}
					
				//Maybe this is our index?
				if(j == index) {
					return entry;
				}
					
				//now iterate over the list if necessary
				j++;	//we've gone over a key - increase j
				entry = entry.next;
				while(entry != null) {
					if(j == index) {
						return entry;
					} else {
						j++;
						entry = entry.next;
					}
				}
			}
				
			return null;
		}
	}
	
	/**
	 * Default constructor - uses default capacity {@link SimpleHashtable#DEFAULT_CAPACITY}
	 */
	public SimpleHashtable() {
		this(DEFAULT_CAPACITY);
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 1-arg constructor that sets the initial capacity to the desired value
	 * 
	 * @param capacity the initial capacity - it shall not be used. Instead, the closest
	 * power of 2 that is >= shall be used instead
	 */
	public SimpleHashtable(int capacity) {
		if(capacity < 1) {
			throw new IllegalArgumentException("Arguments passed to constructor less than 1 is unallowed!");
		}
		
		int actualCapacity = closestPowerOfTwo(capacity);
		table = (TableEntry<K,V>[])new TableEntry[actualCapacity];
	}
	
	/**
	 * Finds the nearest power of 2 that's >= the passed argument.
	 * @param number the number which powers of 2 shall be compared to
	 * @return the nearest power of 2 that's >=
	 */
	private int closestPowerOfTwo(int number) {
		int power = 1;
		while(power < number) {
			power*=2;
			
			//check for overflow
			if(power <= 0) {
				throw new OverflowException("Power overflown!");
			}
		}
		
		return power;
	}
	
	/**
	 * Puts the values into the hashtable - overwrites if a key is
	 * already found in the table
	 * 
	 * @param key the key to look for and insert/replace
	 * @param value the value to put
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);

		int address = calculateSlotAddress(key.hashCode(), table.length);
		if(table[address] == null) {
			//increase amount of slots taken
			tableSlotsFilled++;
			if(checkFillRatio()) {
				/*
				 * if we had to make a new table, the old slotsfilledvalue is now
				 * gone. So the tableSlotsFilled++ before the if triggered the
				 * creation of a double-length array - but now that element
				 * is being added into the NEW TABLE, so it must increase the
				 * slots variable again - because the method checkFillRatio()
				 * erased it.
				 * 
				 * Also the old address is fault - update it
				 */
				tableSlotsFilled++;
				address = calculateSlotAddress(key.hashCode(), table.length);
			}
			table[address] = new TableEntry<K, V>(key, value);
			size++;
			modificationsCounter++;
			//that's it I hope?
		} else {
			/*
			 * Append to the end of the list, on the way there check
			 * if such key already exists
			 */
			slotListPut(key, value, address, table);
		}
	}
	
	/**
	 * Helper method used to check if the fill ratio
	 * is over a certain percentage, as denoted by 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean checkFillRatio() {
		if(tableSlotsFilled/table.length < FILL_RATIO_BOUND) {
			return false;
		}
		
		modificationsCounter++;
		int originalSize = size;	//size shall stay the same
		//need to update our table
		TableEntry<K,V>[] newTable = (TableEntry<K,V>[])new TableEntry[table.length*2];
		int newSlotsFilled = 0;
		//iterate over slots
		for(TableEntry<K,V> entry : table) {
			//iterate over slot lists
			while(entry != null) {
				newSlotsFilled += putIntoTable(entry.key, entry.value, newTable);
				entry = entry.next;
			}
		}
		
		table = newTable;
		tableSlotsFilled = newSlotsFilled;
		size = originalSize;
		return true;
	}
	
	/**
	 * Helper method used for putting values into slot lists
	 * @param key the key
	 * @param value the value
	 * @param address the address of the slot
	 * @param table the table used
	 */
	private void slotListPut(K key, V value, int address, TableEntry<K,V>[] table) {
		/*
		 * Append to the end of the list, on the way there check
		 * if such key already exists
		 */
		TableEntry<K, V> currentNode = table[address];
		
		if(currentNode.key.equals(key)) {
			currentNode.value = value;
			return;
		}
		
		while(currentNode.next != null) {
			if(currentNode.next.key.equals(key)) {
				currentNode.next.value = value;
				return;
			}
			currentNode = currentNode.next;
		}
		//currentNode.next is null- THERE WE SHALL INSTANTIATE A NEW TABLEENTRY
		currentNode.next = new TableEntry<K, V>(key, value);
		size++;
		modificationsCounter++;
	}
	
	/**
	 * Helper method. Puts {@link TableEntry} into the specified argument {@code table}.
	 * 
	 * @param key the key
	 * @param value the value
	 * @param table the table
	 * @return amount of slots taken up
	 */
	private int putIntoTable(K key, V value, TableEntry<K, V>[] table) {
		Objects.requireNonNull(key);

		int address = calculateSlotAddress(key.hashCode(), table.length);
		if(table[address] == null) {
			//increase amount of slots taken
			table[address] = new TableEntry<K, V>(key, value);
			return 1;
			//that's it I hope?
		} else {
			/*
			 * Append to the end of the list, on the way there check
			 * if such key already exists
			 */
			slotListPut(key, value, address, table);
		}
		
		return 0;
	}
	
	/**
	 * Returns the {@link SimpleHashtable#TableEntry#value} 
	 * @param key
	 * @return the value under the key(if such is found in the table)
	 */
	public V get(Object key) {
		if(key == null) {
			return null;
		}
		
		int address = calculateSlotAddress(key.hashCode(), table.length);
		
		if(table[address] == null) {
			return null;		//no such entry in the table
		} else {
			//check them using equals
			TableEntry<K, V> currentNode = table[address];
			while(currentNode!=null) {
				//check the value of current node, then move on to the next
				if(currentNode.key.equals(key)) {
					return currentNode.value;
				} else {
					currentNode = currentNode.next;
				}
			}
			
			return null;
		}
	}

	/**
	 * Returns the size of this hashtable
	 * @return size of this hashtable
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Returns whether the hashtable contains the passed key
	 * 
	 * @param key key to look for
	 * @return whether the hashtable contains the passed key
	 */
	public boolean containsKey(Object key) {
		if(key == null) {
			return false;
		}
		
		int address = calculateSlotAddress(key.hashCode(), table.length);
		
		if(table[address] == null) {
			return false;		//no such entry in the table
		} else {
			//check them using equals
			TableEntry<K, V> currentNode = table[address];
			while(currentNode!=null) {
				//check the value of current node, then move on to the next
				if(currentNode.key.equals(key)) {
					return true;
				} else {
					currentNode = currentNode.next;
				}
			}
			
			return false;
		}
	}
	
	/**
	 * Returns whether the hashtable contains the passed value
	 * 
	 * @param value value to look for
	 * @return whether the hashtable contains the passed value
	 */
	public boolean containsValue(Object value) {
		//will have to iterate over the whole table...
		for(TableEntry<K,V> entry : table) {
			//check values
			while(entry!=null) {
				//iterate over the whole list, and check for values
				if(entry.value == null && value == null) {
					return true;
				} else if(entry.value.equals(value)) {
					return true;
				} else {
					entry = entry.next;
				}
			}
			//else if the current entry is null jut skip it
		}
		
		return false;
	}
	
	/**
	 * Removes the entry from the table of which key equals the passed
	 * argument key
	 * @param key the key of which entry should be removed
	 */
	public void remove(Object key) {
		if(key == null) {
			return;
		}
		
		int address = calculateSlotAddress(key.hashCode(), table.length);
		
		if(table[address] == null) {
			return;		//no such entry in the table
		} else {
			/*
			 * Check them using equals!
			 * 
			 * But first, there are 2 cases: either this entry is the lone list entry
			 * in this slot -- OOORR there is a list made up of 2 or more entries in
			 * this slot.
			 *
			 * Need to differentiate these 2 cases for easier deletion
			 */
			
			TableEntry<K, V> currentNode = table[address];
			if(currentNode.key.equals(key)) {
				if(currentNode.next==null) {
					//excellent, just remove this entry
					table[address]=null;
					size--;
					modificationsCounter++;
				} else {
					//need to reroute the references
					table[address] = currentNode.next;
					size--;
					modificationsCounter++;
				}
				return;
			}
			
			//okay, maybe it's deeper in the list, like at the 2nd, 3rd, or so... place
			while(currentNode.next!=null) {
				//check the value of next node
				if(currentNode.next.key.equals(key)) {
					//reroute!
					currentNode.next = currentNode.next.next;
					size--;
					modificationsCounter++;
					return;
				} else {
					//ehh, miss. try moving down the list
					currentNode = currentNode.next;
				}
			}
		}
	}
	
	/**
	 * Checks whether the table contains any pairs
	 * @return whether the table contains any pairs
	 */
	public boolean isEmpty() {
		return size==0;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		int j = 0;
		for(TableEntry<K, V> entry : table) {
			if(entry == null) {
				continue;
			}
			
			sb.append(entry);
			j++;
			//check if the list goes on?
			entry = entry.next;
			while(entry!=null) {
				sb.append(", ");
				sb.append(entry);
				j++;
				entry = entry.next;
			}
			
			if(j < size-1) {
				sb.append(", ");
			}
		}
		return sb.append("]").toString();
	}
	
	/**
	 * Helper method used for calculating the slot address. Calculates by the
	 * formula |hashcode|%2
	 * @param hashcode the passed hashcode to be used in the calculation
	 * @return the slot address
	 */
	private int calculateSlotAddress(int hashcode, int length) {
		return (hashcode > 0 ? hashcode : -hashcode)%length;
	}
	
	/**
	 * Clears the table
	 */
	public void clear() {
		for(int i = 0; i < table.length; i++) {
			table[i] = null;
		}
		
		//this approach doesn't work
//		for(TableEntry<K,V> entry : table) {
//			entry = null;
//		}
		
		this.size = 0;
		this.tableSlotsFilled = 0;
		modificationsCounter++;
	}

	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl(this.modificationsCounter);
	}
}
