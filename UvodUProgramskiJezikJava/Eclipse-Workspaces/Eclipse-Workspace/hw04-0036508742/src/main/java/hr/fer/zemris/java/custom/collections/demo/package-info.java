/**
 * Demo package used for testing the {@link hr.fer.zemris.java.custom.collections.SimpleHashtable} class
 * 
 * Professor: Marko Cupic
 * @author Kristijan Ceple
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections.demo;
