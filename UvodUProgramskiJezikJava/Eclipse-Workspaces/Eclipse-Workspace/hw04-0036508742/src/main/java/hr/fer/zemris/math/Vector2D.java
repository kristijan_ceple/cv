package hr.fer.zemris.math;

/**
 * Class modelling a simple vector.
 * @author kikyy99
 *
 */
public class Vector2D {
	
	/**
	 * X component
	 */
	private double x;
	/**
	 * Y component
	 */
	private double y;
	
	/**
	 * Length - set internally
	 */
	private double length;
	/**
	 * Angle - set internally
	 */
	private double angle;
	
	/**
	 * Constructor.
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
		updatePolar();
	}
	
	/**
	 * Returns a new instance of {@link Vector2D} as specified by the
	 * arguments passed
	 * 
	 * @param length the length of the vector
	 * @param angle the angle of the vector
	 * @return new instance of {@link Vector2D} as specified by the
	 * arguments passed
	 */
	private static Vector2D fromAngleAndLength(double length, double angle) {
		double x = Math.cos(angle) * length;
		double y = Math.sin(angle) * length;
		
		return new Vector2D(x, y);
	}

	/**
	 * Returns x coordinate
	 * @return x coordinate
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Returns y coordinate
	 * @return y coordinate
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Updates the current vector according to the given value
	 * @param offset the {@link Vector} to be used as offset
	 */
	public void translate(Vector2D offset) {
		this.x+=offset.x;
		this.y+=offset.y;
		updatePolar();
	}
	
	/**
	 * Returns a new {@link Vector} that is produced by taking
	 * this vector and offsetting it with the given value
	 * @param offset the offset to be used
	 * @return a new {@link Vector}
	 */
	public Vector2D translated(Vector2D offset){
		return new Vector2D(this.x+offset.x, this.y+offset.y);
	}
	
	/**
	 * Rotates the current vector counter-clockwise
	 * @param angle the angle to be rotated by counter-clockwise
	 */
	public void rotate(double angle) {
		this.angle+=angle;
		updateRectangular();
	}
	
	/**
	 * Returns a new instance of a vector that is produced by
	 * rotating this vector by the given angle counter-clockwise
	 * @param angle the angle to be rotated by counter-clockwise
	 * @return a new {@link Vector}
	 */
	public Vector2D rotated(double angle) {
		return fromAngleAndLength(this.length, this.angle+angle);
	}
	
	/**
	 * Scales the current vector
	 * @param scaler the multiplier
	 */
	public void scale(double scaler) {
		if(scaler < 0) {
			throw new IllegalArgumentException("Scaler must be non-negative!");
		}
		
		this.x *= scaler;
		this.y *= scaler;
		updatePolar();
	}
	
	/**
	 * Returns a new instance of a vector that is produced by
	 * scaling this vector by the given scalar
	 * @param scaler the multiplier
	 * @return a new {@link Vector}
	 */
	public Vector2D scaled(double scaler) {
		if(scaler < 0) {
			throw new IllegalArgumentException("Scaler must be non-negative!");
		}
		
		return new Vector2D(x*scaler, y*scaler);
	}
	
	/**
	 * Returns the copy of this {@link Vector}
	 * @return the copy of this {@link Vector}
	 */
	public Vector2D copy() {
		return new Vector2D(x, y);
	}
	
	/**
	 * Updates the polar coordinates of the current vector
	 */
	private void updatePolar() {
		this.length = Math.sqrt(this.x*this.x + this.y*this.y);
		angle = Math.atan2(this.y, this.x) + 2*Math.PI;
	}
	
	/**
	 * Updates the rectangular coordinates of the current vector
	 */
	private void updateRectangular() {
		this.x = Math.cos(angle) * length;
		this.y = Math.sin(angle) * length;
	}

	/**
	 * Length getter
	 * @return length
	 */
	public double getLength() {
		return length;
	}
	
	
}
