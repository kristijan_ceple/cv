package hr.fer.zemris.java.pred2;

import java.util.Objects;

public class GeometrijskiLik extends Object {

	private String ime;
	
	// public GeometrijskiLik(GeometrijskiLik this, String ime) {
	public GeometrijskiLik(String ime) {
		this.ime = ime;
	}
	
	/*
	 * String GeometrijskiLik::getIme(GeometrijskiLik this) {
	 * 	return this.ime;
	 * }
	 */
	public String getIme() {
		return this.ime;
	}
	
	public double getOpseg() {
		return 0.0;
	}
	
	public double getPovrsina() {
		return 0.0;
	}
	
	@Override
	public String toString() {
		return "ja sam lik" + ime;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)		//najjednostavnije utvrditi jesu li na istoj adresi - zbog brzine
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof GeometrijskiLik))	//	instanceof apsorbira null
			return false;
		GeometrijskiLik other = (GeometrijskiLik) obj;
		return Objects.equals(ime, other.ime);
	}

	
}
