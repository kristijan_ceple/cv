package hr.fer.zemris.java.pred2;

public class Primjer1 {
	public static void main(String[] args) {
		GeometrijskiLik l1 = new GeometrijskiLik("Lik1");
		GeometrijskiLik l2 = new GeometrijskiLik("Lik2");
		
		System.out.printf("Ime prvog lika je %s.%n", l1.getIme()); // GeometrijskiLik::getIme(l1);
		
		System.out.printf("Ime drugog lika je %s.%n", l2.getIme()); // GeometrijskiLik::getIme(l2);
		
		
		//u stvarnosti, GeometrijskiLik::getIme(l1); ce zapravo biti:
		//l1->(tbrptr[0])(l1);
	}
}
