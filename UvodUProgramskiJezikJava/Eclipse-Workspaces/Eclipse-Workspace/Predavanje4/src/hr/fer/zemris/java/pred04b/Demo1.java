package hr.fer.zemris.java.pred04b;

import java.util.Iterator;

public class Demo1 {

	public static void main(String[] args) {
		Kvartet<String> k1 = new Kvartet<>("Ana", "Jasna", "Ivana", "Petra");
		Kvartet<Integer> k2 = new Kvartet<>(5, 1, 4, 42);
		
		Iterator<String> it1 = k1.iterator();	//ovo rijesiti pomocu factory methoda
		while(it1.hasNext()) {
			String elem = it1.next();
			System.out.println(elem);
		}
		
		Iterator<Integer>  it2 = k2.new IteratorImpl4();		//k2 je otisao kao skriveni argument u konstruktor
		//on ce kasnije u instanci toga razreda postati this
		
		//ovo je sintaksni secer za gore while petlju
		for(String ime : k1) {
			System.out.println(ime);
		}
		
		for(Integer ime : k2) {
			System.out.println(ime);
		}
	}
	
}
