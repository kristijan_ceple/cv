package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArrayIndexedCollectionTest {

	private ArrayIndexedCollection col;

	@BeforeEach
	void setUp() throws Exception {
		col = new ArrayIndexedCollection(4);
		col.add("New York");
		col.add("Budimpest");
		col.add("London");
		col.add("Berlin");
	}

	@Test
	void testSize() {
		var colEmpty = new ArrayIndexedCollection(4);
		assertTrue(colEmpty.size() == 0);
		colEmpty.add("Java");
		assertTrue(colEmpty.size() == 1);
		colEmpty.add("C++");
		assertTrue(colEmpty.size() == 2);
		
		assertTrue(col.size()==4);
		col.add("Munchen");
		assertTrue(col.size()==5);
		col.add("Tokyo");
		col.add("Shenzen");
		assertTrue(col.size()==7);
	}

	@Test
	void testAdd() {
		assertFalse(col.contains("Washington"));
		col.add("Washington");
		assertTrue(col.contains("Washington"));
		assertThrows(NullPointerException.class, ()-> col.add(null));
		assertFalse(col.contains("Hamburg"));
		col.add("Hamburg");
		assertTrue(col.contains("Hamburg"));
	}

	@Test
	void testContains() {
		assertFalse(col.contains(null));
		assertTrue(col.contains("Berlin"));
		assertFalse(col.contains("Whaaat"));
		assertTrue(col.contains("Berlin"));
		col.add("Whatever");
		assertTrue(col.contains(null));
		assertTrue(col.contains("Whatever"));
	}

	@Test
	void testRemoveObject() {
		assertTrue(col.size()==4);
		assertThrows(IndexOutOfBoundsException.class, ()->col.remove(-1));
		assertThrows(IndexOutOfBoundsException.class, ()->col.remove(4));
		
		col.remove(0);
		assertTrue(col.size()==3);
		assertThrows(IndexOutOfBoundsException.class, ()->col.remove(3));
		col.remove(2);
		assertTrue(col.size()==2);
		assertThrows(IndexOutOfBoundsException.class, ()->col.remove(2));
	}
	static final int INITIAL_CAPACITY = 16;
	@Test
	void testToArray() {
		assertArrayEquals(new Object[]{"New York",  "Budimpest", "London", "Berlin"}, col.toArray());
		col.remove(3);
		assertArrayEquals(new Object[]{"New York",  "Budimpest", "London"}, col.toArray());
		col.add("Singapore");
		assertArrayEquals(new Object[]{"New York",  "Budimpest", "London", "Singapore"}, col.toArray());
		col.add("Paris");
		col.add("Madrid");
		assertArrayEquals(new Object[]{"New York",  "Budimpest", "London", "Singapore", "Paris", "Madrid"}, col.toArray());
	}

	@Test
	void testForEach() {
		class PrintProcessor extends Processor {
			
			@Override
			public void process(Object value) {
				System.out.println(value);
			}
		} 
		
		col.forEach(new PrintProcessor());
	}

	@Test
	void testClear() {
		col.clear();
		assertTrue(col.size()==0);
	}

	@Test
	void testArrayIndexedCollection() {
		var newCol = new ArrayIndexedCollection();
		var initialCapacity = ArrayIndexedCollection.INITIAL_CAPACITY;
		constructorTestRoutine_EmptyAndInt(newCol, initialCapacity);
		
	}

	void constructorTestRoutine_EmptyAndInt(ArrayIndexedCollection newCol, int initialCapacity) {
		assertTrue(newCol.getCapacity() == initialCapacity);
		assertTrue(newCol.size()==0);
		for(var i = 0; i <= initialCapacity; i++) {
			newCol.add(i);
		}
		assertTrue(newCol.size()==(initialCapacity+1));
		assertTrue(newCol.getCapacity() == initialCapacity*2);
	}
	
	@Test
	void testArrayIndexedCollectionInt() {
		var newCol = new ArrayIndexedCollection(32);
		constructorTestRoutine_EmptyAndInt(newCol, 32);
		assertThrows(IllegalArgumentException.class, ()-> new ArrayIndexedCollection(0));
		assertThrows(IllegalArgumentException.class, ()-> new ArrayIndexedCollection(-1));
	}

	@Test
	void testArrayIndexedCollectionCollection() {
		var newCol = new ArrayIndexedCollection(col);
		constructorTestRoutine_ColAndInt(newCol, 4);
	}

	void constructorTestRoutine_ColAndInt(ArrayIndexedCollection newCol, int initialCapacity) {
		assertTrue(newCol.getCapacity() == initialCapacity);
		assertTrue(newCol.size() == 4);
		assertArrayEquals(new Object[]{"New York",  "Budimpest", "London", "Berlin"}, newCol.toArray());
	}
	
	@Test
	void testArrayIndexedCollectionIntCollection() {
		var newCol = new ArrayIndexedCollection(4, col);
		constructorTestRoutine_ColAndInt(newCol, 4);
		
		newCol = new ArrayIndexedCollection(0, col);
		constructorTestRoutine_ColAndInt(newCol, 4);
		
		newCol = new ArrayIndexedCollection(-1, col);
		constructorTestRoutine_ColAndInt(newCol, 4);
		
		newCol = new ArrayIndexedCollection(1, col);
		constructorTestRoutine_ColAndInt(newCol, 4);
		
		newCol = new ArrayIndexedCollection(2, col);
		constructorTestRoutine_ColAndInt(newCol, 4);
		
		newCol = new ArrayIndexedCollection(5, col);
		constructorTestRoutine_ColAndInt(newCol, 5);
		
		newCol = new ArrayIndexedCollection(1000, col);
		constructorTestRoutine_ColAndInt(newCol, 1000);
		
		assertDoesNotThrow(()-> new ArrayIndexedCollection(0, col));
		assertDoesNotThrow(()-> new ArrayIndexedCollection(-1, col));
	}

	@Test
	void testGet() {
		assertEquals("New York", col.get(0));
		assertEquals("Budimpest", col.get(1));
		assertEquals("London", col.get(2));
		assertEquals("Berlin", col.get(3));
		assertThrows(IndexOutOfBoundsException.class, ()-> col.get(-1));
		assertThrows(IndexOutOfBoundsException.class, ()-> col.get(4));
	}

	@Test
	void testInsert() {
		col.insert("Insertion", 2);
		assertEquals("New York", col.get(0));
		assertEquals("Budimpest", col.get(1));;
		assertEquals("Insertion", col.get(2));
		assertEquals("London", col.get(3));
		assertEquals("Berlin", col.get(4));
		assertThrows(IndexOutOfBoundsException.class, () -> col.insert("Fail", -1));
		assertDoesNotThrow(()->col.insert("Success", 0));
		assertArrayEquals(new Object[] {"Success", "New York", "Budimpest", "Insertion", "London", "Berlin"}, col.toArray());
		
		col.insert("In the end", 6);
		assertArrayEquals(new Object[] {"Success", "New York", "Budimpest", "Insertion", "London", "Berlin", "In the end"}, col.toArray());
		
		assertThrows(IndexOutOfBoundsException.class, ()->col.insert("Out of bounds", 8));
	}

	@Test
	void testIndexOf() {
		assertEquals(0, col.indexOf("New York"));
		assertEquals(2, col.indexOf("London"));
		assertEquals(3, col.indexOf("Berlin"));
		assertEquals(-1, col.indexOf("AAAAA"));
	}

	@Test
	void testRemoveInt() {
		assertThrows(IndexOutOfBoundsException.class, ()-> col.remove(4));
		assertThrows(IndexOutOfBoundsException.class, ()-> col.remove(-1));
		col.remove(3);
		assertTrue(col.size()==3);
		assertArrayEquals(new Object[] {"New York", "Budimpest", "London"}, col.toArray());
		
		col.remove(1);
		assertTrue(col.size()==2);
		assertArrayEquals(new Object[] {"New York", "London"}, col.toArray());
	}

	@Test
	void testIsEmpty() {
		assertFalse(col.isEmpty());
		var newCol = new ArrayIndexedCollection();
		assertTrue(newCol.isEmpty());
	}

	@Test
	void testAddAll() {
		var newCol = new ArrayIndexedCollection(); 
		newCol.addAll(col);
		assertTrue(newCol.size()==col.size());
		assertArrayEquals(new Object[] {"New York", "Budimpest", "London", "Berlin"}, col.toArray());
		assertArrayEquals(new Object[] {"New York", "Budimpest", "London", "Berlin"}, newCol.toArray());
		
		col.addAll(col);
		assertArrayEquals(new Object[] {"New York", "Budimpest", "London", "Berlin",
				"New York", "Budimpest", "London", "Berlin"}, col.toArray());
	}
	
	@Test
	void testGetInitialCapacity() {
		assertEquals(16, ArrayIndexedCollection.getInitialCapacity());
	}
	
	@Test
	void testGetCapacity() {
		assertEquals(4, col.getCapacity());
		col.add("Santa Barbara");
		assertEquals(8, col.getCapacity());
		for(int i = 0; i < 3; i++) {
			col.add(i);
		}
		assertEquals(8, col.getCapacity());
		col.add("Over Capacity");
		assertEquals(16, col.getCapacity());
		
		var newCol = new ArrayIndexedCollection(32);
		assertEquals(32, newCol.getCapacity());
	}

}
