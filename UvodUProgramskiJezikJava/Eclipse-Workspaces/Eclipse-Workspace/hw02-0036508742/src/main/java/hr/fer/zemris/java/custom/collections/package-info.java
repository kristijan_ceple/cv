/**
 * Second homework in the course: Introduction to Java in academic year 2018/2019.
 * Features all the collections implementations as well as the basis class <code>Collection</code>
 * 
 * Professor: Marko Cupic
 * @author Kristijan Ceple
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections;
