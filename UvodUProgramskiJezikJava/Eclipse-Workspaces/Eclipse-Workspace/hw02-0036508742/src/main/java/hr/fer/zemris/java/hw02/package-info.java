/**
 * Second homework in the course: Introduction to Java in academic year 2018/2019.
 * This package features the <code>ComplexNumber</code> class and a helping debug class <code>Main</code>
 * 
 * Professor: Marko Cupic
 * @author Kristijan Ceple
 * @version 1.0
 */
package hr.fer.zemris.java.hw02;
