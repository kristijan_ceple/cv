package hr.fer.zemris.java.hw02;

import java.util.Scanner;

/**
 * This class was used for debugging the parse function.
 * 
 * @author kikyy99
 */
public class Main {
	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in)){
			while(true) {
				System.out.print("Input > ");
				String input = sc.nextLine();
//				System.out.println("Your input: " + input);
				if(input.isEmpty()) {
					System.out.println("Empty input!");
				}
				ComplexNumber received = ComplexNumber.parse(input);
				System.out.println(received);
				System.out.println("Angle = " + received.getAngle() * 180/Math.PI);
				System.out.println("Magnitude = " + received.getMagnitude());
			}
		}
	}
}
