package hr.fer.zemris.java.p01;

import static org.junit.jupiter.api.Assertions.*;  //import all static methods, shortens function calling
import org.junit.jupiter.api.Test;

public class P07Test {

	@Test
	public void prazanStringNulaAova() {
		int n = P07.broji("");
		assertEquals(0, n);
	}

	@Test
	public void samoSlovoA() {
		int n = P07.broji("A");
		assertEquals(1, n);
	}
}
