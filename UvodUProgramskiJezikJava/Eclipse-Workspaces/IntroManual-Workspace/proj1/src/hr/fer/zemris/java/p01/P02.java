package hr.fer.zemris.java.p01;

public class P02{
	public static void main(String[] args) {
		int i = 1;		
		if(args.length==0){
			System.err.println("No args sent!");			
			System.exit(0);
		}		
		for(String arg : args){
			System.out.printf("[%d] => %s%n", i++, arg);
		}	
	}

}
