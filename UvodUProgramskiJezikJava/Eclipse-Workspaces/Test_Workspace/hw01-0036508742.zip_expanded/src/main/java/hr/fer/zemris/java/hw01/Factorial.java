
package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * A simple user IO program that calculates factorials.
 * 
 * @author kikyy99
 * @version 1.0
 */
public class Factorial {

	/**
	 * The starting method of the program - does the IO, calls the factorial
	 * calculation function.
	 * 
	 * @param args command line arguments - not used in this program
	 */
	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in)) {
			while(true) {
				System.out.print("Unesite broj > ");
				String input = sc.nextLine();
				
				if(input.equalsIgnoreCase("kraj")) {
					break;
				}
				
				try {
					Integer number = Integer.parseInt(input);
					if(number < 3 || number > 20) {
						throw new IllegalArgumentException(String.format("'%d' nije u dozvoljenom rasponu.", number));
					}
					
					long factorial = calculateFactorial(number);
					System.out.printf("%d! = %d%n", number, factorial);
				} catch(NumberFormatException ex){
					System.out.printf("'%s' nije cijeli broj.%n", input);
				} catch(IllegalArgumentException ex) {
					System.out.println(ex.getMessage());
				}
			}
		} finally {
			System.out.println("Dovidenja.");
		}
	}
	
	/**
	 * Recursive factorial calculation.
	 * 
	 * @param number the number of which a factorial will be calculated.
	 * @return factorial of parameter '<code>number</code>' as a primitive <code>long</code> data type
	 */
	public static long calculateFactorial(Integer number) {
		if(!(number instanceof Integer)) {
			throw new IllegalArgumentException(String.format("Ilegalan tip argumenta '%d' poslan funkciji za racunanje faktorijela!", number));
		} else if(number < 0) {
			throw new IllegalArgumentException(String.format("Argument '%d' manji od nule", number));
		}
		
		if(number == 0 || number == 1) {
			return 1;
		}
		
		return number * calculateFactorial(Integer.valueOf(number-1));
	}
}
