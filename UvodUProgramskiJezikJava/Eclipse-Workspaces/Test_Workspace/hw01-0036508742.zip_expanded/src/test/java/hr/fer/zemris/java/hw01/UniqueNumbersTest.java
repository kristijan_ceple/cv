package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;

import static hr.fer.zemris.java.hw01.UniqueNumbers.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
class UniqueNumbersTest {

	private TreeNode glava = null;
	
	@Test
	@Order(1)
	void complete_test() {
		assertEquals(0, treeSize(glava), "Velicina bi trebala biti 0");
		
		glava = addNode(glava, 42);
		fastAdd(glava, 42, false);
		assertEquals(1, treeSize(glava), "Velicina bi trebala biti 1");
		
		glava = addNode(glava, 76);
		fastAdd(glava, 76, false);
		assertEquals(2, treeSize(glava), "Velicina bi trebala biti 2");
		
		glava = addNode(glava, 21);
		fastAdd(glava, 21, false);
		assertEquals(3, treeSize(glava), "Velicina bi trebala biti 3");
		
		glava = addNode(glava, 76);
		fastAdd(glava, 76, true);
		assertEquals(3, treeSize(glava), "Velicina bi trebala ostati 3");

		glava = addNode(glava, 35);
		fastAdd(glava, 35, false);
		assertEquals(4, treeSize(glava), "Velicina bi trebala biti 4");
		
		glava = addNode(glava, 42);
		fastAdd(glava, 42, true);
		assertEquals(4, treeSize(glava), "Velicina bi trebala ostati 4");
		
		glava = addNode(glava, 41);
		fastAdd(glava, 41, false);
		assertEquals(5, treeSize(glava), "Velicina bi trebala biti 5");
		
		glava = addNode(glava, 55);
		fastAdd(glava, 55, false);
		assertEquals(6, treeSize(glava), "Velicina bi trebala biti 6");
		
		glava = addNode(glava, 20);
		fastAdd(glava, 20, false);
		assertEquals(7, treeSize(glava), "Velicina bi trebala biti 7");
		
		glava = addNode(glava, 100);
		fastAdd(glava, 100, false);
		assertEquals(8, treeSize(glava), "Velicina bi trebala biti 8");
		
		glava = addNode(glava, 101);
		fastAdd(glava, 101, false);
		assertEquals(9, treeSize(glava), "Velicina bi trebala biti 9");
		
		glava = addNode(glava, 102);
		fastAdd(glava, 102, false);
		assertEquals(10, treeSize(glava), "Velicina bi trebala biti 10");
		
		glava = addNode(glava, 102);
		fastAdd(glava, 102, true);
		assertEquals(10, treeSize(glava), "Velicina bi trebala ostati 10");
		
		glava = addNode(glava, 15);
		fastAdd(glava, 15, false);
		assertEquals(11, treeSize(glava), "Velicina bi trebala biti 11");
		
		glava = addNode(glava, 56);
		fastAdd(glava, 56, false);
		assertEquals(12, treeSize(glava), "Velicina bi trebala biti 12");
		
		glava = addNode(glava, 25);
		fastAdd(glava, 25, false);
		assertEquals(13, treeSize(glava), "Velicina bi trebala biti 13");
		
		glava = addNode(glava, 43);
		fastAdd(glava, 43, false);
		assertEquals(14, treeSize(glava), "Velicina bi trebala biti 14");
		
		System.out.println("Zasada testovi za dodavanja i velicinu se cine dobrima. "
				+ "Vrijeme je za provjeriti strukturu stabla");
		
		assertEquals(42, glava.value, "Vrijednost u glavi bi trebala biti 42");
		assertEquals(21, glava.left.value, "Vrijednost u glava.left bi trebala biti 21");
		assertEquals(76, glava.right.value, "Vrijednost u glava.right bi trebala biti 76");
		assertEquals(20, glava.left.left.value, "Vrijednost u glava.left.left bi trebala biti 20");
		assertEquals(35, glava.left.right.value, "Vrijednost u glava.left.right bi trebala biti 35");
		assertEquals(15, glava.left.left.left.value, "Vrijednost u glava.left.left.left bi trebala biti 15");
		assertEquals(25, glava.left.right.left.value, "Vrijednost u glava.left.right.left bi trebala biti 25");
		assertEquals(41, glava.left.right.right.value, "Vrijednost u glava.left.right.right bi trebala biti 41");
		assertEquals(55, glava.right.left.value, "Vrijednost u glava.right.left bi trebala biti 55");
		assertEquals(56, glava.right.left.right.value, "Vrijednost u glava.right.left.right bi trebala biti 56");
		assertEquals(43, glava.right.left.left.value, "Vrijednost u glava.right.left.left bi trebala biti 43");
		assertEquals(100, glava.right.right.value, "Vrijednost u glava.right.right bi trebala biti 100");
		assertEquals(101, glava.right.right.right.value, "Vrijednost u glava.right.right.right bi trebala biti 101");
		assertEquals(102, glava.right.right.right.right.value, "Vrijednost u glava.right.right.right.right bi trebala biti 102");
		
		System.out.println("Struktura stabla je u redu...");
	
	}
	
	@Test
	@Order(2)
	void containsValueTest() {
		/*
		 * Already have all the values added from earlier test methods.
		 * (Specifically, the complete_test() Test method)
		 * Just test if the containsValue function works properly
		 */
		assertTrue(containsValue(glava, 42));
		assertTrue(containsValue(glava, 21));
		assertTrue(containsValue(glava, 76));
		assertTrue(containsValue(glava, 20));
		assertTrue(containsValue(glava, 35));
		assertTrue(containsValue(glava, 15));
		assertTrue(containsValue(glava, 25));
		assertTrue(containsValue(glava, 41));
		assertTrue(containsValue(glava, 55));
		assertTrue(containsValue(glava, 100));
		assertTrue(containsValue(glava, 43));
		assertTrue(containsValue(glava, 56));
		assertTrue(containsValue(glava, 101));
		assertTrue(containsValue(glava, 102));
		
		System.out.println("Testovi prisutnosti uspjesni. Zapocinjem testove neprisutnosti...");
		
		assertFalse(containsValue(null, 0));
		assertFalse(containsValue(null, 10));
		assertFalse(containsValue(null, 103));
		assertFalse(containsValue(null, -5));
		assertFalse(containsValue(null, -10));
		
		assertFalse(containsValue(null, 1000000000));
		assertFalse(containsValue(null, -1000000000));
		assertFalse(containsValue(null, 67));
		
		System.out.println("Testovi neprisutnosti uspjesni...");
		System.out.println("Svi testovi USPJESNI!!!");
	}
	
	/**
	 * A helping function - called after adding the number into the binary tree.
	 * Basically is used for easier assertion and string printing.
	 * 
	 * Prints out 2 different Strings depending on the repeatingAddition parameter.
	 * 
	 * Asserts if the glava is not null, and asserts whether the containsValue function
	 * is able to find the number that was added PRIOR to this function call.
	 * 
	 * @param glava the root of our binary tree
	 * @param number the number that was added
	 * @param repeatingAddition whether the number being added is already contained
	 * in the binary tree.
	 */
	void fastAdd(TreeNode glava, int number, boolean repeatingAddition) {
		if(!repeatingAddition) {
			System.out.printf("Dodavanje broja %d u stablo...%n", number);
		} else {
			System.out.printf("Ponovno \"Dodavanje\" broja %d u stablo...%n", number);
		}
		assertNotNull(glava, "Glava JESTE null(a ne bi trebala biti).");
		assertTrue(containsValue(glava, number), String.format("Stablo ili ne sadrzi broj %d(addNode funkcija dobro ne "
				+ "postavi broj) ili funkcija containsValue nije ispravna.", number));
	}

}
