package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * <p>Models a <code>binary tree</code> consisting of only unique numbers. User inputs numbers
 * that are inserted into the binary tree. Features sorting in both ascending and
 * descending(reversed) order.</p>
 * 
 * <p>The tree itself is organised according to the following hierarchy structure:
 * <code>Left Child</code> - number lesser than the <code>Parent</code>, <code>Right Child</code> = number greater than the <code>Parent</code></p>
 * 
 * @author kikyy99
 *
 */
public class UniqueNumbers {

	/**
	 * <p>The main function. It awaits user input and adds numbers into the <code>binary tree</code>.
	 * User can cancel input submission and request that program exit by inputting
	 * string <code>"kraj"</code>.</p>
	 * 
	 * <p>Data insertion into the <code>binary tree</code> is done in the following fashion:
	 * first we check if the <code>number</code> is already present in the tree -&gt; if so, 
	 * don't add it, and print along the accompanying message.</p>
	 * 
	 * <p>If the <code>number</code> isn't present in the <code>binary tree</code> -&gt; we add it and print
	 * along the accompanying message.</p>
	 * 
	 * @param args command line arguments - unused in this program. If entered, program
	 * will exit with <code>status</code> code -1, signalling an error.
	 */
	public static void main(String[] args) {
		if(args.length != 0) {
			System.err.println("Program ne prihvaca nikakve argumente!");
			System.exit(-1);
		}
		
		TreeNode glava = null;
		try(Scanner sc = new Scanner(System.in)){
			while(true) {
				System.out.print("Unesite broj > ");
				String input = sc.nextLine();
				
				if(input.equalsIgnoreCase("kraj")) {
					break;
				}
				
				//Now try to parse the value inside a try-catch
				try {
					int value = Integer.parseInt(input);
					
					//Check for addition
					if(containsValue(glava, value)) {
						System.out.println("Broj vec postoji. Preskacem.");
					} else {
						//add it!
						glava = addNode(glava, value);
						System.out.println("Dodano");
					}
				} catch(NumberFormatException ex) {
					System.out.println(String.format("'%s' nije cijeli broj.", input));
				}
			}
		} finally {
			System.out.println("Ispis od najmanjeg: " + sortOrdered(glava));
			System.out.println("Ispis od najveceg: " + sortReverseOrdered(glava));
			System.out.println("Dovidenja.");
		}
		
	}
	
	
	/**
	 * <p>Sorts the tree by algorithmic means of the Inorder Traversal(<code>Left Child - Parent - Right Child</code>).
	 * Internally uses the <code>StringBuilder</code> class for a dynamic <code>String</code>. The function itself
	 * is <code>recursive</code>.</p>
	 * 
	 * <p>Ascending sort.</p>
	 * 
	 * @param glava The <code>root</code> of the <code>binary tree</code>
	 * @return a sorted <code>String</code>
	 */
	public static String sortOrdered(TreeNode glava) {
		if(glava == null) {
			return "";
		}
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(sortOrdered(glava.left));
		sb.append(glava.value).append(" ");
		sb.append(sortOrdered(glava.right));
		
		return sb.toString();
	}
	
	/**
	 * <strong>Practically the same as the <code>sortOrdered(TreeNode glava)</code> 
	 * function, although the sort is descending as opposed to being ascending.</strong>
	 * 
	 * <p>Sorts the tree by algorithmic means of the Inorder Traversal(<code>Right Child - Parent - Left Child</code>).
	 * Internally uses the <code>StringBuilder</code> class for a dynamic <code>String</code>. The function itself
	 * is <code>recursive</code>.</p>
	 * 
	 * <p>Descending sort.</p>
	 * 
	 * @param glava The <code>root</code> of the <code>binary tree</code>
	 * @return a sorted <code>String</code>
	 */
	public static String sortReverseOrdered(TreeNode glava) {
		if(glava == null) {
			return "";
		}
	
		StringBuilder sb = new StringBuilder();
	
		sb.append(sortReverseOrdered(glava.right));
		sb.append(glava.value).append(" ");
		sb.append(sortReverseOrdered(glava.left));
	
		return sb.toString();
	}
	
	/**
	 * <p>Tries to add a new node into the <code>binary tree</code>, <strong>UNLESS it's
	 * already present in the <code>binary tree</code></strong> in which case 
	 * it does <strong>NOTHING</strong></p>
	 * 
	 * <p>The function itself is <code>recursive</code>.</p>
	 * 
	 * @param glava the <code>root</code> of the <code>binary tree</code>
	 * @param value the value to be inserted into the <code>binary tree</code>
	 * @return argument <code>glava</code>
	 */
	public static TreeNode addNode(TreeNode glava, int value) {
		if(glava == null) {
			glava = new TreeNode(value);
			return glava;
		}
		
		if(value < glava.value) {
			//put the value left
			if(glava.left == null) {
				/*
				 * Superb, the left node doesn't exist, so we create one 
				 * with the desired value and return true for success
				 */
				glava.left = new TreeNode(value);
			} else {
				//Ehh we need to dig deeper
				addNode(glava.left, value);
			}
		} else if(value > glava.value) {
			if(glava.right == null) {
				//same story - empty so add
				glava.right = new TreeNode(value);
			} else {
				//Dig deeper
				addNode(glava.right, value);
			}
		}
		
		return glava;
	}
	
	/**
	 * Calculates the size of the <code>binary tree</code>. 
	 * Is a <code>recursive</code> function.
	 * 
	 * @param glava the <code>root</code> of our <code>binary tree</code>
	 * @return size of the <code>binary tree</code>
	 */
	public static int treeSize(TreeNode glava) {
		if(glava == null) {
			return 0;
		} else if(glava.left == null && glava.right == null) {
			return 1;
		}
		
		int temporarySum = treeSize(glava.left) + treeSize(glava.right) + 1;
		return temporarySum;
	}
	
	/**
	 * Checks if the <code>value</code> is already present in our
	 * <code>binary tree</code>.
	 * 
	 * @param glava the <code>root</code> of our <code>binary tree</code>
	 * @param value the value to be checked for presence
	 * @return true if <code>value</code> is present, false in the opposite case
	 */
	public static boolean containsValue(TreeNode glava, int value) {
		if(glava == null) {
			return false;
		}
		
		if(glava.value == value) {
			return true;
		} else if(value < glava.value) {
			return containsValue(glava.left, value);
		} else {
			//value is not equal or lower than glava.value, so it's gotta be greater than! Check if it is contained
			return containsValue(glava.right, value);
		}
	}
	
	/**
	 * <p>A static class that helps model a <strong>SINGLE <code>TREENODE</code></strong>
	 * of our <code>binary tree</code></p> 
	 * @author kikyy99
	 *
	 */
	public static class TreeNode {
		
		/**
		 * Left <code>child</code>. 
		 */
		TreeNode left;
		/**
		 * Right <code>child</code>. 
		 */
		TreeNode right;
		/**
		 * The value that's written into this <code>TreeNode</code>.
		 */
		int value;
		
		/**
		 * 1-argument <code>constructor</code> that immediately
		 * sets the <code>value</code>.
		 * 
		 * @param value the value to set for this <code>TreeNode</code>'s field '<code>value</code>'
		 */
		public TreeNode(int value) {
			this.value = value;
		}
	}
}

