package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * <p>A proper, full-fledged and functioning implementation of the class
 * {@code Collection}. The implementation itself is an array.</p>
 * 
 * <p>It is a dynamic array, which resizes itself as needed.</p>
 * 
 * @author kikyy99
 *
 */
public class ArrayIndexedCollection extends Collection {

	/**
	 * The default initial capacity of the array
	 */
	static final int INITIAL_CAPACITY = 16;
	
	/**
	 * The number of elements in the array.
	 * 
	 * Note; NOT the number of total free places/space(capacity), rather
	 * the number taken places in the array.
	 */
	private int size;
	/**
	 * The array that's used for storage itself. It
	 * can contain all java classes(it contains {@code Objects})
	 */
	private Object[] elements;
	/**
	 * The total free space inside the array
	 */
	private int capacity;
	
	/**
	 * Default constructor - initializes the array using the default initial capacity
	 */
	public ArrayIndexedCollection() {
		this(INITIAL_CAPACITY);
	}
	
	/**
	 * One-argument constructor. Initializes the array using
	 * a user-specified initial capacity.
	 * 
	 * @param initialCapacity the capacity used instead of the default
	 * initial capacity
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this.setCapacityConstructor(initialCapacity);
	}
	
	/**
	 * Copies the elements from another collection into this collection. Initializes
	 * the array using the initial capacity set to the other collection's size.
	 * 
	 * @param other the collection from which elements shall be copied
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other.size(), other);
	}
	
	/**
	 * <p>Copies the elements from another collection into this collection.<p>
	 * 
	 * <p>Initializes
	 * the array using the initial capacity as specified by the argument; unless
	 * the other collection's size is greater than this specified initial capacity.
	 * In that case, other collection's size is used instead for the initial capacity.</p>
	 * 
	 * @param initialCapacity the value which an attempt to set the initial capacity to
	 * shall be made; if the collection other is of greater size, its size shall be used instead
	 * of this parameter.
	 * @param other the collection which the elements should be copied from
	 * @throws NullPointerException if null is passed as an argument collection
	 */
	public ArrayIndexedCollection(int initialCapacity, Collection other) {
		Objects.requireNonNull(other);
		if(initialCapacity < other.size()) {
			initialCapacity = other.size();
		}
		//Now set that capacity and initialize the array
		this.setCapacityConstructor(initialCapacity);
		this.addAll(other);
		this.size = other.size();
	}
	
	//	##################### COLLECTION METHODS IMPLEMENTATION		##############################
		/**
		 * Returns the size of elements located in this collection
		 */
		@Override
		public int size() {
			return this.size;
		}
		
		/**
		 * Checks if this collection features an {@code Object} as
		 * specified by the parameter {@code value}.
		 */
		@Override
		public boolean contains(Object value) {
			if(value == null) {
				//it's a sequential array - IF there is any null value, it'll be at the last place of the array
				if(this.elements[this.capacity-1] == null) {
					return true;
				} else {
					return false;
				}
			}
			
			for(Object element : this.elements) {
				if(element == null) {
					return false;
				} else if(element.equals(value)) {
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Finds and removes the object from this array.
		 */
		@Override
		public boolean remove(Object value) {
			for(int i = 0; i < this.size(); i++) {
				if(this.elements[i].equals(value)) {
					this.elements[i] = null; 
					size--;
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Returns the underlying storage array.
		 */
		@Override
		public Object[] toArray() {
			Object[] newArray = new Object[this.size];
			for(int i = 0; i < this.size(); i++) {			//copy the elements from the old to the new Array
				newArray[i] = this.elements[i];
			}
			
			return newArray;
		}
		
		/**
		 * Performs processor's process() method on each
		 * element contained in this collection.
		 */
		@Override
		public void forEach(Processor processor) {
			for(int i = 0; i < this.size(); i++) {
				processor.process(this.elements[i]);
			}
		}
	//	##################### COLLECTION METHODS IMPLEMENTATION		##############################

	/**
	 * An inner mechanism method to be used only by this class. Checks if capacity is a proper value,
	 * and initializes the inner mechanism storage array of the size {@code capacity}
	 * 
	 * @param capacity the capacity of which an internal array shall be initialized
	 * @throws IllegalArgumentException if capacity is less than 1
	 */
	private void setCapacityConstructor(int capacity) {
		if(capacity < 1) {
			throw new IllegalArgumentException("Initial capacity cannot be set as less than 1!");
		}
		
		this.capacity = capacity;
		this.elements = new Object[this.capacity];
	}
	
	/**
	 * Adds an element at the first free(inner mechanism: null) space
	 * in this array
	 * 
	 * @throws NullPointerException if null is passed as an argument value
	 */
	@Override
	public void add(Object value){
		Objects.requireNonNull(value);
		this.updateCapacity();
		
		//Insert the new element, and update the size. Must find the first empty slot
		int i;
		for(i = 0; i < this.getCapacity(); i++) {
			if(this.elements[i]==null) {
				this.elements[i] = value;
				size++;
				return;
			}
		}
	}
	
	/**
	 * Inner-mechanism function: updates the capacity if we've run out of space to add new elements.
	 */
	private void updateCapacity() {
		//check if size is on the brink of capacity - literally
		if(this.size == this.capacity) {
			//not enough space, so double the size.
			this.capacity *= 2;
			Object[] newArray = new Object[this.capacity];
			for(int i = 0; i < this.size(); i++) {			//copy the elements from the old to the new Array
				newArray[i] = this.elements[i];
			}
			this.elements = newArray;
		}
	}
	
	/**
	 * Gets an element from this collection located at position index.
	 * 
	 * @throws IndexOutOfBoundsException if the passed index is out of bounds for this collection
	 * @param index the position which an element shall be retrieved from
	 * @return the element located at the position {@code index}
	 */
	public Object get(int index) {
		Objects.checkIndex(index, this.size());
		
		return this.elements[index];
	}
	
	/**
	 * Resets and cleares the contents of this collection
	 */
	@Override
	public void clear() {
		//just clear up to size since it's a sequential array
		for (int i = 0; i < this.size(); i++) {
			this.elements[i] = null;
		}
		this.size = 0;
	}

	/**
	 * <p>Inserts the value at the requested position, and if necessary shifts the array contents.</p>
	 * 
	 * <p>Works at an average complexity of O(n/2) due to the shifting</p>
	 * 
	 * @throws IndexOutOfBoundsException if the argument position is out of bounds for this collection
	 * @param value the value to be inserted
	 * @param position the index at which the value shall be inserted
	 */
	public void insert(Object value, int position) {
		Objects.checkIndex(position, this.size()+1);
		
		if(position == this.size() || this.get(position) == null) {
			//easy peasy insertion, no need to shift and then insert
			this.add(value);
			return;
		}
		
		//else, we have to shift objects. Oh yeah. First update capacity if necessary
		this.updateCapacity();
		for(int i = this.size(); i > position; i--) {
			this.elements[i] = this.elements[i-1];
		}
		this.elements[position]=null;
		this.add(value);
	}
	
	/**
	 * Attempts to find the value inside this collection. If found, returns its index.
	 * 
	 * @param value The value to be found
	 * @return if value if found in this collection returns its index, else returns -1
	 */
	public int indexOf(Object value) {
		if(value == null) {
			return -1;
		}
		
		for(int i = 0; i  < this.size(); i++) {
			if(this.elements[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * <p>Removes an element at the requested index, and shifts the elements if necessary.</p>
	 * 
	 * <p>Works at an average complexity O(n/2)</p>
	 * 
	 * @param index the index from which the element shall be removed
	 */
	public void remove(int index) {
		Objects.checkIndex(index, this.size());
		
		//if it's the last
		if(index == (this.size() - 1)) {
			this.elements[index] = null;
			this.size--;
			return;
		}
		
		//it's not the last - will have to shift elements to the left
		for(int i = index; i < this.size() - 1; i++) {
			this.elements[i] = this.elements[i+1];
		}
		//the last one is now on the place of the second last. Therefore, we must set the last one to null;
		this.elements[this.size()-1] = null;
		this.size--;
	}

	/**
	 * Default initial capacity getter method.
	 * 
	 * @return the default initial capacity of the internal storage array
	 */
	static int getInitialCapacity() {
		return INITIAL_CAPACITY;
	}

	/**
	 * Capacity getter method.
	 * 
	 * @return the actual capacity of the internal storage array
	 */
	int getCapacity() {
		return capacity;
	}
	
	
}
