/**
 * Second homework in the course: Introduction to Java in academic year 2018/2019.
 * This package features the stack demo java file, used for testing and demonstrating our Stack adaptor.
 * 
 * Professor: Marko Cupic
 * @author Kristijan Ceple
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections.demo;
