package hr.fer.zemris.java.hw02;

import java.util.Objects;

import hr.fer.zemris.java.custom.collections.DivisionByZeroException;

/**
 * A class modelling a simple {@code ComplexNumber}. The class
 * itself is immutable, and all the operations on this {@code ComplexNumber}
 * shall return a new instance of it.
 * 
 * @author kikyy99
 *
 */
public class ComplexNumber {
	
		/**
		 * Used for comparing two double values
		 */
		private static final double EPSILON = 1E-6;
		/**
		 * The real part of the complex number
		 */
		final private double real;
		/**
		 * The imaginary part of the complex number
		 */
		final private double imaginary;
		
		/**
		 * 2-argument constructor. Intstantiates a {@code ComplexNumber} with
		 * the specified real and imaginary parts.
		 * 
		 * @param real the real part of the complex number
		 * @param imaginary the imaginary part of the complex number
		 */
		public ComplexNumber(double real, double imaginary) {
			this.real = real;
			this.imaginary = imaginary;
		}
		
		//#####################		PUBLIC STATIC FACTORY METHODS		###############################
		/**
		 * Constructs and returns a new {@code ComplexNumber} with the imaginary part set to 0,
		 * and the real part set to the value as specified by the argument {@code real}.
		 * 
		 * @param real the real part of the complex number
		 * @return A new complex number in the form: real + 0 * i
		 */
		public static ComplexNumber fromReal(double real) {
			return new ComplexNumber(real, 0);
		}
		
		/**
		 * Constructs and returns a new {@code ComplexNumber} with the real part set to 0,
		 * and the imaginary part set to the value as specified by the argument {@code imaginary}.
		 * 
		 * @param imaginary the imaginary part of the complex number
		 * @return A new complex number in the form: 0 + iamginary * i
		 */
		public static ComplexNumber fromImaginary(double imaginary) {
			return new ComplexNumber(0, imaginary);
		}
		
		/**
		 * <p>Constructs and returns a new {@code ComplexNumber} as specified by
		 * arguments of this method - which actually define the polar form of the
		 * desired complex number.</p>
		 * 
		 * <p>This function returns the rectangular form</p>
		 * 
		 * @param magnitude the specified magnitude/radius of the complex number
		 * @param angle the specified angle of the complex number
		 * @return a new {@code ComplexNumber} as specified by the polar form
		 * provided by this function's arguments
		 */
		public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
			double real = magnitude * Math.cos(angle);
			double imaginary = magnitude * Math.sin(angle);
			
			return new ComplexNumber(real, imaginary);
		}
		
		/**
		 * Receives a string, and parses it into the form
		 * from which the data necessary for instantiating
		 * a {@code ComplexNumber} is gathered.
		 * 
		 * @throws IllegalArgumentException by methods extractImaginary and extractReal, see their javadoc
		 * @throws NumberFormatException if the argument s is not
		 * in the form of one of valid {@code ComplexNumber} forms
		 * @param s the String to be parsed
		 * @return the array of the real and the imaginary part
		 * of the complex number. This data is used furtherly in
		 * the class to instantiate a {@code ComplexNumber}
		 */
		private static double[] splitter(String s) {
			if(s.matches("\\d+\\.?\\d*\\+i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary(parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("\\+\\d+\\.?\\d*\\+i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[1]);
				double imaginary = extractImaginary(parts[2]);
				return new double[] {real, imaginary};
			} else if(s.matches("\\+?\\d+\\.?\\d*-i")) {
				String[] parts = s.split("-");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary("-" + parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("-\\d+\\.?\\d*\\+i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary(parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("-\\d+\\.?\\d*-i")) {
				String[] parts = s.split("-");
				double real = extractReal("-" + parts[1]);
				double imaginary = extractImaginary("-" + parts[2]);
				return new double[] {real, imaginary};
			} else if(s.matches("\\d+\\.?\\d*\\+\\d+\\.?\\d*i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary(parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("\\+\\d+\\.?\\d*\\+\\d+\\.?\\d*i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[1]);
				double imaginary = extractImaginary(parts[2]);
				return new double[] {real, imaginary};
			} else if(s.matches("\\+?\\d+\\.?\\d*-\\d+\\.?\\d*i")) {
				String[] parts = s.split("-");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary("-" + parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("-\\d+\\.?\\d*\\+\\d+\\.?\\d*i")) {
				String[] parts = s.split("\\+");
				double real = extractReal(parts[0]);
				double imaginary = extractImaginary(parts[1]);
				return new double[] {real, imaginary};
			} else if(s.matches("-\\d+\\.?\\d*-\\d+\\.?\\d*i")) {
				String[] parts = s.split("-");
				double real = extractReal("-" + parts[1]);
				double imaginary = extractImaginary("-" + parts[2]);
				return new double[] {real, imaginary};
			} else {
				throw new NumberFormatException("Invalid syntax. Cannot parse number.");
			}
		}
		
		/**
		 * Functions which does all the parsing of the string s that represents a
		 * complex number. It gathers data using the splitter(String s) function, and then
		 * from such data instanties a {@code ComplexNumber}.
		 * 
		 * @throws IllegalArgumentException by methods extractImaginary and extractReal, see their javadoc
		 * @throws NumberFormatException if the argument s is not
		 * in the form of one of valid {@code ComplexNumber} forms
		 * @param s the String to be parsed 
		 * @param s the string to be parsed
		 * @return an instance of a {@code ComplexNumber}
		 */
		public static ComplexNumber parse(String s) {
			//first split into real and imaginary part
			if(s.isEmpty()) {
				throw new NumberFormatException("Invalid syntax. No number passed.");
			}
			
			s = s.strip();
			s = s.replace(" ", "");
			
			boolean singlePart = false;
			if(s.matches("[\\+-]?\\d*i") || s.matches("[\\+-]?\\d+\\.?\\d*i?")) {
				singlePart = true;
			}
			//else our string is composed of a real and imaginary part. Still need to verify that
			if(singlePart) {
				if(s.contains("i")) {
					//imag
					double imaginary = extractImaginary(s);
					return fromImaginary(imaginary);
				} else {
					double real = extractReal(s);
					return fromReal(real);
				}
			} else {
				//split and then verify using splitter
				double[] parts = splitter(s);
				double real = parts[0];
				double imaginary = parts[1];
				return new ComplexNumber(real, imaginary);
			}
		}
		
		/**
		 * Extracts the imaginary part from the string by removing the letter "i". Expects to receive
		 * a string denoting ONLY the imaginary part of the complex number. However, this string features
		 * the letter "i" that needs to be removed.
		 * 
		 * @throws IllegalArgumentException if no imaginary part can be extracted
		 * @param toParse the string to be parsed for imaginary part extraction. In fact, the
		 * string is in the form of value * i, and this method just removes the "i" from the string.
		 * @return the imaginary part of the complex number. Practically removes the "i" from the
		 * argument {@code toParse}
		 */
		private static double extractImaginary(String toParse) {
			if(!toParse.contains("i")) {
				throw new IllegalArgumentException("Expected imaginary value, instead received real.");
			}
			
			//else let's extract the value without the "i"
			toParse = toParse.replace("i", "");
			if(toParse.matches("\\+?")) {
				return 1;
			} else if(toParse.matches("-?")) {
				return -1;
			}
			
			return Double.parseDouble(toParse);
		}
		
		/**
		 * Extracts the real part from the string {@code toParse}.
		 * 
		 * @throws IllegalArgumentException if the imaginary part was passed
		 * @param toParse the string to be parsed for real part extraction. In fact, this
		 * method just checks if the passed argument represents the real complex number component,
		 * and then delegates the work to the Double.parseDouble(String) method
		 * @return the real part of the complex number.
		 */
		private static double extractReal(String toParse) {
			if(toParse.contains("i")) {
				throw new IllegalArgumentException("Expected real value, instead received imaginary.");
			}
			
			//else let's extract the value without the "i"
			return Double.parseDouble(toParse);
		}
		//#####################		PUBLIC STATIC FACTORY METHODS		###############################
		/**
		 * Returns a string representation of this complex number. Examples include:
		 * "2.0+6.0i", "+3.172-0.445i", etc...
		 */
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if(real != 0) {
				sb.append(real);
			} else {
				//if both are zero, we return string "0"
				//if however real is zero, but not imaginary, then we return for example: 1i, 2i, -5i,...
				sb.append(imaginary != 0 ? (imaginary + "i") : "0");
				return sb.toString();
			}
			
			if(imaginary < 0) {
				sb.append(imaginary + "i");
			} else if(imaginary > 0) {
				sb.append("+" + imaginary + "i");
			}
			
			return sb.toString();
		}

		/**
		 * Real component getter
		 * 
		 * @return the real component of the complex number
		 */
		public double getReal() {
			return real;
		}
		/**
		 * Imaginary component getter
		 * @return the imaginary component of the complex number
		 */
		public double getImaginary() {
			return imaginary;
		}
		/**
		 * Polar form magnitude getter
		 * @return the magnitude of the polar form of this complex number
		 */
		public double getMagnitude() {
			double xSquared = this.real*this.real;
			double ySquared = this.imaginary*this.imaginary;
			
			return Math.sqrt(xSquared+ySquared);
		}
		/**
		 * Polar form angle getter
		 * @return the angle of the polar form of this complex number
		 */
		public double getAngle() {
			double angle = Math.atan2(imaginary, real);
			if(angle < 0) {
				angle += 2 * Math.PI;
			}
			
			return angle;
		}
		/**
		 * Performs the addition on two complex numbers
		 * 
		 * @param c The second operand
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber add(ComplexNumber c) {
			double newReal = this.real + c.real;
			double newImaginary = this.imaginary + c.imaginary;
			
			return new ComplexNumber(newReal, newImaginary);
		}
		
		/**
		 * Performs the subtraction on two complex numbers
		 * 
		 * @param c The second operand
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber sub(ComplexNumber c) {
			double newReal = this.real - c.real;
			double newImaginary = this.imaginary - c.imaginary;
			
			return new ComplexNumber(newReal, newImaginary);
		}
		
		/**
		 * Performs multiplication on two complex numbers
		 * 
		 * @param c The second operand
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber mul(ComplexNumber c) {
			double thisMagnitude = this.getMagnitude();
			double thisAngle = this.getAngle();
			
			double cMagnitude = c.getMagnitude();
			double cAngle = c.getAngle();
			
			double newMagnitude = thisMagnitude * cMagnitude;
			double newAngle = thisAngle + cAngle;
			
			//need to go to rectangular form
			return fromMagnitudeAndAngle(newMagnitude, newAngle);
		}
		
		/**
		 * Performs division on two complex numbers
		 * 
		 * @throws DivisionByZeroException if division by zero attempt is made
		 * @param c The second operand
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber div(ComplexNumber c) {
			if(c.real == 0 && c.imaginary == 0) {
				throw new DivisionByZeroException();
			}
			
			double newMagnitude = this.getMagnitude()/c.getMagnitude();
			double newAngle = this.getAngle()-c.getAngle();
			return fromMagnitudeAndAngle(newMagnitude, newAngle);
		}
		
		/**
		 * Using de Moivre's formula raises this {@code ComplexNumber} to the power of argument {@code n}
		 * 
		 * @throws IllegalArgumentException if argument {@code n} is less than 0
		 * @param n the power which the {@code ComplexNumber} shall be risen to
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber power(int n) {
			if(n < 0) {
				throw new IllegalArgumentException("n needs to be greater than or equal to 0");
			}
			
			double newMagnitude = Math.pow(this.getMagnitude(), n);
			double newAngle = this.getAngle() * n;
			return fromMagnitudeAndAngle(newMagnitude, newAngle);
		}
		
		/**
		 * Using de Moivre's formula raises this {@code ComplexNumber} to the power of argument 1/{@code n}.
		 * In other words, find the root of the degree n of this {@code ComplexNumber}.
		 * 
		 * @throws IllegalArgumentException if argument {@code n} is less than or equal to 0
		 * @param n the degree of the root
		 * @return the result in the form of a new {@code ComplexNumber} instance
		 */
		public ComplexNumber[] root(int n) {
			if(n <= 0) {
				throw new IllegalArgumentException("n needs to be greater than 0");
			}
			
			double newMagnitude = Math.pow(this.getMagnitude(), 1./n);
			double angle = this.getAngle();

			ComplexNumber[] complexNumbers = new ComplexNumber[n];
			for(int k = 0; k < n; k++) {
				double newAngle = (angle + 2*k*Math.PI)/n;
				complexNumbers[k] = fromMagnitudeAndAngle(newMagnitude, newAngle);
			}
		
			return complexNumbers;
		}
		
		/**
		 * Checks whether two ComplexNumbers are equal. They are equal only if both their real
		 * and imaginary parts are equal. The equivalence is ascertained using the 
		 * doublesEqual(double d1, double d2) method.
		 * 
		 * @throws NullPointerException if obj is null
		 * @throws IllegalArgumentException if obj isn't an instance of {@code ComplexNumber}
		 */
		@Override
		public boolean equals(Object obj) {
			Objects.requireNonNull(obj);
			if(!(obj instanceof ComplexNumber)) {
				throw new IllegalArgumentException("Argument passed isn't "
						+ "an instance of class ComplexNumber");
			}
			ComplexNumber other = (ComplexNumber) obj;
			
			if(doublesEqual(this.real, other.real) && doublesEqual(other.imaginary, other.imaginary)) {
				return true;
			} else {
				return false;
			}
		}
		
		@Override
		public int hashCode() {
			return Objects.hash(real, imaginary);
		}
		
		/**
		 * Checks whether two doubles are equal, by checking if the absolute value of
		 * their difference is less than the constant ComplexNumber.EPSILON.
		 * 
		 * 	
		 * @param d1 first double arg
		 * @param d2 second double arg
		 * @return whether two doubles represent the same number
		 */
		public static boolean doublesEqual(double d1, double d2) {
			return Math.abs(d1-d2) < EPSILON;
		}
}
