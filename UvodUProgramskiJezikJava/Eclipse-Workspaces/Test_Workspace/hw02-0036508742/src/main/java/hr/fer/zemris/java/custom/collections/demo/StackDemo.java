package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.DivisionByZeroException;
import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * A demonstration of our Stack adaptor.
 * 
 * @author kikyy99
 *
 */
public class StackDemo {
	/**
	 * The enumeration used for simpler postfix input processing from the user.
	 * @author kikyy99
	 *
	 */
	private enum Operator {
		/**
		 * Represents addition
		 */
		ADDITION('+'),
		/**
		 * Represents subtraction
		 */
		SUBTRACTION('-'),
		/**
		 * Represents multiplication
		 */
		MULTIPLICATION('*'),
		/**
		 * Represents division
		 */
		DIVISION('/'),
		/**
		 * Represents modulo operation
		 */
		MODULO('%');

		/**
		 * The operator itself held by the enum instance
		 */
		private char value;
		
		/**
		 * 1-argument contructor which insantiates the enum
		 * with the chosen operator
		 * 
		 * @param value the operator to be put into this enum instance
		 */
		private Operator(char value) {
			this.value = value;
		}
		
		/**
		 * <p>Valides the argument {@code value}, and if it represents
		 * a valid operator, returns back the enum wrapping that operator.</p>
		 * 
		 * <p>Valid operators are
		 * addition, subtraction, multiplication, division and modulo remainder.</p>
		 * 
		 * @throws IllegalArgumentException if the argument {@code value} is not one of the valid operators
		 * @param value the string to be verifies
		 * @return the operator that the string contained
		 */
		private static Operator valueToOperation(String value) {
			switch(value) {
			case "+":
				return ADDITION;
			case "-":
				return SUBTRACTION;
			case "*":
				return MULTIPLICATION;
			case "/":	
				return DIVISION;
			case "%":
				return MODULO;
			default:
				throw new IllegalArgumentException("Argument value is not a proper enum Operator value!");
			}
		}
	}
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("Application MUST receive 1 arg!");
			System.exit(-1);
		}
		
		String input = args[0];
		ObjectStack myStack = new ObjectStack();
		//let's split it first
		String[] parts = input.split(" ");
		try {
			for(String part : parts) {
				if(isInteger(part)) {
					myStack.push(Integer.valueOf(part));
				} else if(isOperator(part)) {
					int secondOperand = (int)myStack.pop();
					int firstOperand = (int)myStack.pop();
					int result = evaluateExpression(firstOperand, secondOperand, Operator.valueToOperation(part));
					myStack.push(result);
				} else {
					System.err.println("Invalid expression!");
					System.exit(-1);
				}
			}
		} catch(EmptyStackException ex) {
			System.err.println("Invalid expression!");
			System.exit(-1);
		} catch(DivisionByZeroException ex) {
			System.err.println("Division by zero not permissible!");
			System.exit(-2);
		} catch(IllegalArgumentException ex) {
			System.err.println(ex.getMessage());
			System.exit(-3);
		}
		
		if(myStack.size() == 1) {
			System.out.println("Result is: " + myStack.pop());
		} else {
			System.err.println("Error: stack size is greater than 1; Probably invalid expression!");
		}
	}
	
	/**
	 * Verifies whether the string is a valid operator - valid operators are
	 * addition, subtraction, multiplication, division and modulo remainder.
	 * 
	 * @param verify the string to be verified
	 * @return true if the string is a valid operator - false otherwise
	 */
	private static boolean isOperator(String verify) {
		boolean found = false;
		for(Operator operator : Operator.values()) {
			if(verify.equals(Character.toString(operator.value))) {
				found = true;
			}
		}
	
		return found;
	}
	
	/**
	 * Evaluates the expression by performing the operation. Takes the first and the second operand, and "puts"
	 * the operator between them and then proceeds to calculate the operation.
	 * 
	 * @throws DivisionByZeroException if a division by zero is attempted
	 * @throws IllegalArgumentException if the function doesn't support the passed operator
	 * @param firstOperand the first operand
	 * @param secondOperand the second operand
	 * @param operator the operator to apply between the first and the second operand
	 * @return the result of the operation
	 */
	private static int evaluateExpression(int firstOperand, int secondOperand, Operator operator) {
		int result;
		switch(operator) {
		case ADDITION:
			result = firstOperand + secondOperand;
			break;
		case SUBTRACTION:
			result = firstOperand - secondOperand;
			break;
		case MULTIPLICATION:
			result = firstOperand * secondOperand;
			break;
		case DIVISION:
			if(secondOperand == 0) {
				throw new DivisionByZeroException();
			}
			result = firstOperand / secondOperand;
			break;
		case MODULO:
			if(secondOperand == 0) {
				throw new DivisionByZeroException();
			}
			result = firstOperand % secondOperand;
			break;
		default:
			throw new IllegalArgumentException("Could not evaluate the operator. Internal program error.");
		}

		return result;
	}
	
	/**
	 * Verifies if the string represents an integer, WITHOUT the need for exceptions.
	 * 
	 * @param verify the string to be verified
	 * @return true if this string represents an integer, false otherwise
	 */
	private static boolean isInteger(String verify) {
		return verify.matches("[\\+-]?\\d+");
	}
}
