package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * Should be an abstract class - it helps us model a simple collection by providing a "bare" basis.
 * Other classes shall extend this basis and provide the functions and storage necessary for the proper
 * functioning of a full-fledged collection.
 *  
 * @author kikyy99
 *
 */
public class Collection {

	/**
	 * Empty meaningless constructor. Calls the Object/super constructor
	 */
	protected Collection() {}
	
	/**
	 * Checks whether this collection has got any elements
	 * spoiler: uses the size() function to check emptiness
	 * 
	 * @return true if empty - false otherwise
	 */
	public boolean isEmpty() {
		//TODO: utilize size
		int n = size();
		return n == 0 ? true : false;
	}
	
	/**
	 * Retrieves the size of this collection and returns it.
	 * 
	 * @return the size of this collection
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Adds an object into this collection.
	 * 
	 * @param value
	 */
	public void add(Object value) {}

	public boolean contains(Object value) {
		/* TODO: implement if contains according to EQUALS method. It's okay to ask
		whether col contains null */
		return false;
	}
	
	/**
	 * Removes the object from the collection.
	 * 
	 * @param value the value to find and remove from this collection
	 * @return whether an object was found and removed
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * <p>Allocated a NEW array, and copies all the elements from this collection
	 * into this new array, and then returns it.</p>
	 * 
	 * <p>In other words, the array isn't read-through.</p>
	 * 
	 * @return the array featuring all the elements of this collection
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Processor.process() method is called over each element of this
	 * collection.
	 * 
	 * @param processor the process whose process() method shall be executed over every element
	 */
	public void forEach(Processor processor) {}
	
	/**
	 * Copies all elements from another collection(the other collection itself is UNCHANGED) and
	 * adds them into this collection
	 * 
	 * spoiler: uses a processor in order to perform the addition
	 * 
	 * @param other
	 */
	public void addAll(final Collection other) {
		class addAllProcessor extends Processor {
			
			@Override
			public void process(Object value) {
				Objects.requireNonNull(value, "Argument passed is null!");
				//Validation done, now act on the object
				Collection.this.add(value);
			}
		}
		
		Processor addAllProcessor = new addAllProcessor();
		for(Object element : other.toArray()){
			addAllProcessor.process(element);
		}
	}
	
	/**
	 * Removes all the elements from this collection.
	 */
	public void clear() {}
}
