package hr.fer.zemris.java.hw02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.collections.DivisionByZeroException;

class ComplexNumberTest {

	private ComplexNumber c1 = new ComplexNumber(1, -1);
	private ComplexNumber c2 = ComplexNumber.parse("+1.56-4.44i");
	private ComplexNumber c3 = ComplexNumber.fromMagnitudeAndAngle(10.51, 1.7462);
	
	public static boolean doublesEqual(double d1, double d2) {
		return Math.abs(d1-d2) < 1E-6;
	}
	
	@Test
	void testComplexNumber() {
		ComplexNumber c4 = new ComplexNumber(-14.567, -22.141);
		assertTrue(doublesEqual(c4.getMagnitude(), 26.5032332));
		assertTrue(doublesEqual(c4.getAngle(), -2.152718865+2*Math.PI));
		assertTrue(doublesEqual(-22.141, c4.getImaginary()));
		assertTrue(doublesEqual(-14.567, c4.getReal()));
		
		c4 = new ComplexNumber(10, 10);
		assertTrue(doublesEqual(c4.getMagnitude(), 10*Math.sqrt(2)));
		assertTrue(doublesEqual(c4.getAngle(), Math.PI/4));
		assertTrue(doublesEqual(10, c4.getImaginary()));
		assertTrue(doublesEqual(10, c4.getReal()));
	}

	@Test
	void testFromReal() {
		ComplexNumber c4 = ComplexNumber.fromReal(-6.445);
		assertTrue(doublesEqual(c4.getReal(), -6.445));
		assertTrue(doublesEqual(c4.getImaginary(), 0));
	}

	@Test
	void testFromImaginary() {
		ComplexNumber c4 = ComplexNumber.fromImaginary(-6.445);
		assertTrue(doublesEqual(c4.getImaginary(), -6.445));
		assertTrue(doublesEqual(c4.getReal(), 0));
	}

	@Test
	void testFromMagnitudeAndAngle() {
		assertTrue(doublesEqual(c1.getMagnitude(), Math.sqrt(2)));
		assertTrue(doublesEqual(c1.getAngle(), -Math.PI/4+2*Math.PI));
		
		assertTrue(doublesEqual(c2.getMagnitude(), 4.706081172));
		assertTrue(doublesEqual(c2.getAngle(), -1.232918139+2*Math.PI));
		
		assertTrue(doublesEqual(c3.getMagnitude(), 10.51));
		assertTrue(doublesEqual(c3.getAngle(), 1.7462));
	}

	@Test
	void testParse() {
		assertTrue(doublesEqual(c2.getReal(), 1.56));
		assertTrue(doublesEqual(c2.getImaginary(), -4.44));
		
		ComplexNumber c5 = ComplexNumber.parse("-3.501 -   i");
		assertTrue(doublesEqual(c5.getReal(), -3.501));
		assertTrue(doublesEqual(c5.getImaginary(), -1));
	}

	@Test
	void testToString() {
		assertEquals("1.0-1.0i", c1.toString());
		assertEquals("1.56-4.44i", c2.toString());
	}

	@Test
	void testGetReal() {
		assertTrue(doublesEqual(c1.getReal(), 1));
		assertTrue(doublesEqual(c2.getReal(), 1.56));
	}

	@Test
	void testGetImaginary() {
		assertTrue(doublesEqual(c1.getImaginary(), -1));
		assertTrue(doublesEqual(c2.getImaginary(), -4.44));
	}

	@Test
	void testGetMagnitude() {
		assertTrue(doublesEqual(c3.getMagnitude(), 10.51));
	}

	@Test
	void testGetAngle() {
		assertTrue(doublesEqual(c3.getAngle(), 1.7462));
	}

	@Test
	void testAdd() {
		var add = c1.add(c2);
		assertTrue(doublesEqual(add.getReal(), 2.56));
		assertTrue(doublesEqual(add.getImaginary(), -5.44));
	}

	@Test
	void testSub() {
		var subt = c2.sub(c1);
		assertTrue(doublesEqual(subt.getReal(), 0.56));
		assertTrue(doublesEqual(subt.getImaginary(), -3.44));
	}

	@Test
	void testMul() {
		var mul = c1.mul(c3);
		assertTrue(doublesEqual(mul.getMagnitude(), 14.86338454));
		assertTrue(doublesEqual(mul.getAngle(), 0.9608018366));
	}

	@Test
	void testDiv() {
		assertThrows(DivisionByZeroException.class, ()-> c2.div(new ComplexNumber(0, 0)));
		
		var div = c1.div(c3);
		assertTrue(doublesEqual(div.getMagnitude(), c1.getMagnitude()/c3.getMagnitude()));
		assertTrue(doublesEqual(div.getAngle(), 3.751587144));
	}

	@Test
	void testPower() {
		assertThrows(IllegalArgumentException.class, ()-> c2.power(-1));
		
		var second = c3.power(2);
		var fifth = c1.power(5);
		
		assertTrue(doublesEqual(second.getMagnitude(), Math.pow(c3.getMagnitude(), 2)));
		assertTrue(doublesEqual(second.getAngle(), c3.getAngle()*2));
		
		assertTrue(doublesEqual(fifth.getMagnitude(), Math.pow(c1.getMagnitude(), 5)));
		assertTrue(doublesEqual(fifth.getAngle(), 3*Math.PI/4));
	}

	@Test
	void testRoot() {
		assertThrows(IllegalArgumentException.class, ()->c3.root(0));
		assertThrows(IllegalArgumentException.class, ()->c3.root(-1));
		
		var roots = c3.root(4);
		for(int i = 0; i < 4; i++) {
			assertTrue(doublesEqual(roots[i].getMagnitude(), 1.800531315));
			assertTrue(doublesEqual(roots[i].getAngle(), (1.7462+2*i*Math.PI)/4));
		}
	}
	
	@Test
	void testEquals() {
		var newC = new ComplexNumber(1, -1);
		var secondNew = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), -Math.PI/4);
		assertTrue(c1.equals(newC));
		assertTrue(c1.equals(secondNew));
		assertTrue(!c1.equals(c2));
		assertTrue(!c1.equals(c3));
		
		assertThrows(NullPointerException.class, ()->c1.equals(null));
		assertThrows(IllegalArgumentException.class, ()->c1.equals(23));
	}
	
	@Test
	void testHashCode() {
		var newC = new ComplexNumber(1, -1);
		var secondNew = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), -Math.PI/4);
		
		assertTrue(c1.hashCode() == newC.hashCode());
		/*
		 * We shouldn't actually hash double(float) values and this assertion shows it.
		 * It asserts to false because of the precision loss/bits being modified during
		 * consequent arithmetic operations on the numbers. As such, their hashcodes differ.
		 */
//		assertTrue(c1.hashCode() == secondNew.hashCode());
		assertTrue(c1.hashCode() != c2.hashCode());
		assertTrue(c1.hashCode() != c3.hashCode());
	}
	
	@Test
	void testDoublesEqual() {
		double d1 = 3.145;
		double d2 = 3.145;
		double d3 = 3.1451;
		
		assertTrue(doublesEqual(d1, d2));
		assertTrue(doublesEqual(d2, d1));
		assertTrue(!doublesEqual(d1, d3));
		assertTrue(!doublesEqual(d2, d3));
	}
}
