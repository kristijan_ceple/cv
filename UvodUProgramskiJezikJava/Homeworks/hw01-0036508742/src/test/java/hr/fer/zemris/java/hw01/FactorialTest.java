package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * Tests the factorial calculation function calculateFactorial(Integer number);
 * @author kikyy99
 *
 */
class FactorialTest {

	/**
	 * The function that does the testing itself. Asserts and checks for following arguments:
	 * <code>0, 1, 2, 3, 10, 20, -1, -15</code>.
	 */
	@Test
	void testCalculateFactorial() {
		long n = Factorial.calculateFactorial(0);
		assertEquals(1, n);
		
		n = Factorial.calculateFactorial(1);
		assertEquals(1, n);
		
		n = Factorial.calculateFactorial(2);
		assertEquals(2, n);
		
		n = Factorial.calculateFactorial(3);
		assertEquals(6, n);
		
		n = Factorial.calculateFactorial(10);
		assertEquals(3628800, n);
		
		n = Factorial.calculateFactorial(20);
		assertEquals(2432902008176640000L, n);
		
		assertThrows(IllegalArgumentException.class, () -> Factorial.calculateFactorial(-1));
		assertThrows(IllegalArgumentException.class, () -> Factorial.calculateFactorial(-15));
	}

}
