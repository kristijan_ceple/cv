package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LinkedListIndexedCollectionTest {

	private LinkedListIndexedCollection myList;
	private LinkedListIndexedCollection newList;
	
	@BeforeEach
	void setUp() throws Exception {
		myList = new LinkedListIndexedCollection();
		myList.add("NY");
		myList.add("London");
		myList.add("Budapest");
		myList.add("Warsaw");
		
		newList = new LinkedListIndexedCollection();
	}

	@Test
	void testSize() {
		assertEquals(4, myList.size());
		myList.add("Berlin");
		assertEquals(5, myList.size());
		myList.add("Washington");
		myList.add("San Francisco");
		assertEquals(7, myList.size());
		
		myList.remove("Washington");
		assertEquals(6, myList.size());
		myList.remove("San Francisco");
		myList.remove("London");
		assertEquals(4, myList.size());
		
		//New York, Budapest, Warsaw, Berlin
		assertArrayEquals(new Object[] {"NY", "Budapest", "Warsaw", "Berlin"},  myList.toArray());
		
		myList.remove(3);
		assertEquals(3, myList.size());
		assertArrayEquals(new Object[] {"NY", "Budapest", "Warsaw"},  myList.toArray());
		
		myList.remove(1);
		myList.remove(1);
		assertEquals(1, myList.size());
		assertArrayEquals(new Object[] {"NY"},  myList.toArray());
		
		assertEquals(0, newList.size());
	}

	@Test
	void testAdd() {
		assertArrayEquals(new Object[] {"NY", "London", "Budapest", "Warsaw"}, myList.toArray());
		myList.add("Berlin");
		myList.add("Washington");
		
		assertArrayEquals(new Object[] {"NY", "London", "Budapest", "Warsaw", "Berlin", "Washington"}, myList.toArray());
	}

	@Test
	void testContains() {
		assertFalse(myList.contains(null));
		assertFalse(myList.contains("Not present"));
		assertTrue(myList.contains("Budapest"));
		assertTrue(myList.contains("Warsaw"));
	}

	@Test
	void testRemoveObject() {
		assertEquals(4, myList.size());
		assertTrue(myList.remove("London"));
		assertFalse(myList.remove(null));
		assertFalse(myList.remove("Not present"));
		
		assertArrayEquals(new Object[] {"NY", "Budapest", "Warsaw"}, myList.toArray());
		assertEquals(3, myList.size());
	}

	@Test
	void testToArray() {
		assertArrayEquals(new Object[] {"NY", "London", "Budapest", "Warsaw"},  myList.toArray());
	}

	@Test
	void testForEach() {
		class PrintProcessor implements Processor {
			
			@Override
			public void process(Object value) {
				System.out.println(value);
			}
		} 
		PrintProcessor myPU = new PrintProcessor();
		
		myList.forEach(myPU);
		System.out.println("---------------------------");
		newList.forEach(myPU);
	}

	@Test
	void testClear() {
		myList.clear();
		assertEquals(0, myList.size());
		assertArrayEquals(new Object[] {}, myList.toArray());
		
		newList.clear();
		assertEquals(0, newList.size());
		assertArrayEquals(new Object[] {}, newList.toArray());
	}

	@Test
	void testLinkedListIndexedCollection() {
		var newerList = new LinkedListIndexedCollection();
		assertEquals(0, newerList.size());
		assertArrayEquals(new Object[] {}, newerList.toArray());
		
		newerList.add("A");
		newerList.add("B");
		newerList.add("C");
		newerList.add("D");
		newerList.add("A");
		newerList.add("D");
		
		assertEquals(6, newerList.size());
		assertArrayEquals(new Object[] {"A", "B", "C", "D", "A", "D"}, newerList.toArray());
		
		newerList.remove(2);
		assertEquals(5, newerList.size());
		assertArrayEquals(new Object[] {"A", "B", "D", "A", "D"}, newerList.toArray());
		
		newerList.remove("A");
		assertEquals(4, newerList.size());
		assertArrayEquals(new Object[] {"B", "D", "A", "D"}, newerList.toArray());
	}

	@Test
	void testLinkedListIndexedCollectionCollection() {
		var newerList = new LinkedListIndexedCollection(myList);
		
		assertEquals(4, newerList.size());
		assertArrayEquals(new Object[] {"NY", "London", "Budapest", "Warsaw"}, newerList.toArray());
		
		newerList = new LinkedListIndexedCollection(newList);
		assertEquals(0, newerList.size());
		assertArrayEquals(new Object[] {}, newerList.toArray());
	}

	@Test
	void testGet() {
		assertThrows(IndexOutOfBoundsException.class, () -> myList.get(4));
		assertThrows(IndexOutOfBoundsException.class, () -> myList.get(-1));
		assertDoesNotThrow(() -> myList.get(0));
		
		assertEquals("NY", myList.get(0));
		assertEquals("Warsaw", myList.get(3));
		assertEquals("Budapest", myList.get(2));
		assertEquals("London", myList.get(1));
	}

	@Test
	void testInsert() {
		// NY, London, Budapest, Warsaw
		myList.insert("Krakow", 1);
		assertEquals("Krakow", myList.get(1));
		//NY, Krakow, London, Budapest, Warsaw
		assertEquals(5, myList.size());
		assertArrayEquals(new Object[] {"NY", "Krakow", "London", "Budapest", "Warsaw"}, myList.toArray());
		
		myList.insert("Berlin", 0);
		assertEquals("Berlin", myList.get(0));
		
		myList.insert("San Francisco", 6);
		assertEquals("San Francisco", myList.get(6));
		
		assertEquals(7, myList.size());
		assertArrayEquals(new Object[] {"Berlin", "NY", "Krakow", "London", "Budapest", "Warsaw", "San Francisco"}, myList.toArray());
		
		myList.insert("Santa Barbara", 6);
		assertEquals("Santa Barbara", myList.get(6));
		//Berlin, NY, Krakow, London, Budapest, Warsaw, Santa Barbara, San Francisco
		myList.insert("Stockholm", 6);
		assertEquals("Stockholm", myList.get(6));
		assertEquals("Santa Barbara", myList.get(7));
		assertEquals("San Francisco", myList.get(8));

		//TODO: Comment later
//		for(Object element : myList.toArray()) {
//			System.out.print(element + " ");
//		}
//		System.out.println();
		
	}

	@Test
	void testIndexOf() {
		//NY, London, Budapest, Warsaw
		assertEquals(0, myList.indexOf("NY"));
		assertEquals(1, myList.indexOf("London"));
		assertEquals(2, myList.indexOf("Budapest"));
		assertEquals(3, myList.indexOf("Warsaw"));
		assertEquals(-1, myList.indexOf("Unknown"));
		assertEquals(-1, myList.indexOf(null));
	}

	@Test
	void testRemoveInt() {
	 myList.remove(1);
	 myList.remove(1);
	 assertArrayEquals(new Object[] {"NY", "Warsaw"}, myList.toArray()); 
	 assertThrows(IndexOutOfBoundsException.class, ()->myList.remove(-1));
	 assertThrows(IndexOutOfBoundsException.class, ()->myList.remove(2));
	 assertThrows(IndexOutOfBoundsException.class, ()->myList.remove(3));
	}

	@Test
	void testIsEmpty() {
		assertFalse(myList.isEmpty());
		assertTrue(newList.isEmpty());
		int initialNumberOfElements = myList.size();
		for(int i = 0; i < initialNumberOfElements; i++) {
			myList.remove(0);
		}
		assertTrue(myList.isEmpty());
	}

	@Test
	void testAddAll() {
		newList.addAll(myList);
		assertEquals(4, myList.size());
		assertEquals(4, newList.size());
		assertArrayEquals(new Object[] {"NY",  "London", "Budapest", "Warsaw"}, newList.toArray());
	}

}
