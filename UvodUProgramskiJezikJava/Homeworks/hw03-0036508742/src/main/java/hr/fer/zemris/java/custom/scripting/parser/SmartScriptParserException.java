package hr.fer.zemris.java.custom.scripting.parser;

public class SmartScriptParserException extends RuntimeException {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	
	public SmartScriptParserException() {}
	
	SmartScriptParserException(String message) {
		super(message);
	}
}
