package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * A class representing a {@code Token}
 * 
 * @author kikyy99
 *
 */
public class Token {
	
	private TokenType tokenType;
	private Object value;
	
	public Token(TokenType tokenType, Object value) {
		if(tokenType == null) {
			throw new IllegalArgumentException("Token type can not be null!");
		}
		
		this.tokenType = tokenType;
		this.value = value;
	}
	
	public Object getValue() {
		return value;
	}

	public TokenType getType() {
		return tokenType;
	}
}
