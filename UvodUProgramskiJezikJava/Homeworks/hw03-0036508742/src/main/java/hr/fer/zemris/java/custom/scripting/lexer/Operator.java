package hr.fer.zemris.java.custom.scripting.lexer;

public enum Operator {
	PLUS("+"),
	MINUS("-"),
	MUL("*"),
	DIV("/"),
	POW("^");
	
	private String value;
	
	private Operator(String value) {
		this.value = value;
	}
	
	public String toString() {
		return this.value;
	}
}
