package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration of token types
 * @author kikyy99
 *
 */
public enum TokenType {
	EOF,
	TAG_NAME,
	VARIABLE,
	CONSTANT_INTEGER,
	CONSTANT_DOUBLE,
	STRING,
	FUNCTION,
	OPERATOR;
}
