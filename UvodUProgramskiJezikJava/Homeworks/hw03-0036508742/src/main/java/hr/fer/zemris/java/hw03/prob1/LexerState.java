package hr.fer.zemris.java.hw03.prob1;

/**
 * Enumeration representing possible states of a {@link Lexer}.
 * 
 * @author kikyy99
 *
 */
public enum LexerState {
	/**
	 * <p>In this mode the {@link Lexer} parses the input according to the following
	 * rules: {@link Token} objects are constructed and they can hold words, numbers,
	 * and symbols. Whitespaces are used to separate these tokens.</p>
	 * 
	 *  <p>Backslash is used as an escape character to escape digits or other backslashes 
	 *  in words. If digits are not escaped, they are stored as a special {@link Token}.</p>
	 *  
	 *  <p><pre>The possible {@link Token} objects are:
	 *  		{@link TokenType#WORD} -> represents a word
	 *  		{@link TokenType#NUMBER} -> represents a number
	 *  		{@link TokenType#SYMBOL} -> represents any SINGLE symbol(e.g. #, $, %,...)
	 *  		{@link TokenType#EOF} -> generated at the end of input - represents
	 *  									the last token. Similar to the oft-used
	 *  									'\0' in string escaping.
	 *  </pre></p>
	 *  
	 *  <p>Upon encountering {@link Lexer#SPECIAL_SIGN}, {@link Lexer} shall leave this set,
	 *  and instead be set to the {@link LexerState#EXTENDED} state. In that state, other rules apply,
	 *  and upon the second encounter of a {@link Lexer#SPECIAL_SIGN} in that state, the
	 *  {@link Lexer} shall leave that state and instead again enter the {@link LexerState#BASIC} state.
	 */
	BASIC,
	/**
	 * <p>Upon entering the {@link Lexer#SPECIAL_SIGN} in the input to the {@link Lexer},
	 * the {@link Lexer} shall enter the {@link LexerState#EXTENDED} state.</p>
	 * 
	 * <p>In this state the {@link Lexer} parses the input into words 
	 * separated by whitespaces. In this state, backslashes are not parsed as escape
	 * characters, but are instead taken at face value and put directly into
	 * tokenised words.</p>
	 * 
	 * <p>Upon again encountering the {@link Lexer#SPECIAL_SIGN} this {@link Lexer} shall
	 * leave the {@link LexerState#EXTENDED} state, and once again enter the {@link LexerState#BASIC} state.
	 */
	EXTENDED;
}
