package hr.fer.zemris.java.custom.scripting.elems;

public class ElementOperator extends Element {

	private String symbol;
	
	@Override
	public String asText() {
		return symbol;
	}
}
