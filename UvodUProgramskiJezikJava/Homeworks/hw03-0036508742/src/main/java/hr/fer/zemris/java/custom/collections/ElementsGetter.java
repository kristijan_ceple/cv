package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * A custom-made interface resembling an {@link Iterator} 
 * 
 * @author kikyy99
 *
 */
public interface ElementsGetter {
	default void processRemaining(Processor p) {
		while(this.hasNextElement()) {
			p.process(this.getNextElement());
		}
	}
	/**
	 * Checks whether this {@link ElementsGetter} features any unreturned elements
	 * 
	 * @throws ConcurrentModificationException if the overlaying {@link Collection} has been modified
	 * during the lifetime of this {@link ElementsGetter}
	 * @return true if this {@link ElementsGetter} has got any elements to return; false otherwise
	 */
	boolean hasNextElement();
	/**
	 * Returns the next element from this {@link ElementsGetter} if it has any elements left. Otherwise throws an {@link NoSuchElementException}
	 * 
	 * @throws ConcurrentModificationException if the overlaying {@link Collection} has been modified
	 * during the lifetime of this {@link ElementsGetter}
	 * @return the next element from this {@link ElementsGetter}
	 */
	Object getNextElement();
}
