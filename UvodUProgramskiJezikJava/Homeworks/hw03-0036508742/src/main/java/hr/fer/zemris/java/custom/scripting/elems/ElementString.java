package hr.fer.zemris.java.custom.scripting.elems;

public class ElementString extends Element{

	private String value;
	
	@Override
	public String asText() {
		return value;
	}
	
}
