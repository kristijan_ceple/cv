package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.hw03.prob1.Token;
import hr.fer.zemris.java.hw03.prob1.TokenType;

public class Lexer {
	public static final char STRING_ENCLOSE_SIGN = '"';
	public static final char TAG_OPENS = '{';
	public static final char TAG_CLOSES = '}';
	public static final char ESCAPE_CHAR = '\\';
	public static final int[] FOR_NO_OF_ARGS = {2, 3};
	/**
	 * Specifies in which state is the {@link Lexer} located. 
	 */
	private LexerState state = LexerState.BASIC;
	/**
	 * Entry text/characters
	 */
	private char[] data;      // ulazni tekst
	/**
	 * The current token
	 */
	private Token token;      // trenutni token
	/**
	 * Index of the first non-processed character
	 */
	private int currentIndex; // indeks prvog neobrađenog znaka

	public Lexer(String text) {
		if(text == null) {
			throw new NullPointerException("Null passed as input to the Lexer.");
		}
		
		this.data = text.toCharArray();
	}
	
	public Token nextToken() {
		for(int i = currentIndex; i < data.length; i++) {
			if(state == LexerState.BASIC) {
				//we're in basic state - we encounter only strings and tags
				if(data[i]==TAG_OPENS) {
					setState(LexerState.TAG);
					currentIndex = ++i;
					continue;		//go on to process other tags
				} else if(data[i]==ESCAPE_CHAR) {
					//need to check for i+1
					if(i+1 < data.length) {
						//check what character is it escaping
						if(data[i+1]==ESCAPE_CHAR || data[i+1]==TAG_OPENS) {
							//valid characters - start parsing from that spot
							currentIndex = parseString(++i);
							token = tokenizeString(i, currentIndex);
							return token;
						} else{
							throw new LexerException("Trying to escape an unescapable character...");
						}
					} else {
						throw new LexerException("Lone \\ encountered!");
					}
				} else {
					/*
					 * Neither a tag opening nor an escape - that means
					 * we're dealing with normal text that shall be tokenized
					 */
					currentIndex = parseString(++i);
					token = tokenizeString(i, currentIndex);
					return token;
				}
			} else {
				//we're in the TAG state - we encounter vars, func,...
				if(Character.isWhitespace(data[i])) {
					i++;			//don't move on to the next tag - 
					continue;		//rather IGNORE the whitespace
				} else if(data[i]=='$') {
					//name of the tag
					currentIndex = parseTagName(++i);
					token = tokenizeTagName(i, currentIndex);
					return token;
				}
			}
		}
		
		//generate an EOF token
		if(token == null || token.getType() != TokenType.EOF) {
			//Input has been successfully parsed or is empty --> GENERATE EOF!!!
			currentIndex = data.length;
			return tokenizeEOF();
		} else {
			//WRONG! end of input already reached
			throw new LexerException("Input already processed - its end has already been reached, and/or an EOF generated!");
		}
	}
	
	private int parseString(int startIndex) {
		//depending on the state we're in - parse Strings differently
		int i;
		for(i = startIndex; i < data.length; i++) {
			if(state == LexerState.BASIC) {
				//read everything as is, just take care of escapes
				if(data[i]==ESCAPE_CHAR) {
					//check if next char exists
					if(i+1 < data.length) {
						if(data[i+1]==ESCAPE_CHAR || data[i+1]==TAG_OPENS) {
							//allowed signs
							i+=2;
							continue;
						} else {
							throw new LexerException("Escape attempt on an unescapable character: " + data[i+1] + "!");
						}
					} else {
						throw new LexerException("Lone \\ encountered!");
					}
				} else {
					//all other characters permitted
					i++;
					continue;
				}
			} else {
				/*
				 * We're in TAG state - different rules.
				 * Go from " to the next ". The first " has already been encountered
				 */
				if(data[i]==STRING_ENCLOSE_SIGN) {
					//read everything till the next enclosing sign
				} else {
					throw new LexerException("Trying to parse as String something "
							+ "that is not a String!");
				}
			}
		}
		
		return i;
	}
	
	/**
	 * Creates and returns an {@link TokenType#EOF} {@link Token}
	 * 
	 * @return an {@link TokenType#EOF} {@link Token}
	 */
	private Token tokenizeEOF() {
		token = new Token(TokenType.EOF, null);
		return token;
	}
	
	private boolean validateVarName() {
		return false;
	}

	/**
	 * Sets the state of this {@link Lexer} to the passed argument 
	 * {@code state}
	 * 
	 * @param state the {@code state} which this {@link Lexer} shall be set to
	 */
	public void setState(LexerState state) {
		Objects.requireNonNull(state);
		
		this.state = state;
	}
}
