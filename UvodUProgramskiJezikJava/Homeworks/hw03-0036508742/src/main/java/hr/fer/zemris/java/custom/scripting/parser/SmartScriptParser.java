package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.scripting.proto.lexer.Lexer;

public class SmartScriptParser {

		String input;
		Lexer lexer;
	
		public SmartScriptParser(String input) {
			if(input == null) {
				throw new SmartScriptParserException("Error: NULL input passed to the parser!");
			}
			
			this.input = input;
			this.lexer = new Lexer(input);
			parse();
		}
		
		private void parse() throws SmartScriptParserException{
			try {
				//TODO: implement parsing on a Stack here later
			} catch(Exception e) {
				throw new SmartScriptParserException("Exception thrown during parsing: " + e.getMessage());
			}
		}
}
