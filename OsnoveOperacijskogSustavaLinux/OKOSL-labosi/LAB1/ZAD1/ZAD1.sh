#!/usr/bin/env bash
#PrviZadatak

cd /home/kikyy99/Documents/OKOSL-labosi/LAB1/ZAD1
pwd

echo "Tu cemo raditi zadatke! ls:"
ls
mkdir LAB1
cd LAB1
mkdir source
touch source/empty
echo
echo "Touch empty"

cp -rb /boot /etc ./source 2> /dev/null
echo "Kopirao boot i etc u source dir. ls:"
ls
echo "./source ls:"
ls ./source
echo "Size sourcea"
du -s --si source

ln -s source target
cd target
echo
echo "cd target -bez dereferenciranja"
pwd

cd ..
cd -P target
echo "cd target -sa dereferenciranjem"
pwd
echo


echo "Size followanog symlinka"
cd ..
du -Lhs target 2> /dev/null

cd source
echo "Size sourcea"
pwd
du -Lhs . 2> /dev/null
echo


echo "Stvorio novi sa istim modify timeom kao empty"
touch -r empty novi

echo
stat empty novi
echo


cd ../..
pwd
echo "Sad ide brisanje pred kraj zadatka"

#mozda bolje I u rm za interaktivan safe mode? ne malo i nego I!!!
rm -Rf LAB1
cd ..
#rmdir LAB1

