#!/usr/bin/env bash
echo -n "Upisi godinu za koju te zanima
koliko puta se dogodio petak 13: "
read godina;

echo -n "Upisi godinu do koje te zanima koliko puta se dogodio petak 13: "
read doKada;

petkovi=$(ncal $godina | grep "pe" | tr ' ' '\n' | grep "13" | wc -l)
echo "U godini $godina, petak 13. se dogodio $petkovi puta."

//Sada treba to napraviti za sve godine do doKada
for godina in $(seq $godina $doKada);
do
    petkovi=$(ncal $godina | grep "pe" | tr ' ' '\n' | grep "13" | wc -l)
    echo "U godini $godina, petak 13. se dogodio $petkovi puta."
done

