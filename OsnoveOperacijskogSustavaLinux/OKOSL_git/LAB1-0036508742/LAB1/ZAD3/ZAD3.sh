#!/usr/bin/env bash
echo -n "Upisi godinu za koju te zanima
koliko puta se dogodio petak 13: "
read godina;
petkovi=$(ncal 2017 | grep "pe" | tr ' ' '\n' | grep "13" | wc -l)
echo "U godini $godina, petak 13. se dogodio $petkovi puta."