#!/bin/bash

#Zadatak1
cd
cat .bash_logout

#Zadatak2
#cd
ls -Sralh

#Zadatak 3.a
#cd
array=(ponedjeljak utorak srijeda cetvrtak petak .subota)
mkdir -p /tmp/OKOSL\ tjedan/${array[*]}
for item in ${array[*]}
do 
mkdir -p /tmp/OKOSL\ tjedan/$item;
done

#Zadatak 3.b
pwd

#Zadatak 3.c
array=(predavanja labosi zadaca{1..8})
for item in ${array[*]}
do 
mkdir -p /tmp/OKOSL\ tjedan/.subota/$item
done

#Zadatak 3.d
cp -r /tmp/OKOSL\ tjedan/.subota /tmp/OKOSL\ tjedan/ponedjeljak/

#Zadatak 3.e
ls -Ra /tmp/OKOSL\ tjedan/
sudo apt install tree
tree -a /tmp/OKOSL\ tjedan/

#Zadatak 4.a
#cd
ln -s /var Varionica

#Zadatak 4.b
sudo du --block-size=GB -scL Varionica

#Zadatak 4.c
rm -i Varionica

#Zadatak 5
df --output=avail --block-size=GB --total --sync -a

#Zadatak 6
file /bin/bash
file /etc/passwd
file /boot