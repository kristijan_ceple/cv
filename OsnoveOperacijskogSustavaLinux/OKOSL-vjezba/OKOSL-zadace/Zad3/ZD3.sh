#!/usr/bin/env bash

#Zadatak 1a
ls -l /usr/share/gtk*

#Zadatak 1b
ls -d /usr/share/*[0-9]*[0-9]*

#Zadatak 2a
egrep [[:digit:]]+ /usr/share/dict/words

#Zadatak 2b
egrep ^i.*[[:upper:]].* /usr/share/dict/words

#Zadatak 2c
/usr/share/dict/words

#Zadatak 2d
egrep -v [aeiou] /usr/share/dict/words

#Zadatak 2e
egrep .*[aeiou]*[aeiou]*.* /usr/share/dict/words

#Zadatak 2f
egrep ening$ /usr/share/dict/words | wc -l

#Zadatak 2g
egrep -c "'s"$ /usr/share/dict/words

#Zadatak 2i
egrep -c [[:upper:]]$ /usr/share/dict/words

#Zadatak 3a
sed -r "s/('s)$/s/" /usr/share/dict/words

#Zadatak 3b
egrep word /usr/share/dict/words | sed -r "s/word//" | sed -r "s/([[:alnum:]]+)/word\1/"

#Zadatak 3c
sed -r "s/([A-Z]).*(.)/\L\1\U\2/" /usr/share/dict/words