#include <stdio.h>
#define KONSTANTA_DIJELJENJA 10

double ZbrojiZnamenke(int broj){
    double suma = 0;

    while(broj != 0){
        int znamenka = broj%KONSTANTA_DIJELJENJA;
        suma += znamenka;

        broj /= KONSTANTA_DIJELJENJA;
    }
    return suma;
}

int main(void){
    double suma = 0;
    
    while(1){
        int broj;
        scanf("%d", &broj);
        
        printf("Broj ucitan = %d\n", broj);

        if(broj == 0)
            break;
        else if(broj % 3 == 0){
            suma += ZbrojiZnamenke(broj);
        }
    }

    printf("Suma = %f\n", suma);
}