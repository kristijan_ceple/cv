package hr.fer.os.lab4;

public class Process extends Block {
	
	public Process(int addr, int length, int id) {
		super(addr, length, id);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof Process))
			throw new IllegalArgumentException();
	
		Process other = (Process) obj;
		return (this.getId() == other.getId());
	}
}
