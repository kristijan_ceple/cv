package hr.fer.os.lab4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

public class Memory {
	
	public static final boolean PROCESS = true;
	public static final boolean HOLE = false;
	
	private Word[] words;
	private NavigableMap<Integer, Process> processes = new TreeMap<>();
	private NavigableMap<Integer, Hole> holes = new TreeMap<>();
	
	private int processLastId = 1;
	private int holeLastId = 0;
	//Ckay
	public Memory(int size) {
		words = new Word[size];
		
		Hole whole = new Hole(0, words.length, holeLastId);
		holes.put(whole.getId(), whole);
		
		for(int i = 0; i < words.length; i++)
			words[i] = new Word(holeLastId, HOLE);
		
		holeLastId++;
		printMemory();
	}

	private void writeThrough(Block block) {
		int addr = block.getAddr();
		int length = block.getLength();
		int endAddr = addr+length-1;
		if(endAddr > words.length)
			throw new IndexOutOfBoundsException();
		
		int id = block.getId();
		boolean process = block instanceof Process;
		
		for(int i = addr; i <= endAddr; i++) {
			words[i].setId(id);
			words[i].setProcess(process);
		}
	}
	
	public void printDataStructs() {
		System.out.println();
		
		System.out.println("Holes:");
		System.out.println("[  Id\tAddr\tlen]");
		for(Hole hole : holes.values())
			System.out.printf("%4d\t%4d\t%4d%n", hole.getId()
					, hole.getAddr(), hole.getLength());
		
		System.out.println("Processes:");
		System.out.println("[  Id\tAddr\t len\t]");
		for(Process process : processes.values())
			System.out.printf("%4d\t%4d\t%4d%n", process.getId()
					, process.getAddr(), process.getLength());
		
		System.out.println();
	}
	
	public void printMemory() {
		System.out.println();
		
		System.out.print(" ");
		for(int i = 1; i <= words.length; i++) {
			System.out.printf("%d", i%10);
		}
		System.out.println();
		
		System.out.print("[");
		for(Word word : words)
			System.out.print(word.toString());
		System.out.println("]");
		
		System.out.println();
	}
	
	private void addProcessGC(int length, int id) {
		processLastId = id;
		addProcess(length);
	}
	
	public void addProcess(int length) {
		if(length <= 0)
			throw new IllegalArgumentException();
		
		Hole minHole = null;
		int delta = 0;
		
		Collection<Hole> holesCol = holes.values();
		List<Hole> holesSortedList = new ArrayList<>(holesCol);
		Collections.sort(holesSortedList);
		
		for(Hole hole : holesSortedList) {
			int holeLength = hole.getLength();
			if(length <= holeLength) {
				minHole = hole;
				delta = holeLength-length;
				break;
			}
		}
		
		if(minHole == null) {
			System.out.println("Couldn't find a hole small enough!");
			garbageCollection();
			addProcess(length);
			return;
		}
		
		int addr = minHole.getAddr();
		Process toAdd = new Process(addr, length, processLastId++);
		processes.put(toAdd.getId(), toAdd);
		writeThrough(toAdd);
		
		if(delta != 0) {
			minHole.setAddr(addr+length);
			minHole.setLength(delta);
			writeThrough(minHole);
		} else {
			holes.remove(minHole.getId());
		}
	}
	
	public void removeProcess(int id) throws NullPointerException{
		/* First we have to get the Process, and then find
		 * delete it, and make a hole in its place. Afterwards
		 * follows merging first the left, and then the right hole
		 * into our middle hole
		 */
		Process toRemove = processes.get(id);
		
		// On to removing the process, and substituting a hole in its stead
		Hole substitution = new Hole(toRemove);
		substitution.setId(holeLastId);
		processes.remove(toRemove.getId());
		holes.put(holeLastId++, substitution);
		
		//Let's check for merging
		//First on, LEFT MERGE
		int leftAddr = substitution.getAddr()-1;
		if(leftAddr >= 0 && !words[leftAddr].isProcess()) {
			int holeId = words[leftAddr].getId();
			Hole leftHole = holes.get(holeId);
				
			int newAddr = leftHole.getAddr();
			int lengthDelta = leftHole.getLength();
			
			holes.remove(leftHole.getId());
			int newLength = lengthDelta + substitution.getLength();
			substitution.setAddr(newAddr);
			substitution.setLength(newLength);
		}
		
		//Check and merge the right side if necessary
		int rightAddr = substitution.getAddr() + substitution.getLength();
		if(rightAddr < words.length && !words[rightAddr].isProcess()) {
			int holeId = words[rightAddr].getId();
			Hole rightHole = holes.get(holeId);
			
			int lengthDelta = rightHole.getLength();
			
			holes.remove(rightHole.getId());
			int newLength = lengthDelta + substitution.getLength();
			substitution.setLength(newLength);
		}
		
		writeThrough(substitution);
	}
	
	public void garbageCollection() {
		/* Now I need to first remove all the holes,
		 * and then put all the processess into a separate
		 * container - clear the first, and slowly re-add
		 * them. At the end one large hole will remain!
		 */
		
		//First back the processes up!
		NavigableMap<Integer, Process> tempProcesses = new TreeMap<>(processes);
		Collection<Process> processCol = tempProcesses.values();
		int[] id = new int[processCol.size()];
		int i = 0;
		
		//Now remove each one - this shall also create one large hole
		for(Process process : processCol) {
			if(process != null) {
				id[i++]=process.getId();
				this.removeProcess(process.getId());
			}
		}
		
		//set hole and process indexes
		
		
		//And re-add them!
		i = 0;
		Collection<Process> tempProcessCol = tempProcesses.values();
		for(Process process : tempProcessCol)
			this.addProcessGC(process.getLength(), id[i++]);
	}
	
	public void print() {
		this.printDataStructs();
		this.printMemory();
	}
	
	//	####################	INNER HELP FUNCTION		##############################
	public class Word{
		
		private int id;
		private boolean process;
		
		public Word(int id, boolean process) {
			this.id = id;
			this.process = process;
		}
		
		@Override
		public String toString() {
			return process ? Integer.toString(id) : "-";
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public boolean isProcess() {
			return process;
		}

		public void setProcess(boolean process) {
			this.process = process;
		}
	}
}
