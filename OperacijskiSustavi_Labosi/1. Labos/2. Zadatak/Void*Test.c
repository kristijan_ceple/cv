#include <stdio.h>

int main(){
    char c = 'c';
    void* ptr = &c;

    printf("( (char*)ptr )c = %c\n", (char*)ptr);
    
    printf(" ( *(char*)ptr ) c = %c\n", *((char*)ptr) );
    //printf(" ((char*)ptr*) c = %c\n", (char*)ptr*);
}