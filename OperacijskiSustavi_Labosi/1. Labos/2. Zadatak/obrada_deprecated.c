/**
 * Obrada_deprecated ima mogucnost pamcenja samo 1 prekida at a time,
 * za razliku od obrada, koja moze pamtiti vise prekida
*/

#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define PROGRAM_TIME 30
#define OBRADA_TIME 5
#define N 6
#define PERIOD 1        //Ovo pustiti da bude 1 sekunda

int K_Z[N] = {0};
int KON[N];
int T_P = 0;
int sig[] = {SIGALRM, SIGTERM, SIGTSTP, SIGABRT, SIGINT};


void interrupt_disable()
{
    for(int i = 0; i < N-1; i++){
        sighold(sig[i]);
    }
}

void interrupt_enable()
{
    for(int i = 0; i < N-1; i++){
        sigrelse(sig[i]);
    }
}

//Fja okej
void ispis_polja(int* polje){
    printf("[ ");
    for(int i = 0; i < N; i++){
        printf("%d ", polje[i]);
    }
    printf("]");
}

void ispis_redak(int i, void* element, char String[] ){
    /*  Kao i dolje
        i = razina prekida
        k = indeks u retku(povezan sa i)
    */

   interrupt_disable();

   if( strcmp(String, "char" ) == 0 ){
        
        for(int k = 0; k < N; k++){
            if(k == i){
                printf(" %c ", *( (char *)element ) );
            } else{
                printf(" - ");
            }
        }

   } else if( strcmp(String, "int" ) == 0 ){

        for(int k = 0; k < N; k++){
            if(k == i){
                printf("%2d ", *( (int *)element ) );
            } else{
                printf(" - ");
            }
        }

    } else{

       printf("ERROR pri ispis_redak: KRIVI STRING ARGUMENT!!!");
       exit(-2);

   }

        printf("\tK_Z");
        ispis_polja(K_Z);
        printf("\tT_P=%d", T_P);
        printf("\tKON");
        ispis_polja(KON);
        printf("\n");

    interrupt_enable();
}

void obrada_signala(int i){
    //TO DO nesto o ispisu svake sec???
    /*  i = razina prekida
        j = vrijeme
        k = indeks u retku(povezan sa i)
    */

    //prije pocetka brojenja posaljimo da se ispise P, za pocetak obrade prekida
    char pomocno = 'P';
    ispis_redak( i , &pomocno, "char" );

    for(int j = 1; j <= OBRADA_TIME; j++){

        ispis_redak( i , &j , "int" );
        int x = PERIOD;
        while( ( x = sleep(x) ) );

    }

    //ispis K, koji oznacava kraj obrade prekida
    pomocno = 'K';
    ispis_redak( i, &pomocno, "char" );
}

void prekidna_rutina( int sig )
{
    int x = -1;
    interrupt_disable();
    
    char pomocno = 'X';
    switch(sig){
    case SIGALRM:
        x = 1;
        break;
    case SIGTERM:
        x = 2;
        break;
    case SIGTSTP:
        x = 3;
        break;
    case SIGABRT:
        x = 4;
        break;
    case SIGINT:
        x = 5;
        break;
    default:
        printf("Primljen nedopusten signal! Terminating program...");
        exit(-3);
    }
    if( x  >= 1 && x < N){
        K_Z[x]=1;
        ispis_redak( x ,  &pomocno , "char" );
    }

    /* Sada moramo obaviti glavni dio obrade prekida -
    - odredivati(vise puta - LOOP) signale veceg prioriteta
    i zatim njih obradivati. */

    do {
        
        /* Sada treba odrediti signal najveceg
    prioriteta koji ceka obradu */
        
        x = 0;
        for(int j = T_P + 1; j < N; j++){
            if((K_Z[j] == 1) && j > T_P) {
                x = j;
            }
        }

        //Sada na obradu samoga prekida!(prije toga naravno overhead work)
        if( x > 0 ){
            K_Z[x] = 0;
            KON[x] = T_P;
            T_P = x;
            
            interrupt_enable();
            obrada_signala( x );
            interrupt_disable();

            T_P = KON[x];
            KON[x] = 0;
        }
    } while ( x > 0);
    interrupt_enable();
}

int main(void)
{
    //Directanje signala na prekidnu rutinu
    for(int i = 0; i < N-1; i++){
        sigset(sig[i], prekidna_rutina);
    }
    interrupt_enable();

    printf("Proces obrade prekida, PID=%d\n", getpid());
    
    /* Sada treba ispisati pocetno zaglavlje */
    printf("GP S1 S2 S3 S4 S5\n"
    "-----------------\n");

    // troši vrijeme da se ima šta prekinuti - PROGRAM_TIME s
    
    for(int i = 1; i <= PROGRAM_TIME; i++){
        ispis_redak( T_P , &i, "int" );
        sleep(PERIOD);
    }
    
    printf("Zavrsio osnovni program\n");
    return 0;
}