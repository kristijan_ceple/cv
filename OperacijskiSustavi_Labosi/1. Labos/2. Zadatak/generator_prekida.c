/**
 * Generator Prekida -- salje signale obradi,
 * a ona simulira prekidnu rutinu!
 * */

#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#define N 6
#define PROGRAM_TIME 20

long app_pid;
int pauza = 0;

void prekidni_potprogram(int sig)
{
    if(sig != SIGINT){
        printf("Error! Pogresno poslan signal SIGINT fji!\n");
        return;
    }
     kill( app_pid, SIGKILL );
     exit(0);
}

void controlPause( int sig )
{
    if(sig != SIGALRM ){
        printf("Improper signal! Terminating...");
        exit(-1);
    }
    sleep(1);
    printf("Pokrecem pauzu!\n");
    sleep(1);

    pauza = 1 - pauza;
}

int main( int argc, char* argv[] )
{
    if(argc != 2) exit(-1);
    
    int sig[] = {SIGALRM, SIGTERM, SIGTSTP, SIGABRT, SIGINT};
    sigset( SIGINT, prekidni_potprogram );
    sigset( SIGALRM, controlPause );
    printf("PID generatora=%d\n", getpid());

    srand((unsigned)time(NULL));
    app_pid = atoi(argv[1]);

    for(int i = 0; i < PROGRAM_TIME; i++){
        int randomNumber = rand()%4;
        kill( app_pid, sig[randomNumber] );
        if( pauza ) break;

        sleep(1);
    }

    kill( app_pid, SIGINT );
    printf("Izlazim iz programa...\n");

    return -1;
}