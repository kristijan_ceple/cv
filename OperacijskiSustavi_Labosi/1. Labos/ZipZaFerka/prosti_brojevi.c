#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>

unsigned long zadnji;
char pauza = 0;

void postavi_pauzu( int sig )
{
    pauza = 1 - pauza;
    switch(pauza)
    {
        case 0:
            printf("Zadnji poslije kraja pauze: %ld\n", zadnji);
            break;
        case 1:
            printf("Zadnji prije pocetka pauze: %ld\n", zadnji);
            break;
        default:
            break;
    }
}

void periodicki_ispis ( int sig )
{
    printf ( "Zadnji prosti broj izracunat: %ld\n", zadnji);
}

void prekini( int sig )
{
    printf ( "Izlazim iz programa...");
    printf ( "Zadnji prosti broj izracunat: %ld\n", zadnji);
    exit(1);
}

int prost ( unsigned long n )
{
    unsigned long i, max;

    if ( ( n & 1 ) == 0 ) /* je li paran? */
        return 0;

    max = sqrt ( n );
    for ( i = 3; i <= max; i += 2 )
        if ( ( n % i ) == 0 )
            return 0;

    return 1; /* broj je prost! */
}

int main()
{
    unsigned long broj = 3;    
    struct itimerval t;
    sigset ( SIGALRM, periodicki_ispis );
    sigset ( SIGINT, postavi_pauzu );
    sigset ( SIGTERM, prekini );
    
    /* definiranje periodičkog slanja signala */
    /* prvi puta nakon: */
    t.it_value.tv_sec = 2;
    t.it_value.tv_usec = 0;
    /* nakon prvog puta, periodicki sa periodom: */
    t.it_interval.tv_sec = 5;
    t.it_interval.tv_usec = 0;
    setitimer ( ITIMER_REAL, &t, NULL );

    while(1)
    {
        if(prost(broj)) zadnji = broj;
        broj++;
        
        while(pauza)
            pause();
    }

    return 0;
}