#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <values.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdatomic.h>

atomic_int n, m, *num, lastNum, A;
atomic_bool *entry, threadCont;
pthread_t *thr_id = NULL;

//######################        EXIT FUNCTIONS      ############################
void VarAndMemClean()
{
    printf("Before termination A = %d\n", A);

    free(thr_id);
    free(num);
    free(entry);

    printf("Memory cleaned! Exiting...\n");
}

void SigInt( int sig )
{
    printf("\nExit cause: SIGINT! ");
    VarAndMemClean();
    exit(1);
}

void ExitProgram()
{   
    printf("Normal program exit.\n");
    for(int i = 0; i < n; i++){
        pthread_join(thr_id[i], NULL);
    }
    
    VarAndMemClean();
    exit(0);
}
//######################        EXIT FUNCTIONS      ############################

// KRITICNI ODSJECAK function!!!
void KOFoo(int threadIndex, int* counter)
{   
    (*counter)++;
    A++;
    // printf("CRITICAL SEGMENT: Thread #%d - A++ = %d\n", threadIndex, A);
}
 // NE-KRITICNI ODSJECAK function!!!
void NKOFoo(int threadIndex)
{
    // printf("NONCRITICAL SEGMENT: Thread #%d!\n", threadIndex);
}

void EnterCritSeg(int i)
{
    entry[i] = true;
    num[i] = lastNum + 1;
    lastNum = num[i];
    entry[i] = false;

    for(int j = 0; j < n; j++){
        while(entry[j] == true);
        while( (num[j] != 0) && ( (num[j] < num[i]) || ( num[j] == num[i] && (j < i) ) ) );
    }
}

void LeaveCritSeg(int i)
{
    num[i] = 0;
}

void *thread(void *arg)
{
    while(!threadCont);
    int counter = 0;
    int i = *((int *) arg);

    // printf("Thread number #%d beginning the op!\t"
    //         "Before incrementing A = %d\n", i, A);
    
    do{
        EnterCritSeg(i);
        KOFoo(i, &counter);
        LeaveCritSeg(i);

        NKOFoo(i);
    } while(counter < m);
    
    // printf("Thread #%d done --> A = %d\n", i, A);
    // printf("Thread exiting...\n\n");
    pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
    if(argc != 3){
        printf("Wrong number of arguments: %d!\n", argc);
        exit(1);
    }

    n = atoi(argv[1]);
    m = atoi(argv[2]);
    lastNum = 0;
    entry = (atomic_bool *) malloc(n * sizeof(bool));
    num = (atomic_int *) malloc(n * sizeof(int));

    sigset(SIGINT, SigInt);
    A = 0;

    int threadIndex[n];
    thr_id = (pthread_t *) malloc(n * sizeof(pthread_t));
    
    threadCont = false;
    for(int i = 0; i < n; i++){
        threadIndex[i] = i;
        // printf("Creating thread #%d...\n", i);
        if( pthread_create((thr_id + i), NULL, thread, (void *)(&threadIndex[i]) ) != 0){
            printf("Error while creating threads!\n");
            exit(1);
        }
    }
    threadCont = true;
    //Main did its thing, from now on its up to the threads function and other functions

    ExitProgram();
}


