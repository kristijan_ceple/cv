#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <values.h>
#include <pthread.h>
#include <stdbool.h>

int A, M, N;
pthread_t *thr_id = NULL;
bool threadCont;

void *IncrementA(void* arg)
{
    // while(!threadCont);
    int threadIndex = *( (int *) arg );
    printf("\nThread #%d beginning the op.\t"
            "Before incrementing A = %d\n", threadIndex, A);

    for(int i = 0; i < M; i++){
        ++A;
        printf("++A = %d\tindex(for_loop) = %d\n", A, i);
    }
    printf("Job done -  Thread #%d done --> A = %d\n\n", threadIndex, A);
    pthread_exit(NULL);
}

//######################        EXIT FUNCTIONS      ############################
void VarAndMemClean()
{
    printf("Before termination A = %d\n", A);
    free(thr_id);
    printf("Memory cleaned! Exiting...\n");
}

void ExitProgram()
{
    printf("\nNormal program exit.\n\n");
    for(int i = 0; i < N; i++){
        pthread_join(thr_id[i], NULL);
    }

    VarAndMemClean();
    exit(0);
}

void SigInt( int sig )
{
    printf("\nExit cause: SIGINT! ");
    VarAndMemClean();
    exit(1);
}
//######################        EXIT FUNCTIONS      ############################
int main(int argc, char* argv[]){
    if(argc != 3){
        printf("Wrong number of arguments: %d!\n", argc);
        exit(1);
    }
    N = atoi(argv[1]);
    M = atoi(argv[2]);
    sigset(SIGINT, SigInt);
    A = 0;

    int threadIndex[N];
    thr_id = (pthread_t *) malloc(N * sizeof(pthread_t));
    //DON'T FORGET TO FREE LATER!!!

    threadCont = false;
    for(int i = 0; i < N; i++){
        printf("Creating thread #%d...\n", i);
        threadIndex[i] = i;
        if( pthread_create((thr_id + i), NULL, IncrementA, (void *)(&threadIndex[i]) ) != 0){
            printf("Error while creating threads!\n");
            exit(1);
        }
    }
    threadCont = true;

    ExitProgram();
}