#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <values.h>

int Id, *A, i, myForkRet;
/* myForkRet glavnog procesa cemo staviti na -1,
    a od djece na 0! */

int FreeSHM(void)
{
    for(int i = 1; i < ULONG_MAX; i++){
        //nisam siguran treba li prvo detachati
        /* if (shmdt(i, IPC_RMID, NULL) != -1)
            printf("Obrisao zajednicku memoriju %d\n", i); */

        if(shmctl(i, IPC_RMID, NULL) != -1)
            printf("Obrisao zajednicku memoriju %d\n", i);
    
        if(semctl(i, 0, IPC_RMID, 0) != -1)
            printf("Obrisao skup semafora %d\n", i);
    
        if(msgctl(i, IPC_RMID, NULL) != -1)
            printf("Obrisao red poruka %d\n", i);
    }
    return 0;
}

void IncrementA(int M)
{
    for(int i = 0; i < M; i++){
        *A = *A + 1;
        printf("++A = %d\tM = %d\n", *A, i);
    }    
}

//######################        EXIT FUNCTIONS      ############################
void VarAndMemClean()
{
    printf("Before termination A = %d\n", *A);

    (void) shmdt((char *) A);
    (void) shmctl(Id, IPC_RMID, NULL);

    printf("Memory cleaned! Exiting...\n");
}

void SigInt( int sig )
{
    if(myForkRet == 0){
        printf("Terminating child process #%d!\n", i);
        exit(0);
    }
    
    printf("\nExit cause: SIGINT! ");
    VarAndMemClean();
    exit(1);
}

void ExitProgramLong( int sig )
{
    printf("Exit cause: %d\n", sig);
    FreeSHM();
    printf("Memory cleaned! Exiting...\n");
    exit(sig);
}

void ExitProgram()
{
    printf("Normal program exit.\n\n");
    if(myForkRet == 0){
        printf("Terminating child process #%d!\n", i);
        exit(0);
    }
    
    VarAndMemClean();
    exit(0);
}
//######################        EXIT FUNCTIONS      ############################

int main(int argc, char* argv[]){
    if(argc != 3){
        printf("Wrong number of arguments: %d!\n", argc);
        exit(1);
    }
    int N = atoi(argv[1]);
    int M = atoi(argv[2]);
    sigset(SIGINT, SigInt);

    Id = shmget(IPC_PRIVATE, sizeof(int), S_IRWXU);
    if(Id == -1)
        exit(1);

    A = (int *) shmat(Id, NULL, 0);
    *A = 0;

    //za i = 0 ce biti ID parent procesa
    for(i = 1; i <= N; i++){
        myForkRet = fork();
        if(myForkRet == 0){
            printf("I'm a NEW process #%d\n", i);
            break;
        }
    }

    if(myForkRet != 0)
        i = 0;      //Parent proces

    
    printf("\nI'm process #%d\t"
    "Before incrementing A = %d\n", i, *A);
    if(myForkRet == 0){
        IncrementA(M);
        printf("Process #%d finished incrementing A!\t"
                "*A = %d, exiting...\n\n", i, *A);
        exit(0);
    }
        

    for(int i = 0; i < N; i++){
        (void) wait(NULL);
    }
    printf("\nI'm the parent process and I've closed all the children.\n");
    ExitProgram();

    // if( == 0){
    //         //ovo rade djeca!!! Dakle ona ce inkrementirati var
    //         IncrementVar();
    //     }
}