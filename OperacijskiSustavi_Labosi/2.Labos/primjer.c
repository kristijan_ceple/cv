#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>

int Id;
int *CommVar;

void Writer(int i)
{
    *CommVar = i;
}

void Reader(void)
{
    int i;
    do{
        i = *CommVar;
        printf("CommVar before do-while loop = %d\n", *CommVar);
        sleep(1);
    } while(i == 0);
    printf("CommVar after do-while loop = %d\n", i);
}

void delete(int sig)
{
    //freeing the process shared memory
    (void) shmdt((char *) CommVar);
    (void) shmctl(Id, IPC_RMID, NULL);
    exit(0);
}

int main(void)
{
    Id = shmget(IPC_PRIVATE, sizeof(int), S_IRWXU);

    // printf("ULONG_MAX = %lu\n", ULONG_MAX);
    // printf("LONG_MAX = %ld\n", LONG_MAX);

    if(Id == -1)
        exit(1);

    CommVar = (int *) shmat(Id, NULL, 0);
    *CommVar = 0;
    sigset(SIGINT, delete);

    if(fork() == 0){
        Reader();

        //ovo dijete ceka dok
        //roditelj ne promjeni zajednicku varijablu
        exit(0);
    }

    if(fork() == 0){
        sleep(5);

        /*drugo dijete ce promjeniti
        globalnu varijablu --> NAKON 5 SEC
        pa ce prvo dijete exitati,
        a zatim ce i ovo, drugo exitat */ 

        Writer(123);
        exit(0);
    }

    (void) wait(NULL);
    (void) wait(NULL);
    delete(0);

    return 0;
}