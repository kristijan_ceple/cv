import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

/*
Example program:

0|0,2,0|1,2,0
q1,q2,q3
0,1,2
J,N,K
q3
q1
K
q1,0,K->q1,NK
q1,1,K->q1,JK
q1,0,N->q1,NN
q1,1,N->q1,JN
q1,0,J->q1,NJ
q1,1,J->q1,JJ
q1,2,K->q2,K
q1,2,N->q2,N
q1,2,J->q2,J
q2,0,N->q2,$
q2,1,J->q2,$
q2,$,K->q3,$

a
q0
a
K
q0
q0
K
q0,a,K->q0,K

a
q0,q1
a
K
q1
q0
K
q0,$,K->q1,K
q1,a,K->q1,K

5. Test
a
q0,q1
a
K
q1
q0
K
q0,$,K->q1,$
q1,a,K->q1,K

13. Test
a,a,b
q0,q1,q2
a,b
K,X,Y
q1
q0
K
q0,a,K->q1,XK
q1,a,X->q1,YX
q1,b,Y->q2,Y
q2,$,Y->q2,XY
*/

/**
 * Simulates a push automaton.
 */
public class SimPa {
	/**
	 * Epsilon transition - used to reach acceptable states at the end of input
	 */
	private static final AlphabetChar EPSILON = new AlphabetChar("$");
	private static final String EMPTY_STATE_STRING = "#";

	private static class AlphabetChar implements Comparable<AlphabetChar>{
		
		private String value;

		public AlphabetChar(String value) {
			Objects.requireNonNull(value);
			
			this.value = value;
		}
		
		public AlphabetChar(char value) {
			Objects.requireNonNull(value);
			
			this.value = String.valueOf(value);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AlphabetChar other = (AlphabetChar) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value;
		}

		@Override
		public int compareTo(AlphabetChar o) {
			return this.value.compareTo(o.value);
		}
		
	}
	
	/**
	 * Features the automaton's states. Those states feature
	 * their own transitions stored internally.
	 * 
	 * @author kikyy99
	 *
	 */
	private static class PushAutomaton {
		
		/**
		 * Default empty state
		 */
		private State EMPTY_STATE = new State("#", false);
		private Set<State> states =  new TreeSet<>();
		private Set<State> visitedStates = new LinkedHashSet<>();
		private State startingState;
		private AlphabetChar startingStackCharacter;
		private State activeState;
		private Set<AlphabetChar> alphabet = new TreeSet<>();
		private Set<AlphabetChar> stackCharacters = new TreeSet<>();
		private List<AlphabetChar> input = new ArrayList<>();
		private Stack<AlphabetChar> stack = new Stack<>();
		
		
		public PushAutomaton(Set<AlphabetChar> alphabet, Set<AlphabetChar> stackCharacters) {
			Objects.requireNonNull(alphabet);
			
			this.alphabet.addAll(alphabet);
			this.stackCharacters.addAll(stackCharacters);
		}
		
		private State getState(String find) {
			if(find.equals(EMPTY_STATE_STRING)) {
				return EMPTY_STATE;
			}
			
			for(State currState : states) {
				if(currState.name.equals(find)) {
					return currState;
				}
			}
			
			return null;
		}
		
		/**
		 * Returns a NEW SET of acceptable states
		 * 
		 * @return a new set of acceptable states
		 */
		public Set<State> getAcceptableStates() {
			return retrieveSpecifiedAcceptablenessStates(true);
		}
		
		/**
		 * Returns a NEW SET of unacceptable states
		 * 
		 * @return a new set of unacceptable states
		 */
		public Set<State> getUnacceptableStates() {
			return retrieveSpecifiedAcceptablenessStates(false);
		}
		
		private Set<State> retrieveSpecifiedAcceptablenessStates(boolean acceptable) {
			Set<State> returnStates = new HashSet<>();
			
			for(State state : states) {
				if(state.acceptedState && acceptable) {
					returnStates.add(state);
				} else if(!state.acceptedState && !acceptable) {
					returnStates.add(state);
				}
			}
			
			return returnStates;
		}
		
		public String SecondLabOutput() {
			StringBuilder sb = new StringBuilder();
			
			String statesString = states.toString();
			statesString = statesString.replaceAll("\\s*", "");
			sb.append(statesString);
			sb.append(System.lineSeparator());
			
			String alphabetString = alphabet.toString();
			alphabetString = alphabetString.replaceAll("\\s*", "");
			sb.append(alphabetString);
			sb.append(System.lineSeparator());
			
			// Acceptable states
			String acceptableStatesString = getAcceptableStates().toString();
			acceptableStatesString = acceptableStatesString.replaceAll("\\s*", "");
			sb.append(acceptableStatesString);
			sb.append(System.lineSeparator());
			
			sb.append(startingState);
			
			//Transitions
			for(State state : states) {
				for(SimPa.PushAutomaton.State.Transition transition : state.transitions) {
					sb.append(System.lineSeparator());
					sb.append(transition.lab2String());
				}
			}
			
			sb.append(System.lineSeparator());
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
//			toPrint = toPrint.replaceAll("\\s*", "");
			
			return toPrint;
		}
		
		public void setStartingState(String name) {
			Objects.requireNonNull(name);
			
			State startingState = this.getState(name);
			this.startingState = startingState;
			this.activeState = startingState;
			this.visitedStates.add(startingState);
		}
		
		public void setStartingStackCharacter(String name) {
			Objects.requireNonNull(name);
			
			AlphabetChar startingStackCharacter = new AlphabetChar(name);
			stack.push(startingStackCharacter);
			this.startingStackCharacter = startingStackCharacter;
		}
		
		private void setInput(List<AlphabetChar> input) {
			this.input.addAll(input);
		}
		
		private void addState(String name, boolean acceptedState) {
			State toAdd = new State(name, acceptedState);
			states.add(toAdd);
		}
		
		private void bindTransitions(String stateName, Map<String[], String[]> transitions) {
			State state = this.getState(stateName);
			state.addTransitions(transitions);
		}
		
//		private void addStates(Map<String, List<Boolean, Map<String, State[]>>> statesMap) {
//			for(Entry<String, Boolean> entry : statesMap.entrySet()) {
//				String name = entry.getKey();
//				Boolean acceptedState= entry.getValue();
//				
//				this.addState(name, acceptedState);
//			}
//		}
		
		/**
		 * Reads the input sequence, and prints it out as well
		 */
		private void readInputSequence() {
			StringBuilder sb = new StringBuilder();
			appendWriteOutData(sb);
			for(int i = 0; i <= input.size(); i++) {
//				sb.append("|");
				if(i == input.size() && activeState.acceptedState == true) {
					break;
				}
				
				if(readInputChar(i, sb) == false) {
					//input didn't succeed - append fail and move on
					sb.append("fail|");
					break;
				}
//				if(activeStates.size()>=2) {
//					//in case an empty state still made it into the set,
//					//but there are more than 2 states in the set itself
//					activeStates.remove(EMPTY_STATE);
//				}
				appendWriteOutData(sb);
			}
			
			//let's append whether it was accepted or not
			sb.append(activeState != null ? 1 : 0);			//if we're not in an acceptable state epsilon
			//transitions shall be attempted - if they fail as well the activeState will be set to null
			//so two cases: either an acceptable state is found(non-null) or not found(set to null)
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
			toPrint = toPrint.replaceAll("\\s*", "");
			System.out.println(toPrint);
		}
		
		private void appendWriteOutData(StringBuilder sb) {
			Objects.requireNonNull(sb);
			
			if(activeState == null) {
				//last state and its epsilon env were not acceptable
				return;
			}
			
			sb.append(activeState);
			sb.append("#");
			
			int i = stack.size();
			if(i == 0) {
				sb.append(EPSILON);		//no items on stack
			} else {
				i--;
				do {
					sb.append(stack.get(i));
					i--;
				} while(i >= 0);
			}
			
			sb.append("|");
		}
		
		/**
		 * Reads a single input character(note: a single INPUT CHARACTER - it can be composed of more
		 * letters or numbers -- depends on the definition)
		 * 
		 * @param nextIndex the index of the input character to be read
		 * @return whether the input succeeded or not
		 */
		private boolean readInputChar(int nextIndex, StringBuilder sb) {
			if(startingState == null || startingStackCharacter == null) {
				throw new NullPointerException("Starting state or stack character not set!");
			}
			
			/*
			 * We do as follows. We read the input character, and perform the transition on the
			 * currently active state. If the transition succeeds - we will get a new activeState together
			 * with new Stack content. If it fails however, we shall print out "fail"
			 * 
			 * When attemtping a transition we first have to check for the current character whether there are
			 * ANY transitions. If not, first attempt an epsilon transition. If still not - fail and then end
			 * 
			 * If all the input HAS BEEN READ, and we're in an acceptable state - then that's it.
			 * However, if all the input has been read and we're not in an acceptable state - then we have
			 * to look for epsilon transitions and if there are any - we perform them and then see whether
			 * we're in an acceptable state. Return whether the input was acceptable or not
			 */
			
			if(nextIndex > input.size()) {
				throw new IndexOutOfBoundsException("No more input chars left to be read!");
			} else if(nextIndex == input.size()) {
				//the last character has been read. check if we're in an acceptable state
				
				if(activeState.acceptedState) {
					return true;
				}
				
//				State backupState = activeState;
//				while(activeState.performTransition(EPSILON)) {
//					if(activeState != null && activeState.acceptedState == true) {
//						//transition successful AND the state is accepted - perfect just return true
//						return true;
//					} else {
//						//either transition not successful or the state was not accepted
//					}
//					backupState = activeState;
//				}
				
				//okay, maybe there's an epsilon transition into an active state? Let's try to find it
				//perform epsilon transitions as long as they exist AND the active state IS NOT ACCEPTABLE
				State backupState = activeState;
//				StringBuilder sbExpansion = new StringBuilder();
				while(activeState.performTransition(EPSILON, true, sb)) {
					if(activeState != null) {
						//transition successful
						if(activeState.acceptedState == true) {
							return true;			//perfect - success!
						}
						
						appendWriteOutData(sb);		//attempt further epsilon transitions!
					} else {
						//no epsilons will get us into an acceptable state - restore the backup and return;
						activeState = backupState;
						return true;
					}
				}
				
				//couldn't find an acceptable state through epsilon transitions - well we truly have reached the end then
				return true;
				
			}
			
			//Let's read the next input character and perform the transition for it
			AlphabetChar currentChar = input.get(nextIndex);

			//perform transitions for the current active State
			return activeState.performTransition(currentChar, false, sb);
		}
		
		public void reset() {
			this.activeState = null;
			this.activeState = startingState;
			this.input.clear();
			this.stack.clear();
			this.stack.push(startingStackCharacter);
		}
		
//		private Set<State> dkaTransitionStates(State testedState){
//			Set<State> transitionStates = new HashSet<>();
//			
//			for(SimPa.PushAutomaton.State.Transition transition : testedState.transitions) {
//				transitionStates.add(transition.targetState);
//			}
//			
//			return transitionStates;
//		}
		
//		public String visitedStates() {
//			StringBuilder sb = new StringBuilder();
//			
//			sb.append(visitedStates);
//			sb.append("|");
//			
//			visitedStates.clear();
//			return sb.toString();
//		}

		/**
		 * Models a SINGLE transition - therefore it MUST have an input, 
		 * and it MUST have at least one target state. It SHALL not be used
		 * to model transitions to the {@link EMPTY_STATE} or model transitions
		 * that have no input(input is null). 
		 * 
		 * @author kikyy99
		 *
		 */
		private class State implements Comparable<State>{
			
			private String name;
			private final boolean acceptedState;
			private List<Transition> transitions = new ArrayList<>();
			private boolean active;
			private AlphabetChar lastCharacter;
			
			public State(String name, boolean acceptedState, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we have some transition(s) from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			public State(String name, boolean acceptedState) {
				this(name, acceptedState, (Transition[])null);
			}
			
			public void addTransition(String inputCharacter, String poppedStackCharacters,
					String destinationState, String pushStackCharacters) {
				Objects.requireNonNull(inputCharacter);
				Objects.requireNonNull(poppedStackCharacters);
				Objects.requireNonNull(destinationState);
				Objects.requireNonNull(pushStackCharacters);
				
				Transition toAdd = new Transition(inputCharacter, poppedStackCharacters,
						destinationState, pushStackCharacters); 
				transitions.add(toAdd);
			}
			
			public void addTransitions(Map<String[], String[]> transitions) {
				Objects.requireNonNull(transitions);
				
				for(Entry<String[], String[]> entry : transitions.entrySet()) {
					String[] leftSide = entry.getKey();		//	input char and exp. popped stack chars
					String[] rightSide = entry.getValue();	// destination and pushed stack chars
					
					String inputCharacter = leftSide[0];
					String destinationState = rightSide[0];
					
					String poppedStackCharacter = leftSide[1];
					String pushStackCharacters = rightSide[1];
					
					Transition toAdd = new Transition(inputCharacter, poppedStackCharacter, 
							destinationState, pushStackCharacters);
					this.transitions.add(toAdd);
				}
			}
			
			
			/**
			 * Performs a transition
			 * 
			 * @param currentChar the input alphabet char
			 * @return true if transition successful, false otherwise
			 */
			private boolean performTransition(AlphabetChar currentChar,
					boolean repeatedEpsilonForSameState, StringBuilder sb) {
				/**
				 * First we have to find the matching transition. We do that by comparing whether both
				 * the input character and the expected popped stack characters match -- if they do then we've
				 * found our transition.
				 * 
				 * After we've found our transition, we have to push the characters onto the stack, and change
				 * the active state to the next state.
				 */
				if(activeState == null) {
					return false;
				}
				
				boolean matches = false;
				Transition matchedTrans = null;
				State nextState;
				
				//both the current input, and the expected popped stack chars have to match
				for(State.Transition currentTransition : transitions) {
					if(currentTransition.input.equals(currentChar)) {
//						//now let's check if the popped stack characters match
//						boolean stackMatch = true;
//						for(int i =  0, j = stack.size() - 1;
//								i < currentTransition.poppedStackCharacters.size() && j >= 0; i++, j--) {
//							if(!currentTransition.poppedStackCharacters.get(i).equals(stack.get(j))) {
//								stackMatch = false;
//								break;
//							}
//						}
//						
//						if(stackMatch) {
//							matches = true;
//							matchedTrans = currentTransition;
//							break;
//						} 
						
						//check if popped characters match
						if(!currentTransition.poppedStackCharacter.equals(EPSILON) && stack.size() > 0) {
							AlphabetChar poppedChar = stack.peek();
							
							if(poppedChar.equals(currentTransition.poppedStackCharacter)) {
								//okay
								matches = true;
								matchedTrans = currentTransition;
								break;
							}
						} else if(currentTransition.poppedStackCharacter.equals(EPSILON)) {
							matches = true;
							matchedTrans = currentTransition;
							break;
						} else {
							//it's gonna fail - empty stack and not epsilon transition
							break;
						}
					}
				}
				
				this.active = false;
				this.lastCharacter = currentChar;
				//okay, time to do the algo part
//				if(matchedTrans == null && activeState != null) {
//					//state went into nothing - nothing too bad
//					nextState = EMPTY_STATE;
//					return null;
//				}
				
				if(!matches) {
					if(currentChar.equals(EPSILON) && repeatedEpsilonForSameState == true) {
						//truly no more options - FAILURE ENCOUNTERED!
						activeState = null;
						return false;
					} else {
						//not matched and no active states, go to empty state -- check for epsilon transitions first!
						//first try epsilon
						if(activeState.performTransition(EPSILON, true, sb) == false) {
							//no epsilon transitions found - acftiveState is null
							return false;
						}
						appendWriteOutData(sb);				//need to somehow append to sb here
						return activeState.performTransition(currentChar, false, sb);	//after epsilons, try again the current character
					}
				}
				
				//else we have a normal state
				nextState = matchedTrans.targetState;
				nextState.active = true;
				visitedStates.add(nextState);
				activeState = nextState;
				
				//Let's do the stack operations part
//				for(int i = 0; i < matchedTrans.poppedStackCharacters.size(); i++) {
//					stack.pop();			//first pop, then push the data onto the stack
//				}
				
				if(!matchedTrans.poppedStackCharacter.equals(EPSILON)) {
					stack.pop();
				}

				//now push the new data onto the stack
				for(int i = matchedTrans.pushOntoStack.size() - 1; i >= 0; i--) {
					AlphabetChar toPush = matchedTrans.pushOntoStack.get(i);
					if(!toPush.equals(EPSILON)) {
						stack.push(toPush);	
					}
				}
				
				//that's it. 
				return true;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((name == null) ? 0 : name.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				State other = (State) obj;
				if (name == null) {
					if (other.name != null)
						return false;
				} else if (!name.equals(other.name))
					return false;
				return true;
			}
			
			@Override
			public int compareTo(State o) {
				return this.name.compareTo(o.name);
			}
			
			@Override
			public String toString() {
				return name;
			}

			private class Transition implements Comparable<Transition>{
				
				/**
				 * The input character
				 */
				private AlphabetChar input;
				/**
				 * These characters need to be checked against the actual popped characters from the Stack
				 */
				private AlphabetChar poppedStackCharacter;
				/**
				 * ALL THE TARGET STATES - they MUST be UNIQUE
				 */
				private State targetState;
				/**
				 * The characters to be pushed onto the stack
				 */
				private List<AlphabetChar> pushOntoStack = new ArrayList<SimPa.AlphabetChar>();
				
				/**
				 * Constructor.
				 * @param input
				 * @param targetStates
				 */
				private Transition(String inputCharacter, String poppedStackCharacter,
						String destinationState, String pushStackCharacters) {
					Objects.requireNonNull(inputCharacter);
					Objects.requireNonNull(poppedStackCharacter);
					Objects.requireNonNull(destinationState);
					Objects.requireNonNull(pushStackCharacters);
					
					// Adding the input character
					this.input = new AlphabetChar(inputCharacter);						//adding the input character
					
					// Adding the popped exp. stack chars
//					for(char currStackChar : poppedStackCharacters.toCharArray()) {
//						AlphabetChar stackCharacter = new AlphabetChar(currStackChar);
//						this.poppedStackCharacters.add(stackCharacter);
//					}
					this.poppedStackCharacter = new AlphabetChar(poppedStackCharacter)
;
					// Adding the destination state
					State toAdd = PushAutomaton.this.getState(destinationState);		
					this.targetState = toAdd;									//adding the destination state
					
					// Adding the pushed stack chars
					for(char currStackChar : pushStackCharacters.toCharArray()) {
						AlphabetChar stackCharacter = new AlphabetChar(currStackChar);
						this.pushOntoStack.add(stackCharacter);
					}
					
				}
				
				private State getOuterClassInstance() {
					return State.this;
				}

				
				
				public String lab2String() {
					String toReturn = String.format("%s,%s->%s", State.this, input, targetState);
					toReturn.replace("[", "");
					toReturn.replace("]", "");
					
					return toReturn;
				}
				
				

				@Override
				public int hashCode() {
					final int prime = 31;
					int result = 1;
					result = prime * result + getEnclosingInstance().hashCode();
					result = prime * result + ((input == null) ? 0 : input.hashCode());
					result = prime * result + ((poppedStackCharacter == null) ? 0 : poppedStackCharacter.hashCode());
					result = prime * result + ((pushOntoStack == null) ? 0 : pushOntoStack.hashCode());
					result = prime * result + ((targetState == null) ? 0 : targetState.hashCode());
					return result;
				}

				@Override
				public boolean equals(Object obj) {
					if (this == obj)
						return true;
					if (obj == null)
						return false;
					if (getClass() != obj.getClass())
						return false;
					Transition other = (Transition) obj;
					if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
						return false;
					if (input == null) {
						if (other.input != null)
							return false;
					} else if (!input.equals(other.input))
						return false;
					if (poppedStackCharacter == null) {
						if (other.poppedStackCharacter != null)
							return false;
					} else if (!poppedStackCharacter.equals(other.poppedStackCharacter))
						return false;
					if (pushOntoStack == null) {
						if (other.pushOntoStack != null)
							return false;
					} else if (!pushOntoStack.equals(other.pushOntoStack))
						return false;
					if (targetState == null) {
						if (other.targetState != null)
							return false;
					} else if (!targetState.equals(other.targetState))
						return false;
					return true;
				}

				@Override
				public String toString() {
					String toReturn = String.format("&(%s, %s, %s) = (%s, %s)", State.this, input, poppedStackCharacter, 
							targetState, pushOntoStack);
					toReturn.replace('[', '{');
					toReturn.replace(']', '}');
					
					return toReturn;
				}

				@Override
				/**
				 * The comparison and sorting shall be done in the the lexicographical 
				 * order of AlphabetChar inputs of these 2 Transitions
				 * 
				 * @param o the Other transition
				 * @return -1, 0, or 1 depending on the inputs' compareTo method
				 */
				public int compareTo(Transition o) {
					return this.input.compareTo(o.input);
				}

				private State getEnclosingInstance() {
					return State.this;
				}
			}
		}
	}
	
	public static <K, V> boolean checkMapValuesEquality(Map<K, V> first, Map<K, V> second) {
		HashSet<V> set1 = new HashSet<>(first.values());
		HashSet<V> set2 = new HashSet<>(second.values());
		
		return set1.equals(set2);
		
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		//let's get parsing
		List<List<String>> inputSequences;
		Set<String> states;
		Set<String> alphabet;
		Set<String> acceptableStates;
		String startState;
		String startStackCharacter;
		Set<String> transitions;
		Set<String> stackCharacters;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
		//first row - state sequences
		String input = reader.readLine();
		String[] parts = input.split("\\|");
		inputSequences = new ArrayList<>(parts.length);
		for(int i = 0; i < parts.length; i++) {
			//now must split by commas
			String currentString = parts[i];
			String[] subParts = currentString.split(",");
			List<String> subList = new ArrayList<String>(subParts.length);
			for(String part : subParts) {
				part = part.trim();
				subList.add(part);
			}
			inputSequences.add(subList);
		}
		
		//second row - states set
		input = reader.readLine();
		parts = input.split(",");
		states = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			states.add(part);
		}
		
		//third row - alphabet
		input = reader.readLine();
		parts = input.split(",");
		alphabet = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			alphabet.add(part);
		}
		//fourth row - stack characters
		input = reader.readLine();
		parts = input.split(",");
		stackCharacters = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			stackCharacters.add(part);
		}
		
		
		//fifth row -  acceptable states set
		input = reader.readLine();
		parts = input.split(",");
		acceptableStates = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			acceptableStates.add(part);
		}
		
		//sixth row - starting state
		startState = reader.readLine();
		startState = startState.trim();
		states.add(startState);
		
		//seventh row - starting state
		startStackCharacter = reader.readLine();
		startStackCharacter = startStackCharacter.trim();
		stackCharacters.add(startStackCharacter);
		
		//eighth row+ - transitions
		transitions = new HashSet<>();
		
		input = reader.readLine();
		while(input != null) {
			input = input.trim();
			transitions.add(input);
			input = reader.readLine();
		}
	
//###################################		INPUT PARSING		######################################
		
		//Let's construct an automaton!
		//Add the alphabet
		Set<AlphabetChar> alphabetSet = new HashSet<>();
		for(String alphabetCharacter : alphabet) {
			AlphabetChar toAdd = new AlphabetChar(alphabetCharacter);
			alphabetSet.add(toAdd);
		}
		
		
		//Add the stack characters
		Set<AlphabetChar> stackCharactersSet = new HashSet<>();
		for(String stackCharacter : stackCharacters) {
			AlphabetChar toAdd = new AlphabetChar(stackCharacter);
			stackCharactersSet.add(toAdd);
		}
		PushAutomaton ourAutomaton = new PushAutomaton(alphabetSet, stackCharactersSet);
		
		//Make the StateData array featuring data about all the states
		Map<String, StateData> statesData = new HashMap<String, StateData>();
		for(String currentState : states) {
			//let's see if it's acceptable
			boolean acceptable = false;
			if(acceptableStates.contains(currentState)) {
				acceptable = true;
			}
			
			//need to go through all the transitions, and make the map
			//&(q, a, Z) = (p, y)
			Map<String[], String[]> transitionsMap = new HashMap<>();
			for(String transition : transitions) {
				transition.replaceAll("\\s", "");		//remove all whitespace
				parts = transition.split("->");	
				//parts[0] contains current state, input character, and stack chars to be popped
				//parts[1] contains the next state and the stack chars to be pushed
				String[] subPartsLeft = parts[0].split(",");
				/*
				 * subPartsLeft[0] = current state, subPartsLeft[1] =  input character, subPartsLeft[2] = the stack characters expected upon popping the Stack
				 */
				
				String stateName = subPartsLeft[0];			//the name of the state - check if it equals to the currentState
				if(stateName.equals(currentState)) {
					//get the character, expected popped stack chars -> destination state, chars to be pushed
					String[] destinationData = parts[1].split(",");
					String[] mapLeftData = {subPartsLeft[1], subPartsLeft[2]};			//we need only the input character, and the expected popped stack characters
					transitionsMap.put(mapLeftData, destinationData);
				}
			}
			
			//that' it - we have a piece of data --> add it to the set
			StateData toAdd = new StateData(currentState, acceptable, transitionsMap);
			statesData.put(currentState, toAdd);
		}
		
		//let's add transitions and states to the automaton!
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.addState(stateName, data.acceptable);
		}
		
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.bindTransitions(stateName, data.transitions);
		}
		
		ourAutomaton.setStartingState(startState);
		ourAutomaton.setStartingStackCharacter(startStackCharacter);

		//let's try reading the input... ughhh
		List<AlphabetChar> automatonInput = new ArrayList<>();
		for(List<String> inputSequence : inputSequences) {
			for(String alphChar : inputSequence) {
				AlphabetChar propAlphChar = new AlphabetChar(alphChar);
				automatonInput.add(propAlphChar);
			}
			
			//once all the chars are in - pass to automaton, execute the reading, and then clear the input
			//for another round!
			ourAutomaton.setInput(automatonInput);
			ourAutomaton.readInputSequence();
			automatonInput.clear();
			ourAutomaton.reset();
		}
		
		//TODO: change this later
//		System.out.print(ourAutomaton.SecondLabOutput());
		ourAutomaton.reset();
	}
	
	/**
	 * Represents data about a state
	 * @author kikyy99
	 *
	 */
	private static class StateData {
		
		/**
		 * Name of the state
		 */
		private String name;
		/**
		 * Whether the state is acceptable or not
		 */
		private boolean acceptable;
		/**
		 * Character, popped stack characters expected -> destination state, stack characters to be pushed 
		 */
		private Map<String[], String[]> transitions;
		
		
		public StateData(String name, boolean acceptable, Map<String[], String[]> transitions) {
			super();
			this.name = name;
			this.acceptable = acceptable;
			this.transitions = transitions;
		}
	}
	
}
