"""
Modells a Turing Machine and outputs its effects
"""
import sys

class Transition:
    """
    A helping class modelling a single transition
    """

    def __init__(self, from_state, input_char, to_state, output_char, movement):
        self.from_state = from_state
        self.input_char = input_char
        self.to_state = to_state
        self.output_char = output_char
        self.movement = movement

    def __hash__(self) -> int:
        return hash((self.from_state, self.input_char, self.to_state, self.output_char, self.movement))

    def __eq__(self, o: object) -> bool:
        """Override the default Equals behavior"""
        if not isinstance(o, self.__class__):
            return False

        # Time to test for our attributes - they all must be equal!
        if(self.from_state != o.from_state):
            return False
        elif(self.to_state != o.to_state):
            return False
        elif(self.movement != o.movement):
            return False
        elif(self.input_char != o.input_char):
            return False
        elif(self.output_char != o.output_char):
            return False
        else:
            return True

class TS:
    """
    Class representing a Turing Machine
    """
    LEFT_BOUND = 0
    RIGHT_BOUND = 69

    def __init__(self, states_set, input_alphabet_set, track_alphabet_set,
                 empty_cell_char, ts_tape, acceptable_states_set, starting_state,
                 starting_pos, transitions_input_set):
        """
        Initializes the Turing Machine. Parameters are self-explanatory

        :param states_set:
        :param input_alphabet_set:
        :param track_alphabet_set:
        :param empty_cell_char:
        :param ts_tape:
        :param acceptable_states_set:
        :param starting_state:
        :param starting_pos:
        :param transitions_input_set:
        """

        self.states_set = states_set
        self.input_alphabet_set = input_alphabet_set
        self.track_alphabet_set = track_alphabet_set
        self.empty_cell_char = empty_cell_char
        self.ts_tape = list(ts_tape)
        self.acceptable_states_set = acceptable_states_set
        self.starting_state = starting_state
        self.starting_pos = starting_pos

        self.transitions_set = set()

        # Let's model the transitions_set into transitions
        for transition in transitions_input_set:
            arrow_split = transition.split('->')
            left_side =  arrow_split[0].split(',')
            right_side = arrow_split[1].split(',')

            from_state = left_side[0]
            input_char = left_side[1]


            to_state = right_side[0]
            output_char = right_side[1]
            movement = right_side[2]

            to_add_transition = Transition(from_state, input_char, to_state, output_char, movement)
            self.transitions_set.add(to_add_transition)

    def run(self):
        """
        Runs the Turing Machine - also prints its output
        :return:
        """

        ts_output = ""

        # Let's begin with the algorithm - we have to get the
        current_pos = self.starting_pos
        current_state = self.starting_state
        while True:
            current_char = self.ts_tape[current_pos]

            # Let's get the right side of the transitions - OR None. In case of None abort the operation
            dest = self.find_transition(current_state, current_char)
            if dest is None:
                # End of the simulation
                break

            # Else we have a valid dest transition!
            to_state = dest['to_state']
            output_char = dest['output_char']
            mov = dest['mov']
            if current_pos == self.LEFT_BOUND and mov == 'L':       # If at start and need to move Left terminate
                break
            elif current_pos == self.RIGHT_BOUND and mov == 'R':        #If at the end and need to move Right terminate
                break

            # Let's not stop the sim just yet ehh!
            self.ts_tape[current_pos] = output_char
            current_state = to_state
            current_pos = current_pos - 1 if mov == 'L' else current_pos + 1

        ts_output += current_state + '|' + str(current_pos) + '|'
        for char in self.ts_tape:
            ts_output += char
        ts_output += '|'
        ts_output += '1' if current_state in self.acceptable_states_set else '0'
        print(ts_output)
        raise SystemExit            # Exits the program

    def find_transition(self, current_state, current_char) -> Transition:
        """
        Attempts to find the Transition destinations and returns the new state - or returns None
        :param current_state:
        :param current_char:
        :return: Transition destinations or None
        """

        for transition in self.transitions_set:
            from_state = transition.from_state
            input_char = transition.input_char
            if from_state == current_state and input_char == current_char:
                return  {"to_state": transition.to_state,
                         "output_char": transition.output_char,
                         "mov": transition.movement}

        return None

####################################        Main Program here       ####################################################
##### Let's parse the input

# # First row - States set
# input_string = input()
# states_set = set(input_string.split(','))
#
# # Second row - Input alphabet
# input_string = input()
# input_alphabet_set = set(input_string.split(','))
#
# # Third row - Track alphabet
# input_string = input()
# track_alphabet_set = set(input_string.split(','))
#
# # Fourth row - Empty cell
# empty_cell_char = input()
#
# # Fifth row - TS Tape
# ts_tape = input()
#
# # Sixth row - acceptable states
# input_string = input()
# acceptable_states_set = set(input_string.split(','))
#
# # Seventh row - starting state
# starting_state = input()
#
# # Eighth row - starting position of the TS Head
# starting_pos = int(input())
#
# ############## Nineth row+ - transmissions ##############
# transmissions_input_set = set()
#
# # Need to get each line and separate them - while loop
# line = input()
# while line != EOFError:
#     transmissions_input_set.add(line)
#     line = input()

##########################      ALTERNATIVE INPUT       ##########################
def add_states():
    global states_set
    states_set = set(line.split(','))

def add_input_alph():
    global input_alphabet_set
    input_alphabet_set = set(line.split(','))

def add_track_alph():
    global track_alphabet_set
    track_alphabet_set = set(line.split(','))

def add_empty_cell_char():
    global empty_cell_char
    empty_cell_char = line

def add_ts_tape():
    global ts_tape
    ts_tape = line

def add_acceptable_states():
    global acceptable_states_set
    acceptable_states_set = set(line.split(','))

def add_starting_state():
    global starting_state
    starting_state = line

def add_starting_pos():
    global starting_pos
    starting_pos = int(line)

def add_transition():
    global transmissions_input_set
    transmissions_input_set.add(line)

current_row = 0
transmissions_input_set = set()
for line in sys.stdin:
    line = line.rstrip()
    switcher = {
        0: add_states,
        1: add_input_alph,
        2: add_track_alph,
        3: add_empty_cell_char,
        4: add_ts_tape,
        5: add_acceptable_states,
        6: add_starting_state,
        7: add_starting_pos
    }
    func = switcher.get(current_row, add_transition)
    current_row += 1
    func()
##########################      ALTERNATIVE INPUT       ##########################

# Now time to construct the ts and run it!
turing_machine = TS(states_set, input_alphabet_set, track_alphabet_set,
                 empty_cell_char, ts_tape, acceptable_states_set, starting_state,
                 starting_pos, transmissions_input_set)
turing_machine.run()
