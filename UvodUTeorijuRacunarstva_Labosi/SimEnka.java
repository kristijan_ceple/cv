import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/*
Example program:

a,b,c,b,a,d
s1,s2,s3,s4,s5,s6,s7
a,b,c,d
s2,s5
s1
s1,$->s3,s6,s7
s1,a->s1,s3
s1,b->s5
s1,c->s3
s1,d->s3
s2,$->s2
s2,a->s3
s2,b->s1,s5
s2,c->s2,s2
s2,d->s4
s3,$->s3
s3,a->#
s3,b->s2,s3
s3,c->s4,s5
s3,d->s1
s4,a->s4
s4,b->s1,s4
s4,c->s1,s5
s5,a->s1
s5,b->s5
*/

/**
 * Simulates an Epsilon-transition Non-deterministic Finite Automaton
 */
public class SimEnka {
	/**
	 * Epsilon transition - used to reach acceptable states at the end of input
	 */
	private static final AlphabetChar EPSILON = new AlphabetChar("$");
	private static final String EMPTY_STATE_STRING = "#";

	private static class AlphabetChar {
		
		private String value;

		public AlphabetChar(String value) {
			Objects.requireNonNull(value);
			
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AlphabetChar other = (AlphabetChar) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value;
		}
		
	}
	
	/**
	 * Features the automaton's states. Those states feature
	 * their own transitions stored internally.
	 * 
	 * @author kikyy99
	 *
	 */
	private static class Automaton {
		
		/**
		 * Default empty state
		 */
		private State EMPTY_STATE = new State("#", false);
		private Set<State> states =  new TreeSet<>();
		private Set<State> visitedStates = new LinkedHashSet<>();
		private State startingState;
		private Set<State> activeStates = new TreeSet<>();
		private Set<AlphabetChar> alphabet = new LinkedHashSet<>();
		private List<AlphabetChar> input = new ArrayList<>();
		
		
		public Automaton(Set<AlphabetChar> alphabet) {
			Objects.requireNonNull(alphabet);
			
			this.alphabet.addAll(alphabet);
		}
		
		private State getState(String find) {
			if(find.equals(EMPTY_STATE_STRING)) {
				return EMPTY_STATE;
			}
			
			for(State currState : states) {
				if(currState.name.equals(find)) {
					return currState;
				}
			}
			
			return null;
		}
		
		public void setStartingState(String name) {
			Objects.requireNonNull(name);
			
			State startingState = this.getState(name);
			this.startingState = startingState;
			this.activeStates.add(startingState);
			this.visitedStates.add(startingState);
		}
		
		private void setInput(List<AlphabetChar> input) {
			this.input.addAll(input);
		}
		
		private void addState(String name, boolean acceptedState) {
			State toAdd = new State(name, acceptedState);
			states.add(toAdd);
		}
		
		private void bindTransitions(String stateName, Map<String, String[]> transitions) {
			State state = this.getState(stateName);
			state.addTransitions(transitions);
		}
		
//		private void addStates(Map<String, List<Boolean, Map<String, State[]>>> statesMap) {
//			for(Entry<String, Boolean> entry : statesMap.entrySet()) {
//				String name = entry.getKey();
//				Boolean acceptedState= entry.getValue();
//				
//				this.addState(name, acceptedState);
//			}
//		}
		
		/**
		 * @return false if no more input chars left to be processed,
		 * true otherwise
		 */
		private void readInputSequence() {
			StringBuilder sb = new StringBuilder();
			sb.append(activeStatesWithEpsilonEnv());
			for(int i = 0; i < input.size(); i++) {
				sb.append("|");
				readInputChar(i);
				if(activeStates.size()>=2) {
					//in case an empty state still made it into the set,
					//but there are more than 2 states in the set itself
					activeStates.remove(EMPTY_STATE);
				}
				sb.append(activeStatesWithEpsilonEnv());
			}
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
			toPrint = toPrint.replaceAll("\\s*", "");
			System.out.println(toPrint);
		}
		
		private Set<State> activeStatesWithEpsilonEnv() {
			Set<State> activePlusEpsilon = new TreeSet<>();
			
			for(State currentState : activeStates) {
				activePlusEpsilon.addAll(currentState.epsilonEnvironment());
			}
			
			if(activePlusEpsilon.size()>=2) {
				// if there is an empty state here, IT IS NOT ALONE, SO REMOVE IT
				activePlusEpsilon.remove(EMPTY_STATE);
			}
			
			return activePlusEpsilon;
		}
		
		private boolean readInputChar(int nextIndex) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			if(nextIndex >= input.size()) {
				return false;
			}
			
			AlphabetChar currentChar = input.get(nextIndex);
			Set<State> epsilonEnv = new HashSet<>();
			
			//get the epsilon-env of all the former states
			for(State state : activeStates) {
				epsilonEnv.addAll(state.epsilonEnvironment());
			}

			//first remove all current states
			activeStates.clear();
			//perform transitions for ALL the states located in the epsilon env
			for(State state : epsilonEnv) {
				state.performTransition(currentChar);
			}
			
			//now find the epsilon environment of all the newfound active states, mark them all as active
			Set<State> activeWithEpsilons = new HashSet<>(activeStates);
			for(State state : activeWithEpsilons) {
				activeStates.addAll(state.epsilonEnvironment());
			}
			
			return true;
		}
		
		public void reset() {
			this.activeStates.clear();
			this.activeStates.add(startingState);
			this.input.clear();
		}
		
//		public String visitedStates() {
//			StringBuilder sb = new StringBuilder();
//			
//			sb.append(visitedStates);
//			sb.append("|");
//			
//			visitedStates.clear();
//			return sb.toString();
//		}

		/**
		 * Models a SINGLE transition - therefore it MUST have an input, 
		 * and it MUST have at least one target state. It SHALL not be used
		 * to model transitions to the {@link EMPTY_STATE} or model transitions
		 * that have no input(input is null). 
		 * 
		 * @author kikyy99
		 *
		 */
		private class State implements Comparable<State>{
			
			private String name;
			private final boolean acceptedState;
			private Set<Transition> transitions = new HashSet<>();
			private boolean active;
			private AlphabetChar lastCharacter;
			
			public State(String name, boolean acceptedState, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we have some transition(s) from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			public State(String name, boolean acceptedState) {
				this(name, acceptedState, (Transition[])null);
			}
			
			public void addTransition(String input, String[] destinationStates) {
				Objects.requireNonNull(destinationStates);
				
				Transition toAdd = new Transition(input, destinationStates); 
				transitions.add(toAdd);
			}
			
			public void addTransitions(Map<String, String[]> transitions) {
				Objects.requireNonNull(transitions);
				
				for(Entry<String, String[]> entry : transitions.entrySet()) {
					String input = entry.getKey();
					String[] states = entry.getValue();
					
					Objects.requireNonNull(input);
					Objects.requireNonNull(states);
					if(states.length == 0) {
						throw new IllegalArgumentException("Array of size 0 passed as argument to the "
								+ "addTransitions method!");
					}
					
					Transition toAdd = new Transition(input, states);
					this.transitions.add(toAdd);
				}
			}
			
			private Set<State> epsilonEnvironment(){
				Set<State> epsilonEnvironment = new HashSet<>();
				epsilonEnvironment.add(this);
				//let's check if this contains any epsilon transitions, and if it does
				//add those states to the epsilon env. Repeat recursively
				
				//recursive checking - do it until the two sets are equal
				Set<State> newEpsilonEnv = new HashSet<>(epsilonEnvironment);
				boolean equals;
				do {
					equals = true;
					for(State state : epsilonEnvironment) {
						for(Transition transition : state.transitions) {
							if(transition.input.equals(EPSILON)) {
								equals = false;
								newEpsilonEnv.addAll(transition.targetStates);
								visitedStates.addAll(transition.targetStates);
							}
						}
					}
					
					if(newEpsilonEnv.equals(epsilonEnvironment)) {
						break;
					}
					epsilonEnvironment.addAll(newEpsilonEnv);
				} while(!equals);

				return epsilonEnvironment;
			}
			
			/**
			 * Performs a transition
			 * 
			 * @param currentChar the input alphabet char
			 * @return true if transition has been found, false otherwise
			 */
			private boolean performTransition(AlphabetChar currentChar) {
				boolean matches = false;
				Transition matchedTrans = null;
				for(State.Transition currentTransition : transitions) {
					if(currentTransition.input.equals(currentChar)) {
						matches = true;
						matchedTrans = currentTransition;
						break;
					}
				}

				this.active = false;
				this.lastCharacter = currentChar;
				//okay, time to do the algo part
				if(matchedTrans == null && !activeStates.isEmpty()) {
					//one of states went into nothing - nothing too bad
					return false;
				}
				
				if(!matches) {
					//not matched and no active states, go to empty state.
					activeStates.add(Automaton.this.EMPTY_STATE);
					Automaton.this.EMPTY_STATE.active = false;
					Automaton.this.visitedStates.add(EMPTY_STATE);
					return false;
				}
				
				//else we have a normal state
				Set<State> nextStates = matchedTrans.targetStates;
				for(State currentState : nextStates) {
					currentState.active = true;
					visitedStates.add(currentState);
					activeStates.add(currentState);
				}
				
				//that's it. 
				return true;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((name == null) ? 0 : name.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				State other = (State) obj;
				if (name == null) {
					if (other.name != null)
						return false;
				} else if (!name.equals(other.name))
					return false;
				return true;
			}
			
			@Override
			public int compareTo(State o) {
				return this.name.compareTo(o.name);
			}
			
			@Override
			public String toString() {
				return name;
			}

			private class Transition {
				
				/**
				 * The input character
				 */
				private AlphabetChar input;
				/**
				 * ALL THE TARGET STATES - they MUST be UNIQUE
				 */
				private Set<State> targetStates = new HashSet<>();
				
				/**
				 * Constructor.
				 * @param input
				 * @param targetStates
				 */
				private Transition(String input, String... targetStates) {
					Objects.requireNonNull(input);
					Objects.requireNonNull(targetStates);

					this.input = new AlphabetChar(input);
					//add all the elements from our array to this set
					for(String currentState : targetStates) {
						//search the automaton for the wanted state;
						State toAdd = Automaton.this.getState(currentState);
						this.targetStates.add(toAdd);
					}
				}

				@Override
				public int hashCode() {
					final int prime = 31;
					int result = 1;
					result = prime * result + ((input == null) ? 0 : input.hashCode());
					result = prime * result + ((targetStates == null) ? 0 : targetStates.hashCode());
					return result;
				}

				@Override
				public boolean equals(Object obj) {
					if (this == obj)
						return true;
					if (obj == null)
						return false;
					if (getClass() != obj.getClass())
						return false;
					Transition other = (Transition) obj;
					if (input == null) {
						if (other.input != null)
							return false;
					} else if (!input.equals(other.input))
						return false;
					if (targetStates == null) {
						if (other.targetStates != null)
							return false;
					} else if (!targetStates.equals(other.targetStates))
						return false;
					return true;
				}
				
				@Override
				public String toString() {
					String toReturn = String.format("&(%s, %s) = %s", State.this, input, targetStates);
					toReturn.replace('[', '{');
					toReturn.replace(']', '}');
					
					return toReturn;
				}
			}
		}
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		//let's get parsing
		List<List<String>> inputSequences;
		Set<String> states;
		Set<String> alphabet;
		Set<String> acceptableStates;
		String startState;
		Set<String> transitions;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
		//first row - state sequences
		String input = reader.readLine();
		String[] parts = input.split("\\|");
		inputSequences = new ArrayList<>(parts.length);
		for(int i = 0; i < parts.length; i++) {
			//now must split by commas
			String currentString = parts[i];
			String[] subParts = currentString.split(",");
			List<String> subList = new ArrayList<String>(subParts.length);
			for(String part : subParts) {
				part = part.trim();
				subList.add(part);
			}
			inputSequences.add(subList);
		}
		
		//second row - states set
		input = reader.readLine();
		parts = input.split(",");
		states = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			states.add(part);
		}
		
		//third row - alphabets set
		input = reader.readLine();
		parts = input.split(",");
		alphabet = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			alphabet.add(part);
		}
		
		//fourth row -  acceptable states set
		input = reader.readLine();
		parts = input.split(",");
		acceptableStates = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			acceptableStates.add(part);
		}
		
		//fifth row - starting state
		startState = reader.readLine();
		startState = startState.trim();
		states.add(startState);
		
		//sixth row+ - transitions
		//TODO: kasnije promjeni ovaj dio
		transitions = new LinkedHashSet<>();

//		input = reader.readLine();
//		while(!input.equals("kraj")) {
//			input = input.trim();
//			transitions.add(input);
//			input = reader.readLine();
//		}
		
		input = reader.readLine();
		while(input != null) {
			input = input.trim();
			transitions.add(input);
//			Thread.sleep(10);
			input = reader.readLine();
		}
	
		//Let's construct an automaton!
		//Add the alphabet
		Set<AlphabetChar> alphabetSet = new HashSet<>();
		for(String alphabetCharacter : alphabet) {
			AlphabetChar toAdd = new AlphabetChar(alphabetCharacter);
			alphabetSet.add(toAdd);
		}
		Automaton ourAutomaton = new Automaton(alphabetSet);
		
		//Make the StateData array featuring data about all the states
		Map<String, StateData> statesData = new HashMap<String, StateData>();
		for(String currentState : states) {
			//let's see if it's acceptable
			boolean acceptable = false;
			if(acceptableStates.contains(currentState)) {
				acceptable = true;
			}
			
			//need to go through all the transitions, and make the map
			Map<String, String[]> transitionsMap = new HashMap<>();
			for(String transition : transitions) {
				transition.replaceAll("\\s", "");
				parts = transition.split("->");
				String[] subPartsLeft = parts[0].split(",");
				
				String stateName = subPartsLeft[0];			//the name of the state - check if it equals to the currentState
				if(stateName.equals(currentState)) {
					//get the character -> alfChar and the destination states
					String character = subPartsLeft[1];
					String[] destinationStates = parts[1].split(",");
					transitionsMap.put(character, destinationStates);
				}
			}
			
			//that' it - we have a piece of data --> add it to the set
			StateData toAdd = new StateData(currentState, acceptable, transitionsMap);
			statesData.put(currentState, toAdd);
		}
		
		//let's add transitions and states to the automaton!
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.addState(stateName, data.acceptable);
		}
		
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.bindTransitions(stateName, data.transitions);
		}
		
		ourAutomaton.setStartingState(startState);
		
		//let's try reading the input... ughhh
		List<AlphabetChar> automatonInput = new ArrayList<>();
		for(List<String> inputSequence : inputSequences) {
			for(String alphChar : inputSequence) {
				AlphabetChar propAlphChar = new AlphabetChar(alphChar);
				automatonInput.add(propAlphChar);
			}
			
			//once all the chars are in - pass to automaton, execute the reading, and then clear the input
			//for another round!
			ourAutomaton.setInput(automatonInput);
			ourAutomaton.readInputSequence();
			automatonInput.clear();
			ourAutomaton.reset();
		}
	}
	
	private static class StateData {
		
		private String name;
		private boolean acceptable;
		private Map<String, String[]> transitions;
		public StateData(String name, boolean acceptable, Map<String, String[]> transitions) {
			super();
			this.name = name;
			this.acceptable = acceptable;
			this.transitions = transitions;
		}
	}
	
}
