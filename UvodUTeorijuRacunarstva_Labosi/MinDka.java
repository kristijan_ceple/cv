import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/*
Example program:

p1,p2,p3,p4,p5,p6,p7
c,d
p5,p6,p7
p1
p1,c->p6
p1,d->p3
p2,c->p7
p2,d->p3
p3,c->p1
p3,d->p5
p4,c->p4
p4,d->p6
p5,c->p7
p5,d->p3
p6,c->p4
p6,d->p1
p7,c->p4
p7,d->p2

Second program:
1,2,3,4,5
a,b,c
2,3,4
1
1,a->1
1,b->1
1,c->1
2,a->1
2,b->2
2,c->2
3,a->1
3,b->3
3,c->2
4,a->2
4,b->1
4,c->3
5,a->3
5,b->4
5,c->1
*/

/**
 * For input takes a definition of a Deterministic finite automaton, and then minimizes it.
 * Outputs this minimal DFA to terminal.
 */
public class MinDka {
	/**
	 * Epsilon transition - used to reach acceptable states at the end of input
	 */
	private static final AlphabetChar EPSILON = new AlphabetChar("$");
	private static final String EMPTY_STATE_STRING = "#";

	private static class AlphabetChar implements Comparable<AlphabetChar>{
		
		private String value;

		public AlphabetChar(String value) {
			Objects.requireNonNull(value);
			
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AlphabetChar other = (AlphabetChar) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value;
		}

		@Override
		public int compareTo(AlphabetChar o) {
			return this.value.compareTo(o.value);
		}
		
	}
	
	/**
	 * Features the automaton's states. Those states feature
	 * their own transitions stored internally.
	 * 
	 * @author kikyy99
	 *
	 */
	private static class Automaton {
		
		/**
		 * Default empty state
		 */
		private State EMPTY_STATE = new State("#", false);
		private Set<State> states =  new TreeSet<>();
		private Set<State> visitedStates = new LinkedHashSet<>();
		private State startingState;
		private Set<State> activeStates = new TreeSet<>();
		private Set<AlphabetChar> alphabet = new TreeSet<>();
		private List<AlphabetChar> input = new ArrayList<>();
		
		
		public Automaton(Set<AlphabetChar> alphabet) {
			Objects.requireNonNull(alphabet);
			
			this.alphabet.addAll(alphabet);
		}
		
		private State getState(String find) {
			if(find.equals(EMPTY_STATE_STRING)) {
				return EMPTY_STATE;
			}
			
			for(State currState : states) {
				if(currState.name.equals(find)) {
					return currState;
				}
			}
			
			return null;
		}
		
		/**
		 * Returns a NEW SET of acceptable states
		 * 
		 * @return a new set of acceptable states
		 */
		public Set<State> getAcceptableStates() {
			return retrieveSpecifiedAcceptablenessStates(true);
		}
		
		/**
		 * Returns a NEW SET of unacceptable states
		 * 
		 * @return a new set of unacceptable states
		 */
		public Set<State> getUnacceptableStates() {
			return retrieveSpecifiedAcceptablenessStates(false);
		}
		
		private Set<State> retrieveSpecifiedAcceptablenessStates(boolean acceptable) {
			Set<State> returnStates = new HashSet<>();
			
			for(State state : states) {
				if(state.acceptedState && acceptable) {
					returnStates.add(state);
				} else if(!state.acceptedState && !acceptable) {
					returnStates.add(state);
				}
			}
			
			return returnStates;
		}
		
		public String SecondLabOutput() {
			StringBuilder sb = new StringBuilder();
			
			String statesString = states.toString();
			statesString = statesString.replaceAll("\\s*", "");
			sb.append(statesString);
			sb.append(System.lineSeparator());
			
			String alphabetString = alphabet.toString();
			alphabetString = alphabetString.replaceAll("\\s*", "");
			sb.append(alphabetString);
			sb.append(System.lineSeparator());
			
			// Acceptable states
			String acceptableStatesString = getAcceptableStates().toString();
			acceptableStatesString = acceptableStatesString.replaceAll("\\s*", "");
			sb.append(acceptableStatesString);
			sb.append(System.lineSeparator());
			
			sb.append(startingState);
			
			//Transitions
			for(State state : states) {
				for(MinDka.Automaton.State.Transition transition : state.transitions) {
					sb.append(System.lineSeparator());
					sb.append(transition.lab2String());
				}
			}
			
			sb.append(System.lineSeparator());
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
//			toPrint = toPrint.replaceAll("\\s*", "");
			
			return toPrint;
		}
		
		public void setStartingState(String name) {
			Objects.requireNonNull(name);
			
			State startingState = this.getState(name);
			this.startingState = startingState;
			this.activeStates.add(startingState);
			this.visitedStates.add(startingState);
		}
		
		private void setInput(List<AlphabetChar> input) {
			this.input.addAll(input);
		}
		
		private void addState(String name, boolean acceptedState) {
			State toAdd = new State(name, acceptedState);
			states.add(toAdd);
		}
		
		private void bindTransitions(String stateName, Map<String, String[]> transitions) {
			State state = this.getState(stateName);
			state.addTransitions(transitions);
		}
		
//		private void addStates(Map<String, List<Boolean, Map<String, State[]>>> statesMap) {
//			for(Entry<String, Boolean> entry : statesMap.entrySet()) {
//				String name = entry.getKey();
//				Boolean acceptedState= entry.getValue();
//				
//				this.addState(name, acceptedState);
//			}
//		}
		
		/**
		 * @return false if no more input chars left to be processed,
		 * true otherwise
		 */
		private void readInputSequence() {
			StringBuilder sb = new StringBuilder();
			sb.append(activeStatesWithEpsilonEnv());
			for(int i = 0; i < input.size(); i++) {
				sb.append("|");
				readInputChar(i);
				if(activeStates.size()>=2) {
					//in case an empty state still made it into the set,
					//but there are more than 2 states in the set itself
					activeStates.remove(EMPTY_STATE);
				}
				sb.append(activeStatesWithEpsilonEnv());
			}
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
			toPrint = toPrint.replaceAll("\\s*", "");
			System.out.println(toPrint);
		}
		
		private Set<State> activeStatesWithEpsilonEnv() {
			Set<State> activePlusEpsilon = new TreeSet<>();
			
			for(State currentState : activeStates) {
				activePlusEpsilon.addAll(currentState.epsilonEnvironment());
			}
			
			if(activePlusEpsilon.size()>=2) {
				// if there is an empty state here, IT IS NOT ALONE, SO REMOVE IT
				activePlusEpsilon.remove(EMPTY_STATE);
			}
			
			return activePlusEpsilon;
		}
		
		private boolean readInputChar(int nextIndex) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			if(nextIndex >= input.size()) {
				return false;
			}
			
			AlphabetChar currentChar = input.get(nextIndex);
			Set<State> epsilonEnv = new HashSet<>();
			
			//get the epsilon-env of all the former states
			for(State state : activeStates) {
				epsilonEnv.addAll(state.epsilonEnvironment());
			}

			//first remove all current states
			activeStates.clear();
			//perform transitions for ALL the states located in the epsilon env
			for(State state : epsilonEnv) {
				state.performTransition(currentChar);
			}
			
			//now find the epsilon environment of all the newfound active states, mark them all as active
			Set<State> activeWithEpsilons = new HashSet<>(activeStates);
			for(State state : activeWithEpsilons) {
				activeStates.addAll(state.epsilonEnvironment());
			}
			
			return true;
		}
		
		public void reset() {
			this.activeStates.clear();
			this.activeStates.add(startingState);
			this.input.clear();
		}
		
		public Set<State> reachableStates() {
			Set<State> reachableStates = new TreeSet<>();
			Set<State> comparatorySet = new HashSet<>();
			
			comparatorySet = dkaTransitionStates(startingState);
			comparatorySet.add(startingState);
			do {
				reachableStates.addAll(comparatorySet);
				
				for(State state : reachableStates) {
					comparatorySet.addAll(dkaTransitionStates(state));
				}
			} while(!comparatorySet.equals(reachableStates));

			//let's remove the unreachable states
			Set<State> statesIterationCopy = new HashSet<>(states); 
			for(State state : statesIterationCopy) {
				if(!reachableStates.contains(state)) {
					states.remove(state);
				}
			}
			
			return reachableStates;
		}
		
		public Collection<TreeSet<State>> equivalentStates() {
			Map<String, TreeSet<State>> oldStates = new TreeMap<>();
			Map<String, TreeSet<State>> newStates = new TreeMap<>();
			
			newStates.put("G11", new TreeSet<>(this.getAcceptableStates()));
			newStates.put("G12", new TreeSet<>(this.getUnacceptableStates()));
			
//			Set<Set<State>> oldStatesValues = new TreeSet<>();
//			Set<Set<State>> newStatesValues = new TreeSet<>();
//			oldStatesValues.addAll(newStates.values());
			
			//now we do the generic loop algorithm
			int i = 2;
			while(!checkMapValuesEquality(oldStates, newStates)) {
				//this inner while loop shall do the groups generation
				oldStates.clear();
				oldStates.putAll(newStates);			//Prepare for another round
				newStates.clear();
				int j = 1;
//				Set<State> untestedStates = new HashSet<>();
				for(TreeSet<State> statesGroup : oldStates.values()) {
					//test states for equivalence, and put them into a new group
					//unassigned states go into the untestedStates Set
					j = hopcroftAlgo(statesGroup, newStates, oldStates, i, j);		//on to the next group!
				}
				
				i++;
			}
			
			return newStates.values();
		}
		
		/**
		 * Recursive method
		 * 
		 * @param currentSet the current group being processed
		 * @param newStates the newStates set to which new groups are being added
		 * @return newStates Set
		 */
		private int hopcroftAlgo(TreeSet<State> currentSet, Map<String, TreeSet<State>> newStates, Map<String, TreeSet<State>> oldStates, int i, int j) {
			if(currentSet.size() == 1) {
				newStates.put("G" + i + j, currentSet);
				return j+1;			//base case - end of algo
			}
			
			Iterator<State> iter = currentSet.iterator();
			
			State comparator;
			if(iter.hasNext()) {
				comparator = iter.next();
			} else {
				//empty group - j not filled
				return j;
			}
			
			TreeSet<State> newGroup = new TreeSet<>();
			TreeSet<State> toProcessFurtherly = new TreeSet<>();
			
			newGroup.add(comparator);
			//compare other states to this one
			while(iter.hasNext()) {
				State comparand = iter.next();
				if(comparator.checkIfAlignedWith(comparand, oldStates)) {
					newGroup.add(comparand);
				} else {
					toProcessFurtherly.add(comparand);
				}
			}
			
			//add the new formed group to the newStates map, increase i and j
			//and repeat the algo recursively for the Set toProcessFurtherly, just as their name implies
			newStates.put("G" + i + j, newGroup);
			return hopcroftAlgo(toProcessFurtherly, newStates, oldStates, i, j+1);
		}
				
		private Set<State> dkaTransitionStates(State testedState){
			Set<State> transitionStates = new HashSet<>();
			
			for(MinDka.Automaton.State.Transition transition : testedState.transitions) {
				transitionStates.addAll(transition.targetStates);
			}
			
			return transitionStates;
		}
		
		private void reduceUnreachableStates(Set<State> reachableStates) {
			Set<State> statesIterationCopy = new HashSet<>(states);
			for(State state : statesIterationCopy) {
				if(!reachableStates.contains(state)) {
					//unreachable state - remove it
					states.remove(state);
				}
			}
		}
		
		/**
		 * Eliminates the redundant transitions, and simplifies the equivalent states
		 * 
		 * @param targetStates targets
		 * @param equivalentStates a collection of equivalent state sets
		 */
		private Set<State> eliminateTransitionTargetEquivalentStates(Set<State> targetStates, Collection<TreeSet<State>> equivalentStates) {
			
			Set<State> newTargetStates = new TreeSet<>();
			
			for(State state : targetStates) {
				/*
				 * See if the state is located in any of the equivalent groups
				 * and is not the first lexicographically in the equivalentStates.
				 * 
				 * If not first, substitute for the first State in the equivalentStates
				 * group.
				 * 
				 *  First, search EVERY equivalentState group and look in them for the specified
				 *  state. If located inside either one of them - 2 approaches are possible.
				 *  
				 *  a) Immediately once a group is located return the first state from that group. 
				 *  
				 *  b) First check if the state is equal to the first - if it is return that state itself,
				 *  and if not then return the first state from the groups set
				 *  
				 *  Think I'm just gonna return the first state either way
				*/
				
				State equivalentState = retrieveEquivalentState(state, equivalentStates);		//now we have the equiv. state
				newTargetStates.add(equivalentState);
			}
			
			return newTargetStates;
		}
		
		private State retrieveEquivalentState(State toCheck, Collection<TreeSet<State>> equivalentStates) {
			State equivalentState = null;
			for(TreeSet<State> statesGroup : equivalentStates) {
				if(statesGroup.contains(toCheck)) {
					return statesGroup.first();
				}
			}
			
			return equivalentState;
		}
		
		private void reduceEquivalentStates(Collection<TreeSet<State>> equivalentStates) {
			
			for(Set<State> statesGroup : equivalentStates) {
				//take the first state and keep it
				//take the rest, and remove them from the automaton

				Iterator<State> iter = statesGroup.iterator();
				iter.next();				//skip 1st elem

				while(iter.hasNext()) {
					State toRemove = iter.next();
					this.states.remove(toRemove);
				}
			}
			
			//have to do the same for the starting, and acceptable states
			//first the starting state
			this.startingState = retrieveEquivalentState(this.startingState, equivalentStates);
			
			//now have to go through all the remaining states and their transitions - 
			//and replace the equivalent destination states
			for(State state : states) {
				for(MinDka.Automaton.State.Transition transition : state.transitions) {
					Set<State> targetStates = transition.targetStates;
					transition.targetStates = eliminateTransitionTargetEquivalentStates(targetStates, equivalentStates);
				}
			}
		}
		
//		public String visitedStates() {
//			StringBuilder sb = new StringBuilder();
//			
//			sb.append(visitedStates);
//			sb.append("|");
//			
//			visitedStates.clear();
//			return sb.toString();
//		}

		/**
		 * Models a SINGLE transition - therefore it MUST have an input, 
		 * and it MUST have at least one target state. It SHALL not be used
		 * to model transitions to the {@link EMPTY_STATE} or model transitions
		 * that have no input(input is null). 
		 * 
		 * @author kikyy99
		 *
		 */
		private class State implements Comparable<State>{
			
			private String name;
			private final boolean acceptedState;
			private Set<Transition> transitions = new TreeSet<>();
			private boolean active;
			private AlphabetChar lastCharacter;
			
			public State(String name, boolean acceptedState, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we have some transition(s) from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			public State(String name, boolean acceptedState) {
				this(name, acceptedState, (Transition[])null);
			}
			
			public void addTransition(String input, String[] destinationStates) {
				Objects.requireNonNull(destinationStates);
				
				Transition toAdd = new Transition(input, destinationStates); 
				transitions.add(toAdd);
			}
			
			public void addTransitions(Map<String, String[]> transitions) {
				Objects.requireNonNull(transitions);
				
				for(Entry<String, String[]> entry : transitions.entrySet()) {
					String input = entry.getKey();
					String[] states = entry.getValue();
					
					Objects.requireNonNull(input);
					Objects.requireNonNull(states);
					if(states.length == 0) {
						throw new IllegalArgumentException("Array of size 0 passed as argument to the "
								+ "addTransitions method!");
					}
					
					Transition toAdd = new Transition(input, states);
					this.transitions.add(toAdd);
				}
			}
			
			private Set<State> epsilonEnvironment(){
				Set<State> epsilonEnvironment = new HashSet<>();
				epsilonEnvironment.add(this);
				//let's check if this contains any epsilon transitions, and if it does
				//add those states to the epsilon env. Repeat recursively
				
				//recursive checking - do it until the two sets are equal
				Set<State> newEpsilonEnv = new HashSet<>(epsilonEnvironment);
				boolean equals;
				do {
					equals = true;
					for(State state : epsilonEnvironment) {
						for(Transition transition : state.transitions) {
							if(transition.input.equals(EPSILON)) {
								equals = false;
								newEpsilonEnv.addAll(transition.targetStates);
								visitedStates.addAll(transition.targetStates);
							}
						}
					}
					
					if(newEpsilonEnv.equals(epsilonEnvironment)) {
						break;
					}
					epsilonEnvironment.addAll(newEpsilonEnv);
				} while(!equals);

				return epsilonEnvironment;
			}
			
			/**
			 * Performs a transition
			 * 
			 * @param currentChar the input alphabet char
			 * @return true if transition has been found, false otherwise
			 */
			private Set<State> performTransition(AlphabetChar currentChar) {
				boolean matches = false;
				Transition matchedTrans = null;
				Set<State> nextStates = new HashSet<>();
				
				for(State.Transition currentTransition : transitions) {
					if(currentTransition.input.equals(currentChar)) {
						matches = true;
						matchedTrans = currentTransition;
						break;
					}
				}

				this.active = false;
				this.lastCharacter = currentChar;
				//okay, time to do the algo part
				if(matchedTrans == null && !activeStates.isEmpty()) {
					//one of states went into nothing - nothing too bad
					nextStates.add(EMPTY_STATE);
					return nextStates;
				}
				
				if(!matches) {
					//not matched and no active states, go to empty state.
					activeStates.add(Automaton.this.EMPTY_STATE);
					Automaton.this.EMPTY_STATE.active = false;
					Automaton.this.visitedStates.add(EMPTY_STATE);
					nextStates.add(EMPTY_STATE);
					return nextStates;
				}
				
				//else we have a normal state
				nextStates = matchedTrans.targetStates;
				for(State currentState : nextStates) {
					currentState.active = true;
					visitedStates.add(currentState);
					activeStates.add(currentState);
				}
				
				//that's it. 
				return nextStates;
			}
			
			/**
			 * Checks alignment - the states must for every
			 * transition be located within the same group in the map 
			 * 
			 * @param state2 the state alignment is being checked for
			 * @return true or false
			 */
			public boolean checkIfAlignedWith(State state2, Map<String, TreeSet<State>> oldStates) {
				for(AlphabetChar currentChar : alphabet) {
					State state3 = this.performTransition(currentChar).iterator().next();
					State state4 = state2.performTransition(currentChar).iterator().next();
					
					//now need to check if they are within the same set in the oldStates
					for(Set<State> statesGroup : oldStates.values()) {
						Boolean aligned;
						
						if(statesGroup.contains(state3) && !statesGroup.contains(state4)
								|| !statesGroup.contains(state3) && statesGroup.contains(state4)) {
							return false;			//they are obviously not aligned
						}
					}
					
					return true;	//got through the check - they're aligned
				}
				
				return false;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((name == null) ? 0 : name.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				State other = (State) obj;
				if (name == null) {
					if (other.name != null)
						return false;
				} else if (!name.equals(other.name))
					return false;
				return true;
			}
			
			@Override
			public int compareTo(State o) {
				return this.name.compareTo(o.name);
			}
			
			@Override
			public String toString() {
				return name;
			}

			private class Transition implements Comparable<Transition>{
				
				/**
				 * The input character
				 */
				private AlphabetChar input;
				/**
				 * ALL THE TARGET STATES - they MUST be UNIQUE
				 */
				private Set<State> targetStates = new HashSet<>();
				
				/**
				 * Constructor.
				 * @param input
				 * @param targetStates
				 */
				private Transition(String input, String... targetStates) {
					Objects.requireNonNull(input);
					Objects.requireNonNull(targetStates);

					this.input = new AlphabetChar(input);
					//add all the elements from our array to this set
					for(String currentState : targetStates) {
						//search the automaton for the wanted state;
						State toAdd = Automaton.this.getState(currentState);
						this.targetStates.add(toAdd);
					}
				}
				
				private State getOuterClassInstance() {
					return State.this;
				}

				@Override
				public int hashCode() {
					final int prime = 31;
					int result = 1;
					result = prime * result + ((input == null) ? 0 : input.hashCode());
					result = prime * result + ((targetStates == null) ? 0 : targetStates.hashCode());
					return result;
				}

				@Override
				public boolean equals(Object obj) {
					if (this == obj)
						return true;
					if (obj == null)
						return false;
					if (getClass() != obj.getClass())
						return false;
					Transition other = (Transition) obj;
					if (input == null) {
						if (other.input != null)
							return false;
					} else if (!input.equals(other.input))
						return false;
					if (targetStates == null) {
						if (other.targetStates != null)
							return false;
					} else if (!targetStates.equals(other.targetStates))
						return false;
					return true;
				}
				
				public String lab2String() {
					String toReturn = String.format("%s,%s->%s", State.this, input, targetStates);
					toReturn.replace("[", "");
					toReturn.replace("]", "");
					
					return toReturn;
				}
				
				@Override
				public String toString() {
					String toReturn = String.format("&(%s, %s) = %s", State.this, input, targetStates);
					toReturn.replace('[', '{');
					toReturn.replace(']', '}');
					
					return toReturn;
				}

				@Override
				/**
				 * The comparison and sorting shall be done in the the lexicographical 
				 * order of AlphabetChar inputs of these 2 Transitions
				 * 
				 * @param o the Other transition
				 * @return -1, 0, or 1 depending on the inputs' compareTo method
				 */
				public int compareTo(Transition o) {
					return this.input.compareTo(o.input);
				}
			}
		}
	}
	
	public static <K, V> boolean checkMapValuesEquality(Map<K, V> first, Map<K, V> second) {
		HashSet<V> set1 = new HashSet<>(first.values());
		HashSet<V> set2 = new HashSet<>(second.values());
		
		return set1.equals(set2);
		
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		//let's get parsing
		List<List<String>> inputSequences;
		Set<String> states;
		Set<String> alphabet;
		Set<String> acceptableStates;
		String startState;
		Set<String> transitions;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
		//first row - state sequences
//		String input = reader.readLine();
//		String[] parts = input.split("\\|");
//		inputSequences = new ArrayList<>(parts.length);
//		for(int i = 0; i < parts.length; i++) {
//			//now must split by commas
//			String currentString = parts[i];
//			String[] subParts = currentString.split(",");
//			List<String> subList = new ArrayList<String>(subParts.length);
//			for(String part : subParts) {
//				part = part.trim();
//				subList.add(part);
//			}
//			inputSequences.add(subList);
//		}
		
		//second row - states set
		String input = reader.readLine();
		String[] parts = input.split(",");
		states = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			states.add(part);
		}
		
		//third row - alphabets set
		input = reader.readLine();
		parts = input.split(",");
		alphabet = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			alphabet.add(part);
		}
		
		//fourth row -  acceptable states set
		input = reader.readLine();
		parts = input.split(",");
		acceptableStates = new HashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			acceptableStates.add(part);
		}
		
		//fifth row - starting state
		startState = reader.readLine();
		startState = startState.trim();
		states.add(startState);
		
		//sixth row+ - transitions
		//TODO: kasnije promjeni ovaj dio
		transitions = new HashSet<>();
		
		input = reader.readLine();
		while(input != null) {
			input = input.trim();
			transitions.add(input);
			input = reader.readLine();
		}
	
		//Let's construct an automaton!
		//Add the alphabet
		Set<AlphabetChar> alphabetSet = new HashSet<>();
		for(String alphabetCharacter : alphabet) {
			AlphabetChar toAdd = new AlphabetChar(alphabetCharacter);
			alphabetSet.add(toAdd);
		}
		Automaton ourAutomaton = new Automaton(alphabetSet);
		
		//Make the StateData array featuring data about all the states
		Map<String, StateData> statesData = new HashMap<String, StateData>();
		for(String currentState : states) {
			//let's see if it's acceptable
			boolean acceptable = false;
			if(acceptableStates.contains(currentState)) {
				acceptable = true;
			}
			
			//need to go through all the transitions, and make the map
			Map<String, String[]> transitionsMap = new HashMap<>();
			for(String transition : transitions) {
				transition.replaceAll("\\s", "");
				parts = transition.split("->");
				String[] subPartsLeft = parts[0].split(",");
				
				String stateName = subPartsLeft[0];			//the name of the state - check if it equals to the currentState
				if(stateName.equals(currentState)) {
					//get the character -> alfChar and the destination states
					String character = subPartsLeft[1];
					String[] destinationStates = parts[1].split(",");
					transitionsMap.put(character, destinationStates);
				}
			}
			
			//that' it - we have a piece of data --> add it to the set
			StateData toAdd = new StateData(currentState, acceptable, transitionsMap);
			statesData.put(currentState, toAdd);
		}
		
		//let's add transitions and states to the automaton!
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.addState(stateName, data.acceptable);
		}
		
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.bindTransitions(stateName, data.transitions);
		}
		
		ourAutomaton.setStartingState(startState);

		Set<MinDka.Automaton.State> reachableStates = ourAutomaton.reachableStates();
		ourAutomaton.reduceUnreachableStates(reachableStates);
//		System.out.println(reachableStates);
		
		
		//let's do the 2nd algorithm right now
		Collection<TreeSet<MinDka.Automaton.State>> equivalentStates =  ourAutomaton.equivalentStates();
		ourAutomaton.reduceEquivalentStates(equivalentStates);
//		System.out.println(equivalentStates);
		
		System.out.print(ourAutomaton.SecondLabOutput());
		ourAutomaton.reset();
	}
	
	private static class StateData {
		
		private String name;
		private boolean acceptable;
		private Map<String, String[]> transitions;
		public StateData(String name, boolean acceptable, Map<String, String[]> transitions) {
			super();
			this.name = name;
			this.acceptable = acceptable;
			this.transitions = transitions;
		}
	}
	
}
