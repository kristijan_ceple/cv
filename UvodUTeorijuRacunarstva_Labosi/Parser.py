"""
A simple top to bottom parser implementation.
4th Introduction to Automata Theory lab
"""
def S():
    """
    Character S parsing
    """

    print("S", end='', flush = True)
    # First a switch-case statement
    switcher_S = {
        'a' : S_a,
        'b' : S_b
    }
    func = switcher_S.get(head, unacceptable_input)
    func()

def A():
    """
     Character A parsing
    """

    print("A", end='', flush = True)
    switcher_A = {
        'a' : A_a,
        'b' : A_b
    }
    func = switcher_A.get(head, unacceptable_input)
    func()

def B():
    """
     Character B parsing
    """

    global head
    print("B", end='', flush = True)

    if head == 'c':
        global i
        i += 1
        head = input[i]

        if head != 'c':
            return
        i += 1
        head = input[i]

        S()

        if head != 'b':
            return
        i += 1
        head = input[i]

        if head != 'c':
            return
        i += 1
        head = input[i]

def C():
    """
     Character C parsing
    """

    print("C", end='', flush = True)
    A()
    A()

def unacceptable_input():
    """
    Print "NE" and exit!
    """
    print()
    print("NE")
    raise SystemExit

"""
##############################        SWITCHER FUNCTIONS        #################################3
"""
def S_a():
    global i, head
    i += 1
    head = input[i]


    A()
    B()

def S_b():
    global i, head
    i += 1
    head = input[i]

    B()
    A()

def A_a():
    global i, head
    i += 1
    head = input[i]


def A_b():
    global i, head
    i += 1
    head = input[i]

    C()
"""
##############################        SWITCHER FUNCTIONS END        #################################3
"""

# Main
END_OF_INPUT = '#'
input = input()             # The input
input = input + END_OF_INPUT     # Epsilon marking the end of our input

"""
Here begins the parsing process
"""
head = input[0]
i = 0

S()
print()
if(head == END_OF_INPUT):
    print("DA")
else:
    print("NE")
